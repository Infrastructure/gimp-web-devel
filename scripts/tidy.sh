#!/bin/bash
#
# call as
# $ bash scripts/tidy.sh

for file in `find content -name *.md`
do
sed -i "s/ *$//" $file
done
