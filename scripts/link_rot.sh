#!/bin/bash

function find_link_rot () {
wget -r https://developer.gimp.org/

for file in `find developer.gimp.org`
do
    URL="$1/${file:19}"
    curl --head --silent --fail $URL &> /dev/null
	if [ $? -ne 0 ]
	then
    	echo "The URL $URL is missing."
	fi
done
}

find_link_rot http://localhost:1313
