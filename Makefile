all: build

build:
	hugo
	bash scripts/update_api_docs.sh

build-main:
	hugo -b https://developer.gimp.org
	bash scripts/update_api_docs.sh

build-testing:
	hugo -b https://testing.developer.gimp.org
	bash scripts/update_api_docs.sh

test:
	hugo server

link-rot:
	bash scripts/link_rot.sh
