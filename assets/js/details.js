/* The Javascript code which goes with layouts/shortcodes/details.html */

function copyMarkdown(button) {
  md = button.parentNode.nextSibling.textContent;
  try {
    navigator.clipboard.writeText(md);
    console.log('Details text was copied.');
  } catch (err) {
    console.error('Could not copy details text: ', err);
  }
}
