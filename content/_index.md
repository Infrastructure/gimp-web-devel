+++
title = "GIMP Development"
date = "2022-07-19"
description = "Online Resources for GIMP Developers"
author = "The GIMP Development Team"
+++

This site tries to provide useful information for developers:

* If you are into resource creation, e.g. plug-in development, you
  should look at the [Resource Development](/resource/)
  section.
* If you wish to contribute to GIMP itself, you should read the [Core
  Development](/core/) section.

*Note: for the main GIMP website, visit [www.gimp.org](https://www.gimp.org)
instead; the GIMP manual is at [docs.gimp.org](https://docs.gimp.org/).*

## GIMP 3.0 roadmap

{{< roadmap "3.0" >}}

*[See more roadmaps](/core/roadmap/)*
