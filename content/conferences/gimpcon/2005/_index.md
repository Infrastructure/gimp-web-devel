---
title: "GIMP Developers Conference 2005"
Author: "GIMP team"
Date: 2005-05-30
lastmod: 2016-10-24
---

The GIMP Developers Conference 2005 was held as a sub-event of the [GUADEC 2005](http://2005.guadec.org/) in Stuttgart, Germany on 30.05.2005.


## Minutes of the GIMP Developers Conference 2005

About a dozen people were present at the meeting. We haven't made a
list but off the top of my head I remember Mitch, Michael Schumacher,
Øyvind Kolås, Simon Budig, Raphael Quinet, Karine Delvare and her
husband, David Odin and me. I know I missed someone. Please bear with
me, I suck at remembering names...


### Merchandising

Picking up on the discussion about Sourcewear that we had on this list
earlier, we talked about merchandising. I had the impression that
everyone was rather happy about the solution that we found on the
mailing-list. We agreed that we don't need an official merchandiser
but that it is a good thing to link to shops that support the GIMP
project.


### Spending our money

It was shortly discussed on how the money that is raised by donations
or from other sources should be spent. Problems with the mailing-list
server were mentioned, but without Yosh being around we weren't able
to tell if money could help to improve this. We seemed to agree that
the money should primarily be spent to bring together the people
working on GIMP. We think that it helps GIMP if money is used to help
contributors to attend the next GimpCon. This can also be seen as some
sort of reward for the active contributors and thus can serve a
similar role as a bounty.


### Next GIMPCon

Everyone agreed that we should have another GimpCon soon, if possible
still this year. This needs a suitable location and people willing to
organize the event.  A possible location would be Lyon, France where
David Odin has good connections to the university. He said he will
investigate for possible dates. If you can think of other locations
(Ottawa, Canada was mentioned), please tell us about it.


### Roadmap

The roadmap for GIMP was discussed shortly. We would like to get GIMP
2.4 out soon. The plan is to finish what has been started in the
development branch. This should be doable over the summer. This means
that 2.4 will have color management but we aren't going to try larger
changes such as adding support for higher bit depths.


### GEGL

We agreed though that 8bit is not going to get us much further and
that we need to pick up on GEGL again. The GEGL source tree had been
abandoned for a while, the last commit dating back to March 2004. We
found that in order to make further plans, we first need to get an
overview on the current state of the code. Mitch, Pippin and me
decided that we will spend the next days cleaning up the code in GEGl,
evaluating it and making plans. That's what we've been doing during
the last days. More on that later...


### Menu reorganisation

The menu reorganisation effort was raised. It seems that Akkana's
proposal is widely accepted. The proposed patch can be improved but it
is a good start. If Akkana or someone else has time and motivation to
continute to work on this, then the patch can be committed right away.



GUADEC rocked hard this year and it was a pleasure to meet so many
great people.



Authors: Sven Neumann, Simon Budig. [Read source and discussion](http://gimp.1065349.n5.nabble.com/The-GUADEC-meeting-td15323.html)
