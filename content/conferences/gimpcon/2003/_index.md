+++
title = "GIMP Developers Conference 2003"
abbrev = "GIMPCon 2003"
description = "Chaos Communication Camp, Berlin, Germany"
summary = "Chaos Communication Camp, Berlin, Germany"
date = "2003-08-07"
show_subs = false
+++

## GIMPCon 2003
Chaos Communication Camp, near Berlin

The second GIMP developers conference took place the
*7/8/9/10th August 2003* at the [Chaos Communication
Camp](http://www.ccc.de/camp/2003/). The Camp was an international,
four-day open-air event for hackers and associated life-forms on a field
near Berlin, Germany.

{{< figure src="people-small.png" link="people.png" caption="The People at GimpCon 2003" >}}

The GIMP developers joined this event and set up a GIMP tent to
get together and discuss the future of GIMP development.
Summaries of some of the discussions held there are available
[below](#minutes-of-the-gimp-developers-conference-2003).

We'd like to thank the
<a href="http://www.fsf.org/">Free Software Foundation</a>,
<a href="http://www.oreilly.de/">O'Reilly Germany</a>,
<a href="http://www.macgimp.org/">MacGIMP</a>,
<a href="http://www.flamingtext.com/">FlamingText</a> and
the <a href="https://www.ccc.de/">Chaos Computer Club</a>.
Without their support this meeting wouldn't have been possible.

## Minutes of The GIMP Developers Conference 2003

Three big meetings were held at GIMPCon 2003. The topics of these
meetings are listed below along with links to the minutes of the
meetings.

### The First Big Serious Meeting

This meeting was held in the evening of August 7th, 2003. The main
issues discussed were:

* [The GIMP foundation](minutes-1#the-gimp-foundation)
* [Release manager](minutes-1#release-manager)
* [Decision making (or lack of it)](minutes-1#decision-making-or-lack-of-it)
* [General stuff about CVS, Bugzilla](minutes-1#general-stuff-about-bugzilla-and-cvs)
* [Communication](minutes-1#communication)

The minutes from this meeting can be found [here]({{< ref "minutes-1.md" >}}).

### The Second Big Serious Meeting

This meeting was held in the evening of August 8th, 2003. The
main issues discussed were:

* [Features required for 2.0](minutes-2#features-required-for-20)
* [Documentation](minutes-2#documentation)
* [Web-site](minutes-2#web-site)
* [Roadmap](minutes-2#roadmap)
* [Bugs](minutes-2#bugs)
* [Task List](minutes-2#task-list)
* [GIMP Foundation](minutes-2#gimp-foundation)
* [Release manager](minutes-2#release-manager)

The minutes from this meeting can be found [here]({{< ref "minutes-2.md" >}}).

### The Third Big Serious Meeting

This meeting was held in the afternoon of August 9th, 2003. The
main issues discussed were:

* [How to get new developers?](minutes-3#how-to-get-new-developers)
* [De we need binary distributions?](minutes-3#do-we-need-binary-distributions)
* [Is Bugzilla too hard to use for new users?](minutes-3#is-bugzilla-too-hard-to-use-for-new-users)
* [List of open tasks](minutes-3#list-of-open-tasks)
* [Mailing lists](minutes-3#mailing-lists)

The minutes from this meeting can be found [here]({{< ref "minutes-3.md" >}}).
