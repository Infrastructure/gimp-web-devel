---
title: "GIMP Developer Conferences"
abbrev: "Conferences"
description: "Hanging out with GIMP developers"
date: 2022-08-06T12:49:14-0500
author: 'Pat David'
menu: main
weight: 3
show_subs: false
---

## Developer meetings

Across the years, [GIMP](https://www.gimp.org) and
[GEGL](https://www.gegl.org) developers from all
over the World have gathered at multiple occasions.

Called historically as "GIMP Developers Conference", abbreviated
GIMPCon, this event continued at the [Libre Graphics
Meeting](https://libregraphicsmeeting.org/) for years, starting in 2006,
where we also exchanged with developers of other graphics FLOSS.
These are where we get together and discuss the future development, hack
on GIMP, meet GIMP users…

A new event called "Wilber Week" was born a few years ago, where we meet
just with GIMP developers in a more quiet atmosphere (no conferences, no
big talks…).

* [Libre Graphics Meeting 2024](lgm/2024-rennes_france/), May 9 - May 11, Rennes, France
  ([LGM page](https://libregraphicsmeeting.org/2024/)).
* [Wilber Week 2023](wilberweek/2023-amsterdam/), May 19-28, Amsterdam
* Libre Graphics Meeting 2019, May 29 - June 2, Saarbrücken, Germany
  ([LGM page](https://libregraphicsmeeting.org/2019/)).
  *No minutes available.*
* Libre Graphics Meeting 2018, April 26-30, Seville, Spain
  ([announcement](https://www.gimp.org/news/2018/01/09/libre-graphics-meeting-scale-2018/),
  [LGM page](https://libregraphicsmeeting.org/2018/)).
  *No minutes available.*
* Libre Graphics Meeting 2017, April 20-23, Rio de Janeiro, Brazil
  ([LGM page](https://libregraphicsmeeting.org/2017/)).
  *No minutes available.*
* Wilber Week 2017, January 27 - February 5, El Bruc, near Barcelona, Spain
  ([announcement](https://www.gimp.org/news/2017/01/18/wilberweek-2017-announced/),
  [LGM page](https://libregraphicsmeeting.org/2017/)).
  *No minutes available.*
* Libre Graphics Meeting 2016, April 15-18, University of Westminster, in London, UK
  ([announcement](https://www.gimp.org/news/2016/01/12/libre-graphics-meeting-2016/),
  [LGM page](https://libregraphicsmeeting.org/2016/)).
  *No minutes available.*
* Libre Graphics Meeting 2015, April 29 - May 2, Toronto, Canada
  ([announcement](https://www.gimp.org/news/2015/02/27/libre-graphics-meeting-2015/),
  [LGM page](https://libregraphicsmeeting.org/2015/)).
  *No minutes available.*
* [Libre Graphics Meeting 2014](lgm/2014-leipzig_germany/), April 2-5, Leipzig, Germany
* [Libre Graphics Meeting 2013](lgm/2013-madrid_spain/), April 10-13, Madrid, Spain
* [Libre Graphics Meeting 2012](lgm/2012-vienna_austria/), May 2-5, Vienna, Austria
* [Libre Graphics Meeting 2011](lgm/2011-montreal_canada/), May 10-13, Montreal, Canada
* [Libre Graphics Meeting 2010](lgm/2010-brussels_belgium/), May 27-30, Brussels, Belgium
* [Libre Graphics Meeting 2009](lgm/2009-montreal_canada/), May 6-9, Montreal, Canada
* [Libre Graphics Meeting 2008](lgm/2008-wroclaw_poland/), May 8-11, Wrocław, Poland
* [Libre Graphics Meeting 2007](lgm/2007-montreal_canada/), May 4-6, Montreal, Canada
* [GIMPCon at Libre Graphics Meeting 2006](gimpcon/2006), March 17-18-19 in Lyon, France
* [GIMP meeting at GUADEC 2005](gimpcon/2005/), May 28-29-30-31 in Stuttgart, Germany
* [GIMPCon 2004](gimpcon/2004), June 28-29-30 in Kristiansand, Norway
* [GIMPCon 2003](gimpcon/2003), August 7-8-9-10 in Berlin, Germany
* [GIMPCon 2000](gimpcon/2000), June 2-3-4 in Berlin, Germany

## Solo conferences

GIMP team members have been showing their work at various other events,
even though it was not always including a meetup with many project
members. Some of these events are:

* Conference ["Processing Photographs with
  GIMP"](https://peertube.linuxrocks.online/w/35JggvHZRL5aPHeYLTnEzB) by Pat
  David - **Creative Freedom Summit 2023**
* Conference ["GIMP et
  ZeMarmot"](https://webtv.vandoeuvre.net/w/db139aff-6978-436f-9cbc-5854250855e4)
  (in French) by Aryeom and Jehan - **Fabrique Collective de la Culture du
  libre** in Vandœuvre - 2022-11-04
* **Libre Graphics Meeting 2020**, May 27-29, online, with a remote workshop
  on retouching basics by Aryeom and Jehan
  ([program](https://libregraphicsmeeting.org/2020/en/program.html))
* **Scale 16x 2018**, March 8-11, Pasadenia, California (USA), with a
  conference by Patrick David
  ([announcement](https://www.gimp.org/news/2018/01/09/libre-graphics-meeting-scale-2018/))
* **GUADEC 2016**, August 12-14, with Aryeom and Jehan presenting [ZeMarmot
  project](https://2016.guadec.org/schedule/index.html#42-zemarmot__open_animation_film_produced_with_foss)

## Historical developer meetings on IRC

Finally some developers meeting happened online. A first attempt started
with bi-weekly IRC meetings in 2011 (arranged by LightningIsMyName, once
in every two weeks, on Monday at 10:00 PM, Central Europe Timezone
(usually, CET = GMT+1), with a mail sent to the mailing list before each
meeting to discuss the agenda, and after every meeting with a link to
the meeting page). This process ended but the logs are still available
for logging history:

* [2011 April 19](irc_meeting/dev_meeting_19_apr_2011/)
* [2011 March 28](irc_meeting/dev_meeting_28_mar_2011/)
* [2011 March 14](irc_meeting/dev_meeting_14_mar_2011/)
* [First IRC developer meeting! — 2011 February 28](irc_meeting/dev_meeting_28_feb_2011/)

Nowadays more informal meetings are organized on video-conference tools
such as Jitsi and more recently Big Blue Button, organized by Aryeom and
Jehan. A few such meetings happened already in 2020, 2021 and 2022. So far
these events are not logged.
