---
title: "Libre Graphics 2007: Wrocław, Poland"
author: "The GIMP Development Team"
date: 2008-05-08
lastmod: 2016-10-24
---

* [Homepage](http://libregraphicsmeeting.org/2008/index.php?lang=en)
* [Program](http://www.libregraphicsmeeting.org/2008/index.php?lang=en&amp;action=program)


## GIMP and GEGL topics

* Øyvind Kolås: Extending GEGL with new operations and an introduction to GeglBuffers, the functionality offered by them and their underlying mechanisms
* Peter Sikking: GIMP: a new simple interface for a complex application
* Michael Terry: ingimp 1 Year Later: Initial Data Analyses and Lessons Learned
* Joao S. O. Bueno: Python: Binding the Libre Universe Together - scripting in GIMP and other Graphic program
* Cédric Gémy: Live creation of a magazin using Inkscape+Gimp+Blender+Scribus
* Mariusz Ufnal: GIMP workshop

## Other topics

* Carl Worth: What cairo has to offer now (with 1.6.0) and where it's going,(colour management, extended PDF capability, hardware acceleration, etc.)
* Chris Lilley: What is happening with SVG
* Harrisson, Pierre Huyghebaert, Femke Snelting, Nicolas Malevé: Designing with free software
* Dave Crossland: Font Software Freedom
* Michael Dominic Kostrzewa: Programmers hell: working with the UI designer
* Peter Linnell, Cyrille Berger, Kai-Uwe Behrmann: BoF: OpenICC - goals, themes and results
* Kai-Uwe Behrmann: Introduction to the Oyranos colour management system
* Emanuele Tamponi: A realistic color model for representing pigment
* Udi Fuchs: UFRaw
* Dave Coffin: Presenation about DCRaw
* Liam Quin: Proposal, shared type/text specification for the desktop

More topics were other graphics software, natural media painting simulation etc.
