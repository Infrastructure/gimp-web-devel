---
title: "Libre Graphics 2009: Montréal, Canada"
author: "The GIMP Development Team"
date: 2009-06-09
lastmod: 2016-10-09
---

* [Homepage](http://libregraphicsmeeting.org/2009/)
* [Program](http://libregraphicsmeeting.org/2009/program.php)
* [Announcement on gimp.org](https://www.gimp.org/news/2009/03/07/libre-graphics-meeting-2009/)

## GIMP and GEGL topics

* Michael Terry: ingimp: A Smorgasbord of Usability, Adaptive UIs, and Visually Arresting Graphic Design for 2009
* Peter Sikking: GIMP UI: taking some big issues by the horns
* Øyvind Kolås: Stopmotion

## Other topics

* Andrew Mihal: Nona-GPU: Image Remapping on the Graphics Processor

More topics were graphics FOSS, CSS, fonts etc.
