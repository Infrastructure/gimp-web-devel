---
title: "Libre Graphics Meeting 2014: minutes"
author: "The GIMP Development Team"
date: 2014-04-03
lastmod: 2015-09-05
---

* University of Leipzig, Germany
* GIMP team meeting minutes
* 03.+06.04.2014

Participants:

* Hartmut Kuhse (OnkelHatti) (intermittently)
* Jehan Pagès (Jehan_ZeMarmot)
* Joao S. O. Bueno (gwidion) (intermittently)
* Michael Natterer (mitch)
* Michael Schumacher (schumaml) (minute taker)
* Øyvind Kolås (pippin)
* Peter Sikking (guihipster)
* Simon Budig (nomis) (minute taker)
* Sven Claussner (scl) (minute taker)
* Ville Pätsi (drc) (intermittently)

Guests:

* Antenne Springborn (antenne) (intermittently)
* Aryeom Han
* Jon Nordby (jonnor) from MyPaint (intermittently)
* Tobias Ellinghaus (houz) from Darktable (intermittently)

## Last Year’s Minutes

[https://docs.google.com/document/d/1O8UUijIyVM5r5ivECzwakjUC37LseJ6Az_we-icIOKo/](https://docs.google.com/document/d/1O8UUijIyVM5r5ivECzwakjUC37LseJ6Az_we-icIOKo/)

## Collaboration between developers and interaction architect

Peter points out that the state of the collaboration is critical. For the last three years it has gone down a negative spiral. Sometimes he is asked for advice from developers how certain things in the UI can be solved, but finally it’s often implemented differently. Currently he doesn’t see results from a collaboration with the designer anymore. Either we reboot or stop it.
The Free Select Tool is one positive example. Martin Nordholts and Peter worked complementary very well. The reasons for its success were mutual trust and mutual support. Peter likes to see more of such a fruitful collaboration. Otherwise he sees no alternative than leaving the project.
In Peter’s opinion the non-deterministic development practice is problematic.
Michael Natterer points out that it also necessary to have the UI designer at hand when we need him. In the case of the Unified Transform Tool the developers liked to clarify open questions with the UI designer, but the UI was missing his input.

Peter explains his point of view with a triangle diagram:

![Roles-triangle.png](../Roles-triangle.png)

The designer’s role is to ‘connect the synapses’ with his design, i.e. to moderate between
the three corners to finally bring the project to success.
There is no complete agreement over this model. Some developers point out that this model is more suitable to companies which deliver products and where it is usual to tell other people what they have to do. In their opinion open source development is different: there is no such thing as a ‘product’ and people work on what they like. With respect to the difficulties to find and bind contributors this is something we have to deal with if we want to be attractive to contributors. Separating the designers from the developers takes the fun out of development.
In the further discussion the ‘product’ is more neutrally named ‘gold’.

Final agreement:
To revive the collaboration we need to get a way to improve the shipment of the designer’s results.  From now on there are two ways to implement things:
1. Developers can solve UI problems as they think without involving the UI designer. These things can land in the release. It might be possible that they will be changed in the future and redone involving more comprehensive UI considerations.
2. Developers can ask the interaction architect for advice, but then it becomes a 100% UI topic. This means that these things will be implemented to the UI designer’s specifications and shipped.

## Feature planning for GIMP 2.10

The roadmap in the wiki is outdated. We agree to have the following features in GIMP 2.10 and to update the feature roadmap in the wiki accordingly.
Features and actions for GIMP 2.10:

* GEGL porting: to be finished,
* porting file plugins to GEGL: done,
* fix PNG and TIFF support,
* high bit depths: sort out number format modes in the UI,
* OpenCL porting: is outside of GIMP’s responsibility (is a GEGL task),
* support layer masks,
* libgimp: some more new UI, deprecations, mostly
  * libgimp drawable core,
  * libgimp LCMS API,
  * libgimp metadata API,
* ensure that speedups land in Windows and OSX,
* synchronize Windows nightly and release builds to make builds repeatable,
* commit tools’ results on switching to another function,
* Warp Tool: needs Peter’s review,
* Seamless Clone Tool: needs more work → scl: what exactly?,
* Unified Transform Tool: remove grid(?) look for precision aspects which don't harm feeling,
* Foreground Select Tool is in horrible state: → scl: actions to be taken?,
* fix the mess in the menus, e.g. duplicated entries,
* PSD support is merged into GIMP master and utterly broken: needs rework,
* professional theme.

Agreement:
Instead of big bang releases we switch to a rolling release strategy: each release can have bug fixes as well as new features.

Feature playground:
If a feature isn’t matured enough for production use yet, we release it disabled: it is not shown in the UI by default, but can be enabled in a configuration file. If done so, then GIMP shows a warning: ‘Development feature, might crash. Use at your own risk.’
Playground features for GIMP 2.10:

* n-point Transform Tool,
* constraint-based Transform Tool (see bug 721009).

Floating selection: complex changes, leave it for now
GimpUnitEntry widget: not GTK 3 compliant → will not become part of GIMP or Prioritizing quick fixes with Peter → scl: what was the exact result: live or let die?

Sven remarks that the current way of doing the GEGL port seems not be useful to finish it soon: although everybody is free to port filters to GEGL, the process is quite slow. Some  filters might get sorted out in GIMP 3.0 when
a) the set of default plug-ins and scripts will be revamped (see wiki document 'Mindstorm:Revamping default plugins and scripts') or
b) they won’t be compliant to the new, GTK 3 based API.
We should rather first make a decision whether to keep each filter and then port only the ‘good ones’ to avoid unnecessary work and delay.
Other developers point out that there seems to be a misunderstanding. Pippin already disabled some filters in the GIMP master’s UI. Every developer is free to priorize GEGL ports him/herself.
Note from scl: IMHO this doesn’t solve the problem of slow progress and delay. Have I missed something important or are we hampering ourselves?

## Malicious and reputation-damaging installers and apps

(by schumaml)

1) Malware and ad-ware shippings bundled with GIMP:
This was in the agenda last year already, but we didn’t feel the need for any immediate action. Now, we have
a) abandoned Sourceforge over the mere existence of a such an installer, even though we were never affected,
b) have a “GIMP” applications that is constantly being shoved upon Craigslist user to make them install it to view some “GIMP Images” the scammers send them, and
c) we have a number of installers that ship with sneak-download in “added value” in the form of adware at best, annoying scareware in the middle and full-blown malware at worst.



They are cleverly using one of FLOSS’ greatest advantages - that anyone may distribute it, as long as they comply to the licenses, in this case GPL - against us. Legally, there isn’t much, or anything we can do - adding GIMP to an installer is covered by mere aggregation, according to the people I’ve asked. We would probably be able to do something against b), but we can assume that there are real criminals behind that, one who won’t care about such things as laws.



2) There has also been a related issue, although it is a different angle. On the iTunes store, a user has found the following app:
[https://itunes.apple.com/us/app/awesome-gimp-photo-editor-pro/id756630894](https://itunes.apple.com/us/app/awesome-gimp-photo-editor-pro/id756630894)

That name and icon sem to be just a bit similar, doesn’t it - I got a PDF printout of that page in
case it is taken down or altered, available here:
[https://drive.google.com/file/d/0B-ZgvtqvrjNnTkgzQlZwaUVMYUk/edit?usp=sharing](https://drive.google.com/file/d/0B-ZgvtqvrjNnTkgzQlZwaUVMYUk/edit?usp=sharing)

So this seems to be someone who is trying to capitalize on our name alone (similar to b) above), although the damage to the user is limited to the 99c they spent.
Nevertheless, I complained to Apple, the mailed the author and told him to explain himself, he went on like “that application name is totally different”, and “the icon is from $site and available under some cc license”.

Yeah, right.

A reply to that had been sent by me, with an addendum to Apply as follows:
“@Apple: we really think that this shouldn't be hard for you to decide.”

scl: As of 25.04.2014 the app is still available in the iTunes Store and the iOS AppStore. I propose to insist to Apple to take action.

3) How do we deal with GIMP builds that come with additional plug-ins, such as Simone Karin Lehmann’s or Partha Bagchi’s build?

Agreement to 3)
3rd party plug-ins and improvements to OS-integration (OS X, Windows, etc.) are ok.
? Shall they become part of the official build?
Changing GIMP’s designed behaviour, like modifications to the Save-Export-behaviour must not become part of the official GIMP builds.

Suggestions about how to resolve such issues quicker are welcome:
Brand registration

Some people have suggested that we could finally register GIMP and GNU Image Manipulation Program as a brand, to gain some leverage there.
At least one person tried this in Germany, but failed:

[https://register.dpma.de/DPMAregister/marke/register/302620656/DE?lang=en](https://register.dpma.de/DPMAregister/marke/register/302620656/DE?lang=en)
- this was the owner of the defunct www.gimp.de forum/blog, we snatched the gimp.de domain when it expired, iirc thanks to nomis spotting it. We do not know any details about the intentions of the attempted brand registration.

IIRC there was some attempt to register (basically, troll) GIMP that way in Austria, but I can’t find any trace of it in their brand database.

The following link allows for a brand search in several countries - turns up something, but none related or intended to hamper GIMP in any way, it seems:
[https://www.tmdn.org/tmview/welcome](https://www.tmdn.org/tmview/welcome)

The name ‘GIMP’ and the Wilber logo shall be protected. Michael Schumacher asks the FSF for advice, cc to Michael Natterer. Also ask/inform Tuomas Kuosmanen (TigerT) about intended use of his logo.
Also GEGL shall be protected. → what exactly: name, logo? Also Babl?
Other suggestions?
Fill them in here, or add new sections.
none

## @gimp.org mail aliases for contributors

(by schumaml)

I remember we’ve last discussed this years ago, and some people might actually requested an gimp.org alias to use on e.g. the mailing lists. Others were hesitant, didn’t want to bear the responsibility to have their mails interpreted as “speaking for the project”.
This is, in my opinion, an unsubstantiated fear nowadays. My perception is that everywhere, anywhere, where someone answers a user question by a statement that sounds is it it can have merit, it is taken for an official statement of “The Developers”. This is dangerous because some people do not actually verify their replies before posting on a forum - e.g. by having a look in the gimp user manual - and thus this information spreads.

Establishing some sort of “official” spokesperson for GIMP might help to alleviate this problem, but we want to make sure that there#s no shift towards “no @gimp.org”, not trsuthworthy - there are many people who won’t have such an address, but spread verified and correct help to users. It is just that right now, there is virtually no way to someone to distinguish GIMP contributors from other people, at least not without additional research.
Addendum: team business cards
A possible addition, and nice touch in regard to print-orientation, would be to have official team business cards. These might seem obsolete nowadays, but there are places and times where pulling out a phone to pop over a vCard is not feasible or not deemed appropriate.

And because we are better at doing things than planning, we should probably finish the design and choose other parameters (eco-friendly material,  produced by company the uses  FLOSS/engages)

scl: I don’t remember the results. Do you or was the topic postponed?

## More branch-driven development

(by schumaml; scl:+1)

I’m not sure if we use Git like to should be used - other projects seem to use a lot more branches than we do. This may be due to the fact that that some of our contributors have grown up with CVS, migrated to SVN and still carry its concepts over to Git… but I might be biased ;)
Some opinions on if/how to change this, and a recommended education/enabling program could be useful.

Michael Natterer: have no problems with more branches, but draw attention to these policies:
1. The master branch must always be stable.
2. To update the side branches we rebase them.

We state that many of us come from SVN and use Git as it was SVN. TODO: post a migration tutorial from SVN to Git to use Git as it is intended.

## Bug handling

(by schumaml)

According to a former product manager of a Mozilla sub-project, and a study about bug handling in GNOME Bugzilla, we are doing pretty good compared to other projects. We read all the bugs, even if we may not be able to do something about them, and apparently we are also pretty thorough with assigning target milestones. I think that we can still be a bit better, though, and would like to discuss how exactly we can do this.

Study: [http://bugzillametrics.sourceforge.net/publications/ProcessQualityInGNOME.pdf](http://bugzillametrics.sourceforge.net/publications/ProcessQualityInGNOME.pdf)
and another one, explaining the importance of triaging: [http://mockus.us/papers/triage.pdf](http://mockus.us/papers/triage.pdf)

More gnome-love bugs
postponed, see ‘Increasing attractiveness for new contributors’

## Release management

(by scl)

We know our ‘it is done when it’s done’ situation: we make no clear statements about release dates, users wait sometimes long for a new release that fixes their bugs and out of a sudden without a previous announcement a new release appears.
Secondly the Windows nightly builds and release are still different what unnecessarily hampers reporting windows bugs - we never really know, if a reported problem or solution affects only the nightly build or the released Windows build.
I’m going to improve the current Jenkins installation and jobs. I can introduce Jenkins’ possibilities, answer questions and we could together find out, how it can solve our build and release problems and how far we want to go with it.

Jehan: the time between two releases is too long. Bug fix releases take three months or so. New features appear after years. This is disappointing for new contributors. Jehan proposes rolling releases, containing bug fixes as well as new features. → see chapter ‘Feature planning for GIMP 2.10’

Sven: Jenkins can do much more than just building and running a few tests. It can even enable us to deliver continuously, i.e. every commit can trigger a new release. Sven offers to describe Jenkins possibilities in public and then we decide about our release policy.
Peter: It’s also important to know our needs for this decision.

## Postponed topics

### GIMP User Manual review

(by schumaml)

Occasionally, I come about parts of the user manual that are not entirely accurate or misleading (usually when I look up something in order to refer people to it, in a mail or on IRC). The need to pass this information on in a timely fashion does often not allow me to file a bug report on the spot.

There may be technical means to alleviate the problem (I’d happily take a Firefox extension that will let me file a bug for a docs.gimp.org/* page, and maybe include marked text in the initial comment), but maybe we have to start an effort to do a full review of the (English) docs at least once.

### Increase attractiveness for new contributors

(by scl)

In comparison to GIMP’s goals and open tasks it lacks of contributors, for instance Windows developers. On the other hand it seems hard for new contributors to find their way through the code and tasks and inner processes of GIMP development. Let’s think how we can improve the code and overall infrastructure, build an inviting atmosphere (without allegations), bring together fun of development and necessary professionality to achieve our product vision. Also with this in mind I’m for more feature and tinker/prototype branches.

### More gnome-love bugs

Bugs that have the gnome-love keyword assigned as supposed to be rather simple bugs, ideal for beginners. They need not be trivial, but should be isolated and solvable in a rather short time, to maximize the return on investment for new contributors.

### GIMP on other platforms

(by scl)

The topic of GIMP on mobile devices and alternative platforms (e.g. Steam) raises up at least on the mailing lists. A recent Gartner study shows that the importance of the PC platform is declining and tablets etc. are becoming more important, mainly with Android OS (see [http://www.gartner.com/newsroom/id/2645115](http://www.gartner.com/newsroom/id/2645115)).
We should make a strategic decision: Do we want to support the new platforms? If yes, how? Can we do this? Do we want to wait until GNOME solves these problems satisfyingly (if it hasn’t already)?

### Supporting recent graphics hardware in GIMP

(by scl)

GIMP lacks support of graphics tablets and HiDPI displays. On the other hand other software like Krita shines with out-of-the-box-support for convertible computers (Krita Gemini, see [http://krita.org/item/220-krita-2-8-0-released](http://krita.org/item/220-krita-2-8-0-released)).
Usually we would say, this is solved in GNOME 3 and GIMP will inherit it with the GTK3 port. But this is far in the future.
Let’s think how we can support recent hardware better without letting users wait for so long.

### Improving the website and the wiki

(by scl)

In February I integrated the former website developer.gimp.org to our wiki (except GIMP’s API documentation). Some ideas are still open, see [http://wiki.gimp.org/index.php/Mindstorm:Rethinking_the_wiki](http://wiki.gimp.org/index.php/Mindstorm:Rethinking_the_wiki). Let’s discuss what of them is necessary and what not.
I was also told that the content of www.gimp.org is subject to reconsideration.
Perhaps we can decide these questions as a whole to find a consistent solution.

### Bringing open source commitment and real life together

(by scl)

I really love to contribute to GIMP (and surely others here do the same) but on the other hand everybody has a real life and reconciling both is often a hard thing. Can we share experiences on how each of us deals with this?

### Technical architecture documentation

(by scl)

To make it easier for GIMP developers (including my own practice), especially new contributors, I’m going to write down a current documentation of the technical architecture. Another purpose of it is to make spots visible which can cause us trouble (for instance performance leaks).To get it done I might interview the long-time GIMP/GEGL developers.
