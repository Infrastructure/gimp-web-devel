---
title: "Libre Graphics 2014: Leipzig, Germany"
author: "The GIMP Development Team"
date: 2014-04-02
lastmod: 2016-10-24
show_subs: false
---

"Libre Graphics Meeting 2014, the annual conference and workshop for users and makers of free software creative applications, will take place on April 2–5, in Leipzig, Germany.

The event is open to anyone with an interest in open source graphics, design, and creative culture. Software developers and artists alike are invited to attend, and will be welcome to submit topics for presentations, hands-on workshops, and birds-of-a-feather sessions once the call for participation is officially opened.

2014 will mark the ninth anniversary of LGM, the premiere gathering of the free software graphics community, drawing developers, users and supporters alike from across the globe."

(Announcement at the [Libre graphics World website](http://libregraphicsmeeting.org/2013/), retrieved on 29.06.2013)

## Links

* [Homepage](http://libregraphicsmeeting.org/2014/)
* [Program](http://libregraphicsmeeting.org/2014/program/)
* [GIMP meeting minutes](lgm_2014_minutes)
* [Announcement on gimp.org](https://www.gimp.org/news/2014/01/23/libre-graphics-meeting-2014/)

## GIMP and GEGL topics

* Peter Sikking: UI design for full GEGL integration in GIMP, Wed April 2, 2:30 p.m.

* Artem Popov: Keeping metadata in remixed works, Wed April 2, 3:10 p.m.
* Peter Liljenberg, Artem Popov: BOF: The future of metadata, Fri April 4, 12:10 p.m.

* Patrick David, Tobias Ellinghaus: Utilizing FL/OSS tools in a photographic workflow – Part II: The Work, Sat April 5, 12:30 p.m.

* Manuel Quiñones: GEGL is not GIMP - creating graphic applications with GEGL, Sat April 5, 12:30 p.m.

* Chris Murphy: State of color management, Wed April 2, 11:20 a.m.
* Chris Murphy: End to end color management workflow, Wed April 2, 12:10 p.m.
* Richard Hughes: Building an OpenHardware Spectrograph for Color Profiling in Linux, Wed April 2, 6:10 p.m.
* Chris Murphy: What about the color, Thu April 3, 12:10 p.m.

* Fateh Slavitskaya, Bassam Kurdali: Distributed Free-Cultural Production and the Future of Creative Economy, Sat April 5, 4:00 p.m.

## Other topics

* State of Libre Graphics, Wed April 2, 10:00 a.m.
* LGM Code of Conduct meeting, Fri April 4, 9:30 a.m.
* LGM group photo and closing session, Sat April 5, 6:00 p.m.

* Allan Day, Jakub Steiner: GNOME Design: Open to all, Sat April 5, 5:20 p.m.

* Johannes Hanika: Wavelets for image processing, Wed April 2, 5:50 p.m.
* Øyvind Kolås: 0xA000 a font family, Thu April 3, 10:10 a.m.
* Simon Budig: Fonts in Printed Circuit Board design tools, Thu April 3, 2:30 p.m.
* Jon Nordby, Henri Bergius: Visually programming libre graphics tools, Fri April 4, 12:10 p.m.
* Frederik Steinmetz: Contributing to the community: Writing add-ons, Fri April 4, 5:30 p.m.
* Steve Conklin: Open Hardware devices to aid software testing, Sat April 5, 3:40 p.m.
* Ana Isabel Carvalho, Ricardo Lafuente: Dear designer, have these cool tools, Sat April 5, 4:40 p.m.
* [and more...](http://libregraphicsmeeting.org/2014/program/)
