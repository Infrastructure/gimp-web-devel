---
title: "Libre Graphics 2010: Brussels, Belgium"
author: "The GIMP Development Team"
date: 2010-05-27
lastmod: 2016-10-24
---

* [Homepage](http://libregraphicsmeeting.org/2010/)
* [Program](http://libregraphicsmeeting.org/2010/program.php)
* [Videos](http://www.youtube.com/playlist?list=PL927C3C7CD4DE9E08)

## GIMP and GEGL topics

* Peter Sikking: A first outline for a UI for a fully GEGLed GIMP [Video](http://www.youtube.com/watch?v=D-kbH_ZVMmk&amp;list=PL927C3C7CD4DE9E08&amp;index=3)
* Akkana Peck: Writing GIMP scripts and plugins [Video](http://www.youtube.com/watch?v=anVRFuxqrgo&amp;list=PL927C3C7CD4DE9E08&amp;index=4)

## Other topics

Colour management, fonts, graphics tools etc.
