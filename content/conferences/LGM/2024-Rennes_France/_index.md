---
title: "Libre Graphics 2024: Rennes, France"
author: Jehan
date: 2024-05-03
lastmod: 2024-05-13
show_subs: false
---

"*The Libre Graphics Meeting (LGM) is a yearly event about Free Software
related to graphics.*"

In 2024, after a few years of pause because of the pandemy, it happens
in Rennes, France.

## Links

* [LGM 2024 Homepage](https://libregraphicsmeeting.org/2024/)
* [LGM 2024 Program](https://libregraphicsmeeting.org/2024/program.html)
* [Announcement on gimp.org](https://www.gimp.org/news/2014/01/23/libre-graphics-meeting-2014/)
* [Announcement by LILA](https://libreart.info/en/2024/04/lgm-24-screening-concert-zemarmot-gimp)

## GIMP team minutes
### People present

Team members present:

* Aryeom
* Carlos / garnacho
* Jehan
* Liam / demib0y
* Michael / schumaml
* Niels / nielsdg
* Simon / nomis
* Ville / drc

As a side note, we had quite a lot of interactions with Sebastian Wick,
from the Wayland project and working on colorimetry support for Wayland,
which was quite a fruitful encounter.

### Topics discussed

We didn't have planned meetings, but discussed a few topics at various
points during the stay. Some of the topics (as remembered after the
event) are listed below.

#### The XZ events and GIMP (a.k.a. *should we keep being as trusting as we used to?*)

In the first days, there were a bit of the discussion about trust in
community (the discussion was started by Simon IIRC), and what the XZ
recent issues could mean for us:

* I (Jehan) noted that it is harder to review everything lately because
  we are getting quite a lot more contributions on various aspects,
  in all sort of code areas, but also packaging, CI, developer
  documentation and more… Our elevated rights logic is historically
  mostly based on trust, knowing past work of contributors. I also want
  to avoid maintainers to become too much of a bottleneck by delegating
  more and more.
* As a consequence, social engineering to get elevated git access is not
  impossible in GIMP. As much as being a welcoming and trusting community
  is heartwarming, it may also be seen as a security risk.
* Michael and Simon noted that a security review session (code and
  packaging) might be of interest before GIMP 3 review?
* The trust issue is a bit more important with online-only people, i.e.
  people we never met physically (though even actually meeting people is
  not 100% trustable either, especially for long-run attacks).
* We should be more wary of anyone being a bit too "pushy" (without
  crossing paranoia levels?). We have had a few pushy contributors from
  time to time across the years. This is true in the present, with some
  contributors being sometimes annoying, if not borderline oppressive in
  Gitlab or IRC. Should we be more cautious of such contributors
  (additionaly to telling them when they cross a line, which we already
  do)?

These various points mostly stayed as open answers, hence the question of
how to stay a welcoming community, not falling into paranoïa, while also
staying safe, is not fully answered yet.

As a side topic (yet related), we wondered if we should keep using XZ
for our tarballs. Considering current knowledge of the attacks which
took place, we decided that so far, it looks safe to use xz compression
for tarballs.

#### Agreement with GNOME

* After team members answering by email (when they weren't present at
  LGM) and others answering live, all were in favor of legal review.
* Contract signed by Jehan on Sunday 05 for the legal review,
  counter-signed by Holly of GNOME on Monday 06. Review in-progress.

#### Social email account

We receive too much spam (a.k.a. "notifications") from Twitter and
nobody cares about them or read them (we used to have a contributor
really into social networks). We (mostly Liam and I, Jehan) agreed to
uncheck most notifications (but those for direct messaging).

#### GIMP 3

Some code was worked on but not a lot. Compared to a Wilber Week, time
is more spent on meeting other projects.

Some work also happened outside of GIMP (for instance, there were major
slowness issues in GTK's font chooser raised, which Niels debugged and
worked on).

#### GSoC

Nothing really specifically happened. We didn't make the time to make
video calls with GSoC contributors during LGM. Most of them were in
middle of exams anyway.

#### Wilber Week 2025 organization

- Chosen dates trying to get some of the new people!
- Is the Barcelona place a bit hard to go to? What about the advance
  money we already paid?
- Some other place ideas were suggested, though no suggestions really
  made it much further.
- On the last day, we decided to just go forward, with the Can Serrat
  place from July 4th to July 14th of 2025.

#### Wayland

Discussions with Sebastian happened on color management in Wayland.
Globally the plans re-explained in [Sebastian's
talk](https://libregraphicsmeeting.org/2024/conf-sebastianwicklinuxcolorhandlingmanagement.html)
as well as when discussing with him still seem quite solid. So we are
globally confident (while staying in the expectative for more).

The specific topic of multi-screen with color management was raised
again by myself (Jehan), though. I had already raised this topic back in
the [Wilber Week 2023](/conferences/wilberweek/2023-amsterdam/)
meetings, mostly with the conclusion that maybe a compromise was
acceptable. But after recent work massively using 2 displays, we really
came to the conclusion that if you really want Wayland to also target
graphics professional, it needs to be able to correctly manage colors on
several displays, i.e. for instance a single surface visible on 2
displays (e.g. display mirroring).<br/>
An argument was made by Sebastian that just targetting the best display
and accepting wrong render on the other display could suffice was not
good for us.
An extremely common use case is professional graphics display-tablets
which are unfortunately not that good as display, so artists work on the
tablet-display and double-check colors on a dedicated better-quality
display. In such a case, you want your surface to be perfectly rendered
on the better display but it doesn't mean it is OK to render it worse on
the graphics tablet display. You want it rendered as best as possible on
both displays.<br/>
An other argument about the fact that it would require more memory was
also not relevant in our opinion. Most people who color-manage their
workflow are professionals in field of graphics, buying possibly quite
expensive hardware and expecting to make good use of its rendering
capabilities. They do it knowingly, and they expect best render as
possible. If it requires more memory, then so be it. If they were OK for
approximate render, they would just not color-manage at all and stick to
low-end hardware.

An other topic of interested was window management (session storage for
windows position and size) which is something broken with GIMP on
Wayland currently. There are a few very lengthy discussions and even
merge requests related to such a capability in Wayland, but nothing was
ever decided yet. Niels said he'd help if he can.

## Conferences/Talks

3 talks and 1 meeting/workshop by GIMP team.

* [OpenType and the desktop](https://libregraphicsmeeting.org/2024/wk-liamquin-opentypedesktop.html) by Liam Quin
* Workshop "OpenType and the desktop", following the talk, to work on a
  cross-project font-selection service. As a consequence for it, a
  project on FreeDesktop is going to be created for people to continue
  the discussion and start implementing a shared service.
* [GIMP 3.0 and
  beyond](https://libregraphicsmeeting.org/2024/gimpteam-gimp.html) by
  Jehan
  ([slides](https://download.gimp.org/users/jehan/LGM-2024/GIMP-lgm-2024.odp), [video recording](https://peer.tube/w/s4ZoaPAFfekojyVybPCDkg)).
* [Early screening with live music (cinema-concert): « ZeMarmot
  »](https://libregraphicsmeeting.org/2024/Aryeomehan-zemarmot.html) by
  Aryeom and Jehan
  ([slides](https://download.gimp.org/users/jehan/LGM-2024/ZeMarmot-lgm-2024.odp), [video recording](https://peer.tube/w/vyDjnSM7hZgM1Kb9KSPnN4)).

Fun info: we were told by organizers that our GIMP conference was
particularly awaited because they suddenly had an afflux of people
the last day with people saying they came for the GIMP talk. Moreover
when coming back from lunch, we also met someone outside who came from
Paris and showed us our Patreon news on his phone. Apparently they came
for ZeMarmot! \o/

This is very heartwarming to see all these people coming specially for
GIMP, because they want to know more about it and learn how to use it.
