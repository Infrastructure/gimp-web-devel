---
title: "Libre Graphics 2012: Vienna, Austria"
author: "The GIMP Development Team"
date: 2012-05-05
lastmod: 2016-10-24
---

* [Homepage](http://libregraphicsmeeting.org/2012/)
* [Program](http://libregraphicsmeeting.org/2012/program/)
* [Announcement on gimp.org](https://www.gimp.org/news/2012/02/27/gimp-at-libre-graphics-meeting-2012/)


## GIMP and GEGL topics

* Peter Sikking, Kate Price: Rethinking text handling in GIMP [Video](http://www.youtube.com/watch?v=eWCmb3eIr6k)
* Victor Oliveira: Implementing OpenCL support in GEGL and GIMP [Slides](http://www.slideshare.net/lgworld/implementing-opencl-support-in-gegl-and-gimp#btnNext) [Video](http://www.youtube.com/watch?v=UzBPUalkqg8&amp;feature=plcp)
* Øyvind Kolås: GeglBuffer tight and flexible raster abstraction [Video](http://www.youtube.com/watch?v=HFDbSQk9Ba0&amp;feature=plcp)
* Michael Natterer: Goat Invasion in GIMP [Video](http://www.youtube.com/watch?v=HFDbSQk9Ba0&amp;feature=plcp)
* Øyvind Kolås: Petting Zoo with Goats [Video](http://www.youtube.com/watch?v=HFDbSQk9Ba0&amp;feature=plcp)
* Ramon Miranda: Digital Painting with open source [Slides](http://www.slideshare.net/lgworld/lgm2012-digital-painting)
* Martin Renold: Predictable Painting
* Bassam Kurdali, Fateh Slavitskaya: Tube Open Movie: Blender 3D Animation in a Distributed Pipeline
* Amir Hassan: SoundFumble: live sound generation with Gimp [Video](http://www.youtube.com/watch?v=teNkaSs_OmY&amp;feature=plcp)
* Sirko Kemter: Open Source, Just Works - The LGM Book Workshop

## Other topics

Colour management, import filters, text handling, other graphics software, workflow, tools etc.



