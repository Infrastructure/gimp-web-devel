---
title: "IRC developer meeting — 2011 April 19"
author: "The GIMP Development Team"
date: 2011-04-19
lastmod: 2019-05-07
---

Meeting page for the Developer Meeting which will take place in the GIMP IRC on April 19th 2011, 20:00 UTC (For time zone conversions, see [here](http://www.timeanddate.com/worldclock/fixedtime.html?msg=GIMP+Developer+Meeting+%234&amp;iso=20110419T20)). '''Developers who mentor at GSoC, and all other admins involved in GSoC, must arrive 20 minutes earlier''' to a final meeting and "clicking some buttons" ;)

## Agenda

### GSoC final meeting - GSoC Mentors Only!

As Alexia_Death stated, developers will meet 20 minutes before the official meeting in order to finish the process of handling GSoC student applications.

### GIMP 2.8

Congrats for the new GIMP 2.7.2 release :)

Now this obviously leads to the discussion of what next? What can we do to speed up 2.8? Some devs asked how can they help on Single-Window-Mode (SWM) and there were also some more questions. The more devs attending this meeting, the more complete will be the image of what we have left to do.

### Anything else?

Reply on the mailing list or email LightningIsMyName, and it will be added. (Developers, feel free to add topics and/or edit existing ones)

## Decided Actions

To be filled once the meeting ends

## Meeting Log

Will try to record and upload once the meeting is done
