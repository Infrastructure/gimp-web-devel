---
title: "IRC developer meeting — 2011 March 14"
author: "The GIMP Development Team"
date: 2011-03-14
lastmod: 2016-10-24
---

Meeting page for the Developer Meeting which will take place in the GIMP IRC on March 14th 2011, 10:00 PM CET (GMT+1)

**Time for Next Meeting(?):** March 28 2011, 10:00 PM CET (GMT+1).

## Agenda

### Reviewing last meeting's decision

Follow up what was done and what wasn't, from the decisions of last meeting. See [Dev Meeting 28 Feb 2011](../dev_meeting_28_feb_2011/).

### Serious discussion of GIMP's programming language

There have been many questions about whether we should switch some of GIMP's code to some language other than C+GObject, where the development would be more rapid. On the list of suggested languages we saw JavaScript, Python, Vala and some more.

There is a consensus(?) that core code was and will continue to be in C, mainly for performance reasons but not only. But now that we are starting to get some logic out to GEGL, would it make sense to change at least the language of the UI to something other than C?

Some people vetoed this, others vetoed remaining in C - so let's discuss it!

### 2.8 - Get it out!

We are [54 bugs](https://bugzilla.gnome.org/buglist.cgi?query_format=advanced&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;bug_status=NEEDINFO⌖_milestone=2.6⌖_milestone=2.8&amp;product=GIMP) away from releasing GIMP 2.8. Lets browse hrough that list and see who can fix what, and if there are any ways in which some devs can help others to balance the load (for example, mitch is working too much!)

### Roadmap Review

Enselic made a wonderful roadmap for gimp (*link dead*)! I think we should all review this together, to make plans clear for everyone. For example, some stuff that were proposed for the upcoming google summer of code, are targeted for versions of gimp which aren't exactly "near".

### Anything else?

Tell LightningIsMyName and he'll add it :)

## Decided Actions

See chat log - nothing organized this time...

## Meeting Log

```html
## Topic: GIMP developer meeting, 22:00 CET (GMT+1) March 14th 2011 | Agenda: http://gimp-wiki.who.ee/index.php?title=Hacking:Dev_Meeting_14_Mar_2011 | THIS MEETING IS BEING LOGGED!

<lightningismyname> Topic 1 on the agenda
 Reviewing last meeting's decision
 http://gimp-wiki.who.ee/index.php/Hacking:Dev_Meeting_28_Feb_2011#Decided_Actions
 old-decision 1: schumaml - any progress on wgo? I had none
<schumaml> neo hasn't replied to my mail
<lightningismyname> so we are stuck on this...
<schumaml> I asked him to tell me what' left to do to get automatic updates
<lightningismyname> ...
 old-decision 2: Alexia and mitch take care of redirecting wiki.gimp.org
 Both aren't attending, so it's kind of hopeless to ask for progress
 seems as if it's not working, so let's move on
<schumaml> it's not redirecting for me
<lightningismyname> old-decision 3: Enselic makes a draft of a GIMP roadmap and sends to gimp-developer
<enselic> done, next
<schumaml> http://gimp-wiki.who.ee/index.php/GIMP_Roadmap
<lightningismyname> Enselic, anything you'd like to add to your mail? Or should we wait for the in depth discussion, which is scheduled later for this meeting?
<enselic> yes let's be done with the old stuff first
<lightningismyname> old-decision 4: everybody subscribe to bug mail and TRIAGE BUGS
 Is any of the devs here not submitted to bugmail?
<enselic> I haven't noticed a significant increase in bug triaging
 it's not that important... it's more important that people write code
<schumaml> we should set goals and guidelines
<lightningismyname> 2.8 and future are planned for the rest of this meeting :)
<schumaml> LightningIsMyName: for bug triaging
<pippin> Enselic: regarding the roadmap, low:priority High-end CMYK support...
<lightningismyname> schumaml, for example? what sort of goals?
<pippin> Enselic: I think taking the separate- plug-in given on the mailinglist and making it part of GIMPs default install is a sensible thing to do
<enselic> pippin: let's wait with this discussion
<schumaml> LightningIsMyName: what to do with new bugs, unconfirmed bugs, long-time nedinfo
<enselic> common sense is enough to get started, if someone does the wrong thing, more experienced bug triagers will correct it
 don't be scared
<lightningismyname> :) I think this should be tightly related to the roadmap
<enselic> just start slowly so that people have a chance to correct you if you do something wrong
 next item on the old-action list please
<schumaml> bug don't necessarily involved the roadmap
 but yes, next
<lightningismyname> old-decision 5: mitch, make a development release soo
<enselic> epic fail, next ;)
<lightningismyname> mitch itsn't here, and 2.8 is topic 2 of the agenda
 old-decision 6: LightningIsMyName takes care of next meeting
 done, next :)
 old-decision 7: setting pages on the wiki to discuss changes in different topics
 now, here we have something more serious
<enselic> can someone elaborate on the topic? I don't understand it
<lightningismyname> for example, the last PDB change annoyed some people, and broke many things
 mainly scripts/plugins
<schumaml> what change?
<lightningismyname> these things should be discussed more in depth and not be solo work
 schumaml, all the context related ones
 Enselic, what's the status of the dev site?
<enselic> the work should have been performed on a feature branch, that's the problem
* LightningIsMyName objects new branches - we have too many of these
<kevin> I don't think the PDB changes broke anything but they made most scripts pop up some deprecation warnings.
<enselic> LightningIsMyName: if you mean tasktaste.com, it's not for production use yet
<enselic> let's revisist this topic after we're done with the old-action list
<lightningismyname> agreed
<enselic> next on that list please
<lightningismyname> old-decision 8: LightningIsMyName gets a list of projects on the wiki by tomorrow, developers review the ideas by email or by commenting on the wiki. Saturday (March 5th, 2011) is hard dead line for finalizing the project list!
 done with gsoc ideas :)
 old-decision 9: all devs PM/Email LightningIsMyName username and email
 any dev here without a user on the wiki?
<schumaml> yes
<enselic> ... that wants one
<lightningismyname> schumaml, as the topic states - PMing me is the way to get a user
 closed topic 1 - review old decisions
 topic 2: Serious discussion of GIMP's programming language
 I think this title talks for itself, but I'm afraid we have a serious lack of devs here
<enselic> I noticed that the AdaptableGIMP guys used Vala for quite some substantial parts of their added code, and I think Vala is the language we should choose as a higher-level GIMP language
<lightningismyname> I was also thinking in that direction. Is vala stable?
<enselic> I haven't had time to thoroughly test it yet unfortunately
<pippin> is the vala language spec frozen yet?
<schumaml> do we drop scheme and python for vala then?
<enselic> enough to get some vala code into GIMP
<lightningismyname> my main concern with vala is horror stories I heard on #gimp about programs that break because of vala changes
<kevin> what are you referring to as "GIMP's programming language"?
<enselic> no, Vala doesn't fit our gimprc files for example
 ketas-av Kevin
<lightningismyname> Kevin, ui programming and other non core
<enselic> for "settings files", my long-term pick is JavaScript, mu short-term pick is to stick with Scheme
<pippin> vala has the advantage of possible integrating directly with existing C gobject code
<lightningismyname> how hard would it be to introduce a vala dependancy for building gimp from git?
<enselic> my plan is to rewrite GimpTag in Vala
<lightningismyname> (vala code can generate C code for tars)
<akk> Are there vala plug-in bindings already? Maybe try translating some plug-ins into vala and see how it goes before totally committing to it?
<kevin> Perhaps you should define the problem before we trying to pick a single(?) language for the GUI and non-core stuff.
<pippin> plug-ins != core
<schumaml> IMO Vala will be "yet another obscure language those gimp guys chose" to the general public
<enselic> schumaml: it's very C#-ish
 or Java-ish if you prefer. It's not an alien syntax
<lightningismyname> ok, let's make a list: gui, image-processing core, plug-ins
<akk> Would you really want to use a language in the core that wasn't supported for plug-ins?
<lightningismyname> gui first
<enselic> before we decide on a language, we need some hands on experience
 that is, we need patches that introduces Vala to the GIMP core
 we need experimental patches for other languages too preferably
<lightningismyname> is there an auto* expert here?
 (autotools)
<enselic> I'm fairly comfortable with autotools
<schumaml> what do we want to achieve by this?
 ketas-av Kevin
<enselic> schumaml: increased productivity
<schumaml> by whom?
<enselic> manually initializing vtables is a waste of time
<lightningismyname> Kevin, what would it take to introduce vala as a git optional dependancy
<kevin> As I said, I think we need to define the problem first.
<enselic> writing GOBject C boilerplate code is a waste of time
* LightningIsMyName more than just agrees
<schumaml> so the problem is "no C!!!"
<lightningismyname> no, the problem is "no  gobject!"
<enselic> no, the problem is "stop using the wrong tool for the job"
<schumaml> I guess mayn people agree on that
<akk> The problem is "glib C bindings are a pain"
<kevin> It shouldn't take much to add vala but I don't know anything about Vala and only heard pippin talk about it.
<enselic> http://live.gnome.org/Vala
<pippin> from a more argumentative perspective, learning to read and understand GObject with C is hard; potential contributors flee before they are able to decode what code does...
<enselic> if we add Vala, it must not be optional. If it is optinal, we won't know if it's good enough
<kevin> If the problem is "how to create dialog boxes in an easier and more portable method than coding in C", the solution doesn't really depend on picking a single language.
<schumaml> is Vala supported on all of our platforms, within the standard autotools chain?
<lightningismyname> I think that in general, having 30% of gobject files on glue code is a waste of resources?
<enselic> schumaml: Vala generates C code
<pippin> schumaml: vala is just a pre-processor that generates C+gobject code passed to the c compiler
<enselic> schumaml: answer: yes
<schumaml> I'd really hate to hit a "oh, hehe, nobody uses autools on windows" pitfall
<lightningismyname> We need someone to fix the tutorial on native windows build. I can do it with enselic's nightlys, but direct git is a pain
<kevin> For many situations a dialog can be designed with GUI tools such as Glade.
<enselic> don't see it as my nightlys, it's GIMP's nightlys
 ketas-av Kevin
<kevin> or one of its variants to get a gtk builder file.
<enselic> guys, this is not about a higher level language for the GUI parts, it's for the whole core
<schumaml> btw, where are the vala builds for windows and os x?
<enselic> Few, if none, of the GIMP core objects needs to be in C
* LightningIsMyName did a mistake and started talking about gui
<enselic> if any*
<pippin> I think the PDB in GIMP and its myriad set of bindings is a problem in this regard.. what we are talking about is not yet another language with bindings to the PDB either...
<kevin> Enselic: That is why I thought we should clearly define the problem of needing to pick some language first. :-)
<enselic> Kevin: again, the problem is that one is not productive in C
<kevin> Productive doing what?
<enselic> getting your computer to do what you want
 ketas-av Kevin
<lightningismyname> Kevin,  30% glue code for gobject
<kevin> Works fine for me.
<enselic> getting the GIMP core do what you want*
<lightningismyname> and in small files, up to 60%
<pippin> if gegl ops didnt use chanting, they would be ~50 lines longer
 with a lot of repetition
<schumaml> IMO any language would be good, if we decide to make it the default for scripts and plug-ins too and drop anything else from the default installs
<lightningismyname> the good part of vala, is that it can integrate with things we have - so the transition could be gradual
<kevin> I don't run in to gObject in scripts and plug-ins. I've only seen it in GEGL.
<enselic> it seems like we agree that Vala probably is the best choice to fix the problem of too much GObject C boilter plate in the core, the next step is writing some actual Vala code
<lightningismyname> so, not hearing any real suggestions for other core languages - I'm closing this topic as:
 1. All devs take a look at vala, and try to mess with it
<kevin> The next thing is finding someone who has heard of Vala and knows something about it.
<lightningismyname> 2. Someone tries some actual patch
 ketas-av Kevin
 Kevin, I played a bit with vala
 so, any other additions on this subject? Any other languages?
<kevin> I'm already trying to get more of a handle on C# for another project I'm working on and I also am looking more at Python. I don't need to deal with another (obscure?) language right now.
<akk> Stupid question: would this be in the 2.8 timeframe, or 3.0, or what?
<pippin> javascript can work quite nicely, using the same/similar api meta data binding generation that vala relies on
<enselic> vala is low-prio
 compared to high-prio things on our roadmap
<lightningismyname> so tagging vala as a "hack if you want" on the roadmap?
* pippin is playing with the idea of resurrecting a proper example GEGL ui front-end, and would be trying to avoid doing that in C
<enselic> I don't think we should keep it on the roadmap, the roadmap should be high-level enough for users to digest
 things there should matter for users
<lightningismyname> I think there should be a "dev roadmap"
<kevin> pippin: Python and wxWindows? :
<enselic> LightningIsMyName: we don't need to put this on any roadmap imo
<pippin> Kevin: I wouldnt exclude python,. but wxwindows is out of the quesiton
<enselic> this meeting log is good enough
<lightningismyname> ok :) so let's move on
 topic 3: 2.8 - Get it out!
 Enselic did a very serious clean of bugs for 2.8
<enselic> first of all, things on the 2.6 milestone don't block a 2.8 release
<lightningismyname> Enselic, what were the things you filtered off?
 (other than 2.6 specific bugs)
<enselic> I didn't touch any bugs on the 2.6 milestone, only on the 2.8, 2.10 and 3.0 milestones
<kevin> isn't it a bit premature to change milestones on bugs set for 2.10 and 3.0?
<enselic> we have a roadmap
<lightningismyname> I'm going to ask a possibly "non popular" question - if we get 2.8 out, can we tell 2.6 only bugs (that don't appear on 2,8) to go to hell?
<enselic> our priorities are set, so I don't see how it was premature
<lightningismyname> Enselic, roadmap is the next topic on the list - I think there is place to discuss it
<enselic> LightningIsMyName: one we have released 2.8, I plan to go through the 2.6 bugs and remove them if they are not important
 LightningIsMyName: ok
<enselic> once we have*
<lightningismyname> Is it acceptable to "discard" such bugs in open source?
<enselic> with "remove" I meant "remove from milestone"
 we fix what's important and what matters, we don't have resources to fix more things than that
<schumaml> why would you do that. isn't it enough to resolve them as wontfix?
<enselic> a bug is still a bug and should be in our bugtracker
<schumaml> it is
<lightningismyname> As a part of this topic, I also mentioned helping other devs.
 Are there any devs here blocked by others, other than gui specs?
<enselic> again, we're not blocked by gui specs...
 afaik
<lightningismyname> gui specs should have recieved "blocked"
 speaking of gui, guiguru - do we/us/you have gimp interns?
<enselic> are we done with "2.8, get it out"?
<lightningismyname> not yet
 Enselic, you said you'll work on swm, right?
<enselic> I will, yes
<lightningismyname> Is there any load of the work that other devs can take off from that project?
 something which does not require deep hacking of the display?
<enselic> not really no
<lightningismyname> so yeah, we are done :P
 topic 3: Roadmap Review
 http://gimp-wiki.who.ee/index.php/GIMP_Roadmap
 all devs and *users* please take a look
<enselic> um "*users*"? ;)
<pippin> ... I think taking the separate- plug-in given on the mailinglist and making it part of GIMPs default install is a sensible thing to do ...
<lightningismyname> Enselic, roadmap is also about community :)
<enselic> pippin: yes, but for 2.10, we must not add more features to 2.8
<enselic> LightningIsMyName: fair enough
<pippin> I am referring to the statement on the roadmap that CMYK is low priority
<lightningismyname> quick question - why do we have 2.10?
<enselic> well, existing patches have high priority
<kevin> core developers *really* want JavaScript support?
<enselic> LightningIsMyName: in particular because mitch wants to adjust the libgimp API before 3.0
 ketas-av Kevin
<lightningismyname> Kevin, not me
<kevin> mitch has already stated more changes to the api are in the works.
<lightningismyname> Has mitch discussed these api changes anywhere?
 I haven't seen anything specific before it happened :P
<kevin> I don't remember much of the discussion of the changes that have been made already.
<enselic> not that I know of, but what he has done so far has been sensible
<kevin> I'm not 100% certain about the sensible part.
<lightningismyname> hehe
<enselic> Kevin: what changes would you consider controversial?
<kevin> Moving parameters out of the procedure call and making this context set of api calls seems odd and might have broken some scripts. It remains to be seen how that is going to work out.
 ketas-av Kevin
<enselic> Kevin: so you like functions with 10 parameters?
<lightningismyname> Kevin, that actually makes sense - allow adding more parameters in the future
<kevin> I am not sure if the default context values will be the same as the values previously used by scripts.
<lightningismyname> Enselic, win32 api calls can have 20 :P
 ketas-av Kevin
 Kevin, has a point there - I'm also worried about that
 I think we should add something like (gimp-context-reset "2.8")
<enselic> why are you not sure?
<kevin> I'm still not happy about the gimp-flip that used to be simple but now takes about a half dozen parameters to do a simple flip in either H or V directions.
<enselic> isn't it as simple as, instead of   f(a, b, c); it's now f(b); f(c); f(a); ?
<kevin> I have two other things I need to get done then I'll get back to reviewing pending patches for Script-Fu scripts. After the changes are commited, it will mean having to get someone(s) to test all 100 scripts to make sure the move to context didn't break something.
<lightningismyname> I think this is something we can use all the non programming users that wanted to help for
<enselic> Kevin: that situation is a good argument for making sure new code gets regression tests written too
<lightningismyname> I don't think we can write such tests for script-fu
<kevin> gimp-flip used to take drawable and a direction. Now its replaced by gimp-drawable-transform-flip-simple which requires 5 parameters
<enselic> LightningIsMyName: of course we can, it's software we're crafting
<enselic> :)
<lightningismyname> How do we write regression tests for scripts with random output?
<enselic> Kevin: so write a simple wrapper function that you use in your scripts?
<enselic> LightningIsMyName: seeding+
<enselic> ?
<kevin> A change that can affect parameters used in operations make it hard to write regression tests that would ensure scripts will still do what they used to do.
<lightningismyname> I think that adding (gimp-context-reset "2.8") for a different versions is an asap requirement for allowing such wrapper functions
 back to the roadmap, shall we?
<enselic> yes please
<kevin> Every script will need to push and pop context but IIRC, the script gets a copy of the existing context which may or may not have all the context settings as the default so a script may not work the same as before.
<kevin> Move JavaScript to low priority?
* guiguru was afk
<lightningismyname> Enselic, how can "Automatic layer boundary management" be scheduled for 3.2, and also proposed as a gsoc this year?
<guiguru> LightningIsMyName: interviewing 2 interns this week
<lightningismyname> guiguru, great!
<enselic> LightningIsMyName: the roadmap is not set in stone
 LightningIsMyName: if code is ready, we merge it in
<lightningismyname> so let's discuss it(=roadmap).
 I'll begin with 2.10
 doing a release just for bugfixes and libgimp changes?
* guiguru signs off
<enselic> LightningIsMyName: and the CMYK plug-in, and new layer modes
<kevin> Its the last chance to get in any remaining API changes before 3.0
<enselic> LightningIsMyName: and other things that will drop in
<enselic> I'm willing to move JavaScript off the milestone btw
<lightningismyname> The move to 3.0 includes integrating gegl as the core of everything, right?
<enselic> I've moved JS off the roadmap now
<enselic> LightningIsMyName: we will store image data in GeglBuffers, yes
<kevin>  Enselic: ok. You could have moved it to low priority as its also currently a GSoC idea
<lightningismyname> I was urged by many photographers whom I know, to ask this to be done earlier. Not that I can do anything about it, but I'm raising this here
 GIMP 3.4 - "Auto-anchoring of floating selection"
 can someone explain me why do we need floating selections? it's more of a trouble than anything else
<kevin> what happened to 3.2?
 ketas-av Kevin
<lightningismyname> Kevin, no objection on 3,2 goals, I think :)
<kevin> :-)
<lightningismyname> Why can't pasting be above the current layer or at the top (different shortcuts) and we simply get rid of floating selections?
 I'll lie if I say that I used these more than once in a month, and I use gimp daily as a user
<mikachu> then how would you move what you pasted, if it's not a new layer?
<kevin> How do other programs handle pasting? How would you be able to move a pasted thing in to position?
<akk> I don't even use them that often, and I spend a lot of time trying to explain them to users who don't understand them.
<lightningismyname> Mikachu, paste a new layer would paste above the current layer. then simply move
 merge would be easy
<kevin> What about automerge of the paste to current layer when tool changes from the move tool?
 ketas-av Kevin
<lightningismyname> Kevin - other ops may be needed on pastes - not sure that's the right criteria. but we can work something out
<akk> The tool won't normally be the move tool to begin with.
<schumaml> should we default to non-destructive solutions if possible?
<pippin> ,. this is ux/guiguru territory, but we do know we want to get rid of the floating selection
<akk> You might want to paste then scale, paste then levels/curves/brightness, ...
<kevin> Paste something, move tool is selected, you move it then after changing tool, it merges. It would be predictable behaviour. Just have to decide which steps push Undo's
<lightningismyname> I think getting rid of floating selection is easier than continuing to support it when moving to 3
<akk> What if you don't want it to merge?
 I always want pastes to be a new layer.
* Enselic signs off
<kevin> Paste as new layer.
<lightningismyname> bye Enselic ;)
<pippin> paste as new layer is a possible valid option
<lightningismyname> The only problem is pasting on a channel, everything else doesn't need this
<schumaml> but we're discussing 'move to new layer'
<kevin> I usually want to paste to current layer and move the paste before anchoring. I don't tend to do other ops on a floating layer.
<akk> I very often do ops on something I've just pasted
 but I can't, until I make it a real layer -- can't do anything useful while it's floating
<mikachu> i hate when you select something and do copy visible and paste, then try to new the layer, and it won't let you because you were accidentally in a layer mask
<akk> can't even see it, a lot of the time.
<lightningismyname> vote: Does anyone have a reason to use floating selections? (+1/0/-1)
 (+=keep floating layers(
<kevin> Only to allow me to move the paste to the right spot before anchoring.
<lightningismyname> Kevin, a on canvas option for merging down a pasted layer, would fix that?
<akk> If floating layers were eliminated, the Anchor button in the Layers dialog could do Merge down (which would be really handy anyway).
<lightningismyname> suggested policy: after paste, while selection == pasted segment, show merge down
<kevin> So a paste would always create a new layer above current selected layer but below any other higher layers? That might work but then that changes workflow to merge new pasted layer to active layer. Just need a simple way to merge the two.
<lightningismyname> Kevin: how do you merge them right now?
<kevin> paste, move, anchor.
 simple
<lightningismyname> anchor with keyboard or gui? both can be kept
 Ctrl+H will merge down, and button will be replaced
<kevin> Either. I usually use the gui as I don't always remember keyboard shortcuts
<mikachu> merge down isn't the same as anchor, in case the layer isn't visible
<lightningismyname> Ctrl+H: if after paste (and) selection = paste area (and) visible, merge down.
<pippin> anchor would go away, and I guess all things achiveable in the other way would still be achievable.. but..
 next topic?
<kevin> How would merge down know which layers are to be merged if multiple layers exist&gt;
<lightningismyname> "Layer effects" on 3.6 - can't we do that with filter layers?
<akk> If paste made a new layer above the current one, it wouldn't have to know anything special.
<pippin> IMO - filter effects and layer effects are two sides of the same thing
<kevin> bbiab
<akk> I thought they were both basically "make a UI for treating gegl ops as layers"
<lightningismyname> practically, right
<lightningismyname> seeing the lack of will of people to talk (and the desire to sleep for most of us), I suggest locking the meeting as discussed everything as much as we can with the current dev's attendancy
 any topic that someone wants to raise before?
<lightningismyname> I am hereby declaring this gimp developer meeting, LOCKED. Next meeting, March 28th - same time, same place
```
