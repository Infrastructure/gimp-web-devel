---
title: "Wilber Week 2023: Amsterdam, Netherland"
author: "Jehan"
date: 2023-05-22
lastmod: 2023-05-22
show_subs: false
---

Wilber Week 2023 took place between May 19 to May 28 of 2023, in
Amsterdam, Netherland. The Blender Foundation [lent us
offices](https://twitter.com/schumaml/status/1660561160880414720) for
meetings.

Two team members from Inkscape were also invited to discuss color
management.

## Planned topics to discuss

This is the list of topics we gathered and wrote down on a whiteboard in
the first day in Blender's HQ. In the end, more topics were discussed
and worked on; while in the same time, some of these topics were never
really discussed (or not enough).

* Color Management (discussion with Wayland/GNOME/Blender/Inkscape).
* Indexed mode
* GIMP 3.0 roadmap
* OpenType
* Tablet support
* GSoC
* Wayland
* Foundation
* Sharing files between GIMP and Blender
* Documentation
* Community
* Testing
* GIMP Logo
* Merch: enamel pins? t-shirt? socks? (esp. with new logo!)
* CI artifacts storage (493GiB?!)

## GIMP 3.0 roadmap
### 2023-05-22

- Reviewing the existing roadmap as posted on the developer website.
- Adding the "Documentation" item as we want to make sure documentation
  is up-to-date by the time we release GIMP 3.0.
- The rest of the roadmap is more or less right.

## Color Management
### 2023-05-22

- We want to discuss about this with Inkscape (Mc, Inkscape developer,
  will be here later today).
- Swick, Wayland developer, will join us with remote video tomorrow at
  2PM for a 4-way meeting (GIMP, Inkscape, Blender, Wayland)

### 2023-05-23

- Developers present to the meeting (see toots
  [1](https://fosstodon.org/@zemarmot/110423031427522764) and
  [2](https://mstdn.social/@tonroosendaal/110417957182203430)): [GIMP's]
  Jehan, Liam, Mitch, Niels, Øyvind, Simon, [Blender's] Nathan,
  [Inkscape's] Marc and [Wayland's] Sebastian.
- The meeting with GIMP, Inkscape, Blender and Wayland went very well.
  GIMP, Blender and Inkscape could explain some of their use cases in
  their respective area which sometimes overlap but often can be quite
  different. Printing use cases were evoked (GIMP and Inkscape), screen
  usage, web usage, but also video with workflows using OpenColorIO
  rather than ICC (Blender).
- The plans for Wayland in-progress API was exposed and looked quite
  promising as far as could be understood. Links to in-progress API spec
  was sent to everyone after the meeting for further reading and
  comments.
- Future with HDR abilities was also evoked, though this proves quite
  tricky in Wayland, especially regarding mixing of usage (e.g.
  applications using HDR while others use SDR on the same display). More
  work and thoughts are needed on Wayland side.
- The question whether to allow differences between editing and display
  usages was also raised, especially regarding HDR, but also regarding
  temporarily displaying of nearly unviewable details.

### 2023-05-24

- Much longer and impromptu discussion happened between GIMP developers
  (especially Jehan, Niels, Øyvind and Simon) and Nathan from Blender. A
  lot of stories and use cases were shared in everyone's respective
  area. It was very insightful.
- Nathan shared his work on OpenColorIO, in particular the
  [OCIOMaker](https://github.com/EatTheFuture/image_tools) software to
  create OpenColorIO configurations.
- Discussions went on on how image data can be easily shared between
  software using different workflows (ICC world and OCIO world in
  particular).

### 2023-05-25

- More [impromptu discussion happened with a video
  call](https://mastodon.social/@nielsdg/110430409972153385) with
  Sebastian (Wayland), Nathan (Blender), Niels, Jehan and Simon (GIMP).
- Some review and comments of the currently [proposed protocol
  specs](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14/diffs)
  was done live.
- Nathan raised some questions about definition of some terms (*was it
  container vs. primary vs. target color volume?*) and Niels provided
  another document for [documentation about color management and
  HDR](https://gitlab.freedesktop.org/pq/color-and-hdr/), and in
  particular its
  [glossary](https://gitlab.freedesktop.org/pq/color-and-hdr/-/blob/main/doc/glossary.md).
- Jehan raised a worry about use cases where we want to display a same
  image with different display spaces in the same time. The typical
  example is the very common case of using a Wacom tablet-display and
  another display for better color check (even calibrated, Wacom devices
  are sadly well known for not being so good at color accuracy and many
  people prefer to use a secondary display for double-checking colors!).
  Sebastian said that such a use case probably won't be dealt by Wayland
  if this is about the same surface shown on 2 displays (typically
  mirrored displays), but it can work fine if the image is shown on 2
  surfaces (non-mirrored displays and GIMP rendering the image in 2
  windows for instance, such as a work view on the tablet display and a
  "grading" view on the better display).
  This is probably enough for our own use case and many professionals,
  though the mirrored display use case (while still keeping good color
  management on both displays) would be worth discussing further in the
  future.

## Wilber Foundation
### 2023-05-22

The email and documents sent by Jehan was reviewed and commented.

A hand-raised vote was done to see who was agreeing on the current
direction taken for a possible entity. The vote was unanimous that it
seemed to be good. No blocker from anyone either.

*Note: Akkana, João and Alx were also supportive of the direction so
far, through email comments.*

Green light was given to Jehan to continue the work for our own entity.

A meeting with Ton Roosendaal, tomorrow around 4PM, was also set up. Ton
wants to explain us how Blender works and how they made it so far to
help us with our own endeavour.

### 2023-05-23

Ton made a presentation of how the Blender Foundation is organized (also
the money and organization flow between Blender Institute, Blender
Studio and Blender Foundation).

Discussions went on about what GIMP is trying to do, with some
suggestions or opinions. This meeting was interesting especially because
we could get Ton's opinion about their own experiences on various
topics, things they think they did well, less well, or are not so sure
were such good ideas now…

## Indexed mode
### 2023-05-22

Simon reminded on his experience with a company wanting to use GIMP for
carpet designing with indexed mode (knitting machines?).
He would like that GIMP improves its indexed mode and says that not that
much were missing for it to be very usable for this use case.

### 2023-05-22

- Various discussions happened around evolution on palettes. First of
  all, we have 2 concepts in GIMP: palette and colormap, which are
  basically the same thing except that one is image data and the other
  is generic GIMP data. Eventually these 2 concepts should likely be
  merged to one (a "palette" can simply be tied to an image or to the
  GIMP instance).
- It was evoked that eventually all `GimpData` should eventually become
  image data (e.g. fonts packed with an image, same as textures, project
  brushes and whatnot).
- Palette formats for program interexchange were discussed. The CxF
  format was discussed as possibly the most complete format (also
  supported by Scribus).

## Logo update
### 2023-05-22

An update of the logo was worked on by Aryeom, Ville and Chris, with
comments by Øyvind and others.

### 2023-05-23

Aryeom continued a lot to work on the Wilber icon, going through many
iterations, mostly with various inputs and comments by Ville.

### 2023-05-24

Many more iterations continued by Aryeom, who also started to write a
report about the iterations.

The day ended with a long exchange between Aryeom, Ville and Øyvind for
even further iterations.

The discussion diverged also on [ctx](https://ctx.graphics/)'s logo too.

### 2023-05-25 and 26

Work on the logo continued, with Aryeom doing more iteration and
submitting them to the team. People who gave useful inputs were mostly
Ville, Øyvind and Simon.

Points were raised on negative space, symmetries, recent trends on flat
design and simpler logos, usage on various types of media and sizes:
digital display, but also printing, silkscreen, color, grayscale or
mono-color usage, huge or very small display/print, and so on.
Some technical issues were raised like very how a very small triangle in
the logo (between the eyes) might create print issues (so work was done
to find alternatives), and so on.

Also obviously we tried to keep the original character concept and
feeling while giving it some new vibes and simplicity.

The shared opinion by the whole team was that the direction taken so far
was great and Simon noted that it is good not to take necessarily all
remarks into consideration anyway (because comity-led design will
usually lead to the most boring one).

Øyvind noted that it felt very good to work closely with an
artist/designer (Aryeom) where all sides listen well to each other,
because technical people often know of and can point out common future
technical issues, yet they don't always know well how to find proper
solutions. Whereas the good artist do find solutions to avoid these
technical issues to-come.

## Social events

Most meals were social events as the GIMP project decided to fund all
meals for GIMP and Inkscape contributors during the whole stay.

Some of the most worthwhile events are noted below:

### Lodged in a ship

GIMP team accomodation in itself was an awesome social win. We rented a
full sailing ship, more than a century old (132 years old to be exact!).

Every evening and day passed onboard was a [cool hacking
adventure](https://floss.social/@nomis@chaos.social/110425881696237462)
in itself.

### 2023-05-19

Our first meal, meeting again after 4 years of world sanitary situation,
was in a pizzeria where we got a "play dough" which [Aryeom decided to
get to make a
Wilber](https://fosstodon.org/@zemarmot/110403126751688605).

The "pizza Wilber" ended up in the floating accomodation we were in,
which Aryeom completed with a [customized pizza cardboard
display](https://chaos.social/@nomis/110406801052021728) for the whole
duration of the stay.
Schumaml even kept the work of culinary art in the end (unsure how he
will keep it from rotting!).

It was decided as an obviousness by the team that a photo of this pizza
Wilber had to be used for the next dev 2.99.16 splash screen (a "Wilber
Week 2023 edition" development version of GIMP).

### 2023-05-25
#### Podcast

We were invited on a [podcast by Pablo
Vazquez](https://video.blender.org/w/3s2qjtxXsKLqi7ngdmyfP9) around
2:30PM. Pablo interviewed Jehan, Mitch and Simon.

#### Movie night

Blender invited us to their weekly "movie night". This , they were
watching some Star Wars animated series on Disney+ (each episode by a
different studio).

Wilber/GIMP offered pizzas for everyone!

### 2023-05-26
#### end of Wilber Week report

Around midday, Jehan presented a report of what happened during the
whole week, in particular for Blender teams.

The talk was apparently of interest as many people had questions.

#### Blender "weeklies"

At end of every week, Friday around 4PM, Blender teams (especially art
teams, but a bit of devs too) showcase their work of the ending week. As
Ton Roosendaal invited us to participate, Aryeom Han showcases her work
on [ZeMarmot](https://film.zemarmot.net/).

#### Last day dinner

We invited Blender teams to join us for a nice dinner on the last day,
offered by the GIMP team. It was a nice time to exhange with the few
Blender people who came, around yummy Eritrean/Ethiopian food!

## What wasn't done/discussed
### Blender demo/workshop?

Aryeom and Jehan was hoping to get some workshops about some very
interesting (personally) Blender features, such as Sculpting and the
Grease pencil, but this couldn't happen (lack of time and finding the
right people). Some links were exchanged though.

### File exchange

Aryeom and Jehan wanted to have a in-depth discussion about better
exchanging files and working together in several creative Free Software
(think "Libre Creative Suite"). This is indeed a very common issue in
Aryeom's workflow as she needs to exchange a lot between software, and
sometimes more painfully that she'd like it.

Unfortunately this meeting couldn't happen (lack of time and so many
things happened already).

Works in this area are for instance the [link
layers](https://developer.gimp.org/core/roadmap/#non-destructive-layer-types)
where people could work on Inkscape and see the result instantly on
GIMP. We could also imagine having 2D views of 3D projects in GIMP. Or
oppositely working on patterns/textures in GIMP while they display on
Blender.

Or for 2D animation, the ability to load XCF files directly in the
Blender VSE and continue editing them in GIMP (a feature in particular
which Aryeom really wants).

And so on.

### Release video

We wanted to discuss about a possible [release
video](https://gitlab.gnome.org/GNOME/gimp/-/issues/9311) for GIMP 3.0
but it didn't happen either by lack of time.

### Videos and peertube

A topic we forgot to raise: better usage of GIMP's video channels, and
in particular a new (yet still unused) Peertube channel.

## GSoC
### 2023-05-25

14H (UTC+5:30 i.e. IST for sbdaule5): GSoC appointment with sbdaule5
(remotely through video call) and Jehan and Simon (locally at
Blender's).

It was quite productive. sbdaule5 prepared various questions which we
could answer to help them progress in their work.

### 2023-05-26

Appointment with Idriss (GSoC contributor) and Matthias Claessens
(GNOME/GTK/Pango guy) in video call, and Liam and Niels on local side.

One of the big question was about whether we should/could use Pango 2
(especially as it has some features we want, and which are not in v1),
but it turns out that its maintainers are considering it a dead end and
eventually plan on dropping Pango altogether even in GTK (*I — Jehan —
was not in the meeting and wrote this more than a week later, so I hope
I'm not mistaken in my report!*).

A possible plan which Liam is now envisioning would be to start writing
our own library to support missing features and eventually take over
when Pango is discontinued.

## Theming

Ville worked on the dark variant of the default theme because it was too
dark and contrast too strong.

The question was raised whether we'd want a "high contrast" theme so for
now the previous theme is kept as a "Darker" theme. We'll see what we do
with it.

Liam also noted that the fonts of various widgets were too small because
our default theme is overriding the system font size (with "smaller" CSS
size). So we removed the various "smaller" font size rules, though we
moved using small font sizes in the "Compact" theme. The default theme
should likely override sizes from system theme as little as possible.

Finally the possibility to propose color customization of themes was evoked.
This may be possible if we decide to limit to specific color variables (the ones
in our own Default theme) by hacking into the theme CSS contents at runtime
(just like we do right now to ask for symbolic or regular icons; as well as to
tell GTK that we prefer a dark theme).
There were some questions on whether it could not generate totally crappy
renders with the wrong combination of colors, so we also discussed of maybe
simply allowing to tweak colors within a small-ish range only.

## Bitcoin

This was one of the first topic we discussed in the accommodation-ship,
during the first weekend. Many of us were incomfortable with still
displaying bitcoin on our website, despite the various issues (social,
ecological, etc.) which were clearly raised recently.

Historically bitcoin was only created because some people asked us to
create a wallet as they wanted to donate with it, but none of the
contributors ever really cared much about crypto-currencies (even less
now). Also back then, it was mostly thought to be a system for
micro-donations as nobody foresaw it would explode this way.
Of course the GIMP project never mined nor did any speculation. And we
never accepted any other crypto-currency later on (as we were all
incomfortable because when the other currencies came out, most
crypto-currencies were already a bit of "speculative bubbles").

The last problem is that we never really knew how to use these funds
properly (a few past use were done for material and such, but very
limited). It is still a huge problem to this day. The whole bitcoin
funds is fully donations only, but we also know that there are a lot of
defiance and distrust (sometimes for good reasons, other times for bad
ones) by organizations and governments so we can't use these lightly.
Ton from the Blender Foundation confirmed this. When they did bitcoin
cash-out, they had a ton of administrative documents to fill out and
things to prove. In their own words, it lit a lot of "red lights" in
Netherland tax administration.

So what we decided is to remove the bitcoin mentions on the website. We
will also explain our decision on a news (probably the Wilber Week 2023
report news), and tell people that if they absolutely want to and still
have the wallet address (easily findable through *archive.org* for
instance), they are free to donate there, but we won't promote/display
it prominently on the website anymore.

As for existing funds, hopefully if our plans for a [Wilber
Foundation](#wilber-foundation) succeed, we will see how we can transfer
the already donated bitcoin to euro currency, probably hiring finance
jurists to make sure we do everything perfectly by the book with no risk
for any contributor. But first we need a proper entity for us to be able
to do this.

## Unit testing

The sad situation of current unit testing in GIMP was discussed a bit.
Some [plans by Jehan](https://gitlab.gnome.org/GNOME/gimp/-/issues/9339)
was discussed.

Schumaml and Liam also noted the existing of other [GUI testing
tools](https://en.wikipedia.org/wiki/Comparison_of_GUI_testing_tools)
which use the ATK and may be good candidates.

## GIMP renaming?

A discussion also took place about having an option to rename GIMP to GNU IMP,
which is a long-time topic of ours.
Jehan proposed to have 2 installers, making it a build option but Øyvind was
suggesting rather an installer option (at least for Windows), so that one may
choose at install time with a unique executable.

Similarly for the website, rather than having 2 websites duplicating the data,
it was suggested that `gimp.org` should remain the main website (any alias
domain would just redirect to it).

We don't know when this would happen, as it depends on volunteer time, but so
far, this seems to be the direction we would be heading to, regarding this
topic.
