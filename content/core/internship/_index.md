---
title: "Internship programs"
author: GIMP team
weight: 1000
---

In the past, our project welcomed a **FSF internship** (2018), several
**Google Summers of Code** (between 2006 and 2013; then in
[2022](https://www.gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/) and
[2023](https://www.gimp.org/news/2023/03/20/gimp-in-gsoc-2023/)).
We were also proposed to be in the [#BlueHats 🧢 Semester of
Code](https://communs.numerique.gouv.fr/bluehats/bluehats-semester-of-code/),
organized by a France governmental organization and a French school though it
didn't end in an internship. Other organizations should feel free to propose us
internships.

## How to participate

The rules for each internship is specific to organizers, not us. Please refer to
every program's rules.

On GIMP side, we don't care about your origins, your country, your gender, your
age or education, or anything else, so we don't have such rules of our own.
Though we do have technical rules for us to accept your project:

* [Compile GIMP](https://developer.gimp.org/core/setup/), from the `git`
  repository, before acceptance deadline (don't hesitate to ask us for help).
  This is **important**. People who skip this step generally fail and we don't
  want to spend all our time teaching you how to compile a software.
* **Get familiarized with GIMP and/or GEGL code**: fix a [few bugs and
  contribute patches](https://developer.gimp.org/core/submit-patch/)
  (this link also contains lists of bugs tagged for newcomers). It doesn't
  necessarily have to be the most complicated fixes. We mostly need to interact
  with you in a technical manner to see how you handle code review and technical
  feedback.
* **Socialize and communicate**: join [`#gimp` channel in GIMPNet IRC
  network](https://www.gimp.org/discuss.html#irc-matrix) to discuss your idea
  with developers, find a developer interested in mentoring you  and get a
  primary approval from your mentor to be.
  We are human beings too and we like to interact with contributors. Do not
  simply publish your project with whatever platform or procedure is proposed by
  the organizer. Get in touch with us first on IRC.
* Possibly **introduce your project more technically** on
  [Gitlab](https://gitlab.gnome.org/GNOME/gimp/): this can be useful to explain
  the details too long for an IRC conversation. We expect you to understand what
  you are proposing well enough to explain it to us. So do your research.
  It can also help both you and us to determine if the project is feasible or not,
  and in turn, developers can explain base technical points to get you started.

During all these steps, do not hesitate to ask questions and interact with us.
It might seem like I already said it too much, but **communication** is
absolutely the key to a succesful project.

If you have already done all this, you are ready to file the application!

People who haven't done the above don't have much of a chance of getting
accepted. People who have done all this compete against each other for the
limited resource of willing mentors and slots.

## General student requirements for GIMP related projects

What we look for in a student is:

* **Communication**
* Considerate: GIMP is a community, not a company; we value good
  people being nice to each others more than the most talented jerks.
* Initiative: we welcome questions, yet we also value independance and ability
  to make decision and researches beforehand.
* Good understanding of either GIMP or your project or both: it can just mean
  that you have the will to learn and to research.
* Knowledge and experience of coding: GIMP is mostly written in `C`, though
  depending on your project, you might have to use other languages (there are a
  few pieces of code in `C++`, plug-ins in `Python` or `Script-fu`/`Scheme`, some
  internal scripts in `Perl` or `Python`, and so on). We don't require you to be
  experts but to be willing to learn.
* Experience with `GTK`, `GLib`, `GObject` and/or `GEGL` is a plus.
* If you are implementing a graphical algorithm, then some knowledge of the
  algorithm, or at least about general computer graphics/image manipulation, is
  obviously recommended (or again a strong will to learn).

## Project ideas and past projects

The following link lists ideas which are generally considered as good this year
and are recommended for students. Note however that students are strongly
encouraged to come up with their own project — this list is not exhaustive.

Note also that a submission which is just a copy-and paste job of one of these
suggestions is not likely to be accepted.

Feel free to also browse the past list of projects realized by interns.
