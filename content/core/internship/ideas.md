---
title: "Project ideas for internship programs"
description: "Ideas for newcomers participating to internship programs"
author: "GIMP team"
abbrev: "internship-ideas"
weight: 1
---

This page is shared for events such as Google Summer of Code or similar
internship programs which GIMP might participate in, as a list of project ideas
for participants.
You may choose to implement exactly one of the proposed ideas, propose new ideas
inspired from this list, or suggest completely new projects (which is perfectly
fine if your proposition makes sense). Our
[roadmaps](https://developer.gimp.org/core/roadmap/) may also be a good source
of inspiration.

You will be selected on the quality of the proposal and on your attitude within
the context of a Free Software Community.  Also we prefer smaller projects which
end up in our main codebase, rather than over-ambitious projects which you won't
have time to finish and might end bitrotting for years.

Please also read the [main page on internship programs with
GIMP](/core/internship/).

## Implement In-Painting Tool

Category
: User Interface, Core, Tools, GEGL

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, CmykStudent

Difficulty
: Intermediate

Outcome
: implementation of the tool in GIMP codebase

GIMP has had support for in-painting (filling in an area based on the
surrounding image) for many years with the third-party
[Resythesizer plug-in](https://github.com/bootchk/resynthesizer).
There have been many requests to implement the feature as a tool directly in
core GIMP. In addition to this algorithm, there is also the GEGL operation
[alpha-inpaint](https://gitlab.gnome.org/GNOME/gegl/-/blob/master/operations/workshop/alpha-inpaint.c)
which works similarly.

Relevant discussions that would assist with implementing this feature can be found
[here](https://gitlab.gnome.org/GNOME/gimp/-/issues/8692) and
[here](https://gitlab.gnome.org/GNOME/gimp/-/issues/4762).

* Study the Resynthesizer plug-in, `gegl:alpha-inpaint` operation, 
  and other implementations
* Design a potential implementation and UI
* Improve the implementation of the algorithms as needed
* Implement the tool

## Image Segmentation Improvements

Category
: Core, Tools

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, CmykStudent

Difficulty
: Intermediate

Outcome
: New and/or improved algorithms for selecting parts of images

GIMP has many different tools and algorithms for selecting specific
sections of an image - from the standard Rectangle and Ellipse Select
tools to the more advanced Paint Select and Foreground Select tools.
Yet there are always new algorithms and methods for segmenting images.

* Find an existing algorithm or propose a new one
* Implement, optimize and test for real image processing work
on a variety of images
* Design how it should be used in GIMP
  * An additional mode in an existing tool
  * A new tool entirely

## Improving unit testing

Category
: Unit testing

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, Jacob

Difficulty
: Intermediate

Outcome
: Improved unit testing infrastructure and new unit tests

Currently GIMP unit testing framework is really outdated, adding new tests is
complex and therefore never happens. We should specify and code a proper
framework for testing GIMP features.

This implies automated tests we can run in our Continuous Integration in Gitlab
and not interactive tools (though such tools can be interesting too, as
additional process, if someone has something nice to propose).

* Port existing tests to the new framework;
* Testing all libgimp functions;
* Testing GEGL operations implemented within GIMP codebase;
* Testing plug-ins (in priority the file import/export ones, but not only);
* Testing core code;
* Testing GUI code if possible;
* Writing down the procedure to add unit tests to make it a mandatory process in
  future development.

## Fuzz testing integration

Category
: Unit testing, Security

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, Jacob

Difficulty
: Intermediate

In addition to unit testing, we would also like to build a robust
automated fuzz testing suite. Integrating a fuzzer would help us
better detect when new code could lead to a security vulnerability
or incorrect behavior in GIMP. This project would cover many aspects
of GIMP, from core code to plug-ins to public API.

* Study GIMP and determine what areas to cover in initial implementation
* Review fuzzing techniques and tools
* Design a test suite and process
* Implement fuzz testing suite

## Implement sandboxing for plug-ins

Category
: Security, Plug-ins

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, Jacob

Difficulty
: Complicated

Many features of GIMP such as image import and export are implemented
with separate plug-ins. For this project, we would like to run them in
a sandbox environment for safety and security.

This is a complex project, and requires knowledge of both GIMP's
architecture as well as extensive research into security. Mentors
would be learning alongside the student, so any interested individual
would need to be able to work well independently. Please 
[contact us](https://www.gimp.org/discuss.html#irc-matrix) to
discuss your proposal for this project.

## Improving the text tool

Category
: GEGL, color science

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Liam Quin

Difficulty
: Intermediate

Outcome
: Improvement of the text tool

**This is a project following up a few previous GSoC projects, which
deserves further work as this is a complicated topic.**

Our text tool is a bit of a UI and UX mess and deserves a proper
rewrite/enhancement project:

* Re-specify text editing and formating as well as the tool option, for existing
  features, but also adding new features for modern text editing (see also this
  [draft](https://gui.gimp.org/index.php?title=OnCanvasTextEditing));
* Add [OpenType support](https://gui.gimp.org/index.php?title=OpenTypeSupport).
* Continue previous years experiments on a new text layout library.

## Implement GEGL operations for GIMP

Category
: GEGL, image processing

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, Øyvind

Difficulty
: Intermediate

Outcome
: implementation or improvements of GEGL operations in GIMP or GEGL codebase

The migration of GIMP to use GEGL has accelerated - for some GIMP functionality
the main hurdle to migrate is having GEGL ops that can act as drop in
replacement for the core processing functionality (some ops would be desired
directly in GIMP others could likely go directly into GEGL).

For most code involved, porting to GEGL involves understanding what the current
code does; and port or reimplement it as a floating point processing operation
(floating point math often ends up shorter and more readable than the 8bit
equivalents.

There are also some filters which were ported to GEGL, but some people prefer
the old one (e.g. the Sharpen filter). It would be worth investigating the
difference and either implement the old one or improve the new one.

[Talk to us](https://www.gimp.org/discuss.html#irc-matrix) for specifics on
which operations would be a good project`.

## Implement OpenColorIO Color Management

Category
: User Interface, Core, Tools, GEGL

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: drc, CmykStudent

Difficulty
: Intermediate

Outcome
: implementation of the OCIO color management system in GIMP codebase

GIMP uses industry standard [ICC Color profiles](https://www.color.org/profiles2.xalter)
to allow users to match and maintain colors for image editing and printing.
The film industry utilizes a separate standard, [OpenColorIO](https://opencolorio.org/),
which focuses more on manipulating colors in a space rather than trying to keep them
consistent across multiple devices.

This project would involve adding support for OCIO color management in addition to the
existing system. This addition would improve user workflows for motion picture and
animation work, as well as improve compatibility with other OCIO-supporting software 
like Blender and Krita. The project would involve the following:

* Research OCIO and color management systems in general
  * The Krita manual has an 
    [excellent overview of the subject](https://docs.krita.org/en/general_concepts/colors/color_managed_workflow.html)
* Design and test a user interface
* Implement the code

## Implement GEGL Filter Browser

Category
: GEGL, User Interface

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan

Difficulty
: Intermediate

Outcome
: implementation of the feature in GIMP codebase

During the 3.0 development process, a new filter API was created that
allows script and plug-in developers to apply any valid GEGL filter they
have installed. However, GIMP doesn't not currently have a built-in browser
to easily find the name, descriptions, and properties of these filters.

For this project, you would design and develop a GEGL Browser, similar to
the existing Plug-in and Procedure Browsers. This will involve both
code development and UX/UI Design.

## Improve Non-Destructive Editing

Category
: GEGL, User Interface

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, CmykStudent

Difficulty
: Intermediate

Outcome
: implementation of the feature in GIMP codebase

As of version 3.0, GIMP now has initial support for non-destructive
editing with layer effects. Yet there is much more work to be done.
Our [roadmap](https://developer.gimp.org/core/roadmap/#non-destructive-filters)
provides some ideas for the next areas to improve, or you can propose your own:

* Studying the current implementation
* Design improvements to UI or functionality
* Implement the improvements

## Improving off-canvas editing

Category
: User Interface, Core

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan

Difficulty
: Intermediate

Outcome
: implementation of the feature in GIMP codebase

GIMP recently got the ability to view the image out of the canvas. This is still
incomplete. Among the many possible improvements:

* Being able to select off-canvas.
* Being able to see off-canvas but with an effect (e.g. dimming).
* Having various tools and features working differently when "Show All" is
  enabled.

## Improve Metadata Editor and Viewer

Category
: Metadata, Plug-in, User Interface

Project size (GSoC)
: Large (350 hours)

Skills
: C

Possible mentors
: Jehan, Jacob

Difficulty
: Intermediate

Outcome
: Improved metadata UI and codebase

Our image metadata viewer and editor could use some code review and improvements.
It currently supports only a subset of all valid metadata, and the UI could be
improved to allow easier editing and viewing of metadata.

Additionally, some image formats such as HEIC, FITs, and DICOM have custom metadata.
Another aspect of this project might be considering how to handle these in a way that
is easily extensible and maintainable.

For further inspiration, you can review open issues
[tagged with the Metadata label](https://gitlab.gnome.org/GNOME/gimp/-/issues/?label_name%5B%5D=5.%20Metadata)
in our tracker.

* Review metadata related issues and develop a plan
* Design user interface and code structure
* Implement planned changes in the metadata plug-in

## Extension website

Category
: Web

Project size (GSoC)
: Large (350 hours)

Skills
: Python, HTML, Javascript and other web technologies

Possible mentors
: Jehan

Difficulty
: Intermediate

Outcome
: New website and build scripts for continuous integration

We would want a website for our future extension platform, with very specific
criteria.
Apart from some necessary dynamic parts, we want a website as static as
possible, with generated pages when possible. GIMP is a software project, which
relies on community. We don't want to spend all our time having to maintain and
manage a website with a lot of moving parts. So we need simplicity first,
security first, with just the right amount of dynamicity. The static
website framework which we seem to want to go with the most in our
project right now is [Hugo](https://gohugo.io/).

Even though it has a "web" component, this project is also about
building a proper backend, which includes processing XML metadata
([AppStream](https://www.freedesktop.org/software/appstream/docs/)) in a
secure way (considering third-party received data as unsafe and possibly
malicious), generic static web and repository data from these
repositories, and more.

See [this
document](https://gitlab.gnome.org/Infrastructure/gimp-extensions-web/-/blob/master/docs/README.md)
for an early overview of what we are looking for.

## Convert gimp-help build system from autotools to meson

Category
: gimp-help, build system

Project size (GSoC)
: Large (350 hours)

Skills
: Python, some understanding of autotools and Makefiles

Possible mentors
: Jacob

Difficulty
: Intermediate

Outcome
: The gimp-help repository can be built using meson

More and more projects move from autotools to meson for building. GIMP itself
already uses meson as main build system.
We would want all targets in our autotools build converted to meson. The main
ones being able to build html for all languages, validate the xml files,
create the distribution packages, making pdf quickreference guides, etc.

Some guidelines for porting from autotools to meson can be found
[here](https://mesonbuild.com/Porting-from-autotools.html), and more Gnome
specific [here](https://wiki.gnome.org/Initiatives/GnomeGoals/MesonPorting).

The build system needs to function on Linux, Windows and Mac. This would need
to be integrated in our CI builds. There is an almost 2 year old
[repository](https://gitlab.gnome.org/GNOME/gimp-help/-/commits/wip/wormnest/meson)
where some work was done, but we ran into some problems due to the more strict
directory and other requirements of meson. However, there have been improvements
to meson which may make it easier now.

It is possible that you may have to write a meson module like the
[gnome one](https://mesonbuild.com/Gnome-module.html), which has functions for
handling yelp, gtkdoc, etc. Don't forget to check out the i18n module, you
may be able to use that too.
