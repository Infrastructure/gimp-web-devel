---
title: "Algorithms"
author: "The Gimp Development Team"
date: 2022-08-07
weight: 8
---

# Index of Algorithms with Commentary

Some of the algorithms used within the source can be hard to interpret without first reading a rationale. Particularly those that involve geometric reasoning.

Here's a list of algorithms for which we have written a description of their behaviour and/or implementation.
