+++
title = "Obtaining source code"
date = "2022-08-07"
abbrev = "Git"
description = "Living on the bleeding edge"
weight = 1
+++

GIMP source code lives in the [GNOME git server](https://gitlab.gnome.org) as
repositories and on [GIMP download server](https://download.gimp.org/) as tarballs.

* For most people, we recommend cloning the repos with `git` distributed
version control tool. For more information on how to install and how to use
it, we advise the very good [online documentation](https://git-scm.com/doc);
to read our brief tutorial about the basis, go [here](/core/submit-patch/#checkout-the-source).

The main GIMP repositories are:

| Module | Description |
|---|---|
| [babl](https://gitlab.gnome.org/GNOME/babl) | Pixel format conversion library |
| [gegl](https://gitlab.gnome.org/GNOME/gegl) | Generic Graphical Library |
| [gimp](https://gitlab.gnome.org/GNOME/gimp) | GIMP and the standard set of plug-ins |
| [gimp-data](https://gitlab.gnome.org/GNOME/gimp-data) | GIMP Data files (NOT related with gimp-data-extras) |

If you wish to help with core development, **you should clone at least babl, gegl
and gimp** repositories (gimp-data needs to be cloned as a submodule inside gimp).

GIMP also provides release archives (tarballs). They can be useful when
building stable releases.
* https://download.gimp.org/babl
* https://download.gimp.org/gegl
* https://download.gimp.org/gimp

---

We have other GIMP related repositories:

| Module | Description |
|---|---|
| [gimp-test-images](https://gitlab.gnome.org/Infrastructure/gimp-test-images) | Image files used for plug-ins testing on CI |
| [gimp-data-extras](https://gitlab.gnome.org/GNOME/gimp-data-extras) | GIMP old Data files such as brushes, gradients, patterns and the like; outdated plug-ins are sometimes moved there as well |
| [gimp-perl](https://gitlab.gnome.org/GNOME/gimp-perl) | GIMP Perl bindings and a bunch of nice gimp-perl scripts |
| [gimp-tiny-fu](https://gitlab.gnome.org/GNOME/gimp-tiny-fu) | GIMP Tiny-Fu, a drop-in replacement for Script-Fu |

Our websites also have git repositories:

| Module | Description |
|---|---|
| [gimp-help](https://gitlab.gnome.org/GNOME/gimp-help) | GIMP User Manual, available at [docs.gimp.org](https://docs.gimp.org) |
| [gimp-web](https://gitlab.gnome.org/Infrastructure/gimp-web) | The GIMP web site, available at [www.gimp.org](https://www.gimp.org) |
| [gimp-web-devel](https://gitlab.gnome.org/Infrastructure/gimp-web-devel) | The source of the pages you are reading right now |
| [gimp-extensions-web](https://gitlab.gnome.org/Infrastructure/gimp-extensions-web) | The potential successor of GIMP Registry. If you wish to contribute, would be cool to try the [Internship](https://testing.developer.gimp.org/core/internship/ideas/#extension-website) |

Finally, our teams have repositories:

| Team | Description |
|---|---|
| [Design team](https://gitlab.gnome.org/Teams/GIMP/Design) | The various design team repos |
| [GIMP Committee](https://gitlab.gnome.org/Teams/GIMP/Committee) | The GIMP team that decides the use of funds |

---

The following repositories are archived (development stopped and the
repository is read-only):

| Module | Description |
|---|---|
| [gimp-ci](https://gitlab.gnome.org/groups/World/gimp-ci/-/archived) | GIMP old CI system based on Jenkins, superseded by [GitLab CI](https://gitlab.gnome.org/GNOME/gimp/-/blob/master/.gitlab-ci.yml?ref_type=heads) |
| [gimp-gap](https://gitlab.gnome.org/Archive/gimp-gap) | GIMP Animation Package, a set of plug-ins that provide video editing functionality |
| [gimp-plugin-template](https://gitlab.gnome.org/Archive/gimp-plugin-template) | GIMP Plug-In Template, a starting ground for plug-in developers, currently in need of updating with regards to the use of GEGL |
| [gimp-plugins-unstable](https://gitlab.gnome.org/Archive/gimp-plugins-unstable) | GIMP plug-ins from the past, a collection of unstable and unmaintained plug-ins |
| [gimp-ruby](https://gitlab.gnome.org/GNOME/gimp-ruby) | GIMP Ruby-based scripting plug-in |
