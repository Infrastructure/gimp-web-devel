---
title: "Building GIMP documentation"
Author: "GIMP team"
Date: 2021-06-20
weight: 2
show_subs: false
---

To contribute to the documentation, you can also build the
[documentation website](https://docs.gimp.org/).

It uses `docbook` that requires some software, such as:

* gettext
* automake
* docbook2odf
* pngcrush
* pngnq
* docbook-xsl
* docbook-utils
* dblatex

Now, to build the documentation:

```sh
 git clone --depth=0 git@gitlab.gnome.org:GNOME/gimp-help.git
 cd gimp-help
 # Set this variable to your own language. It should correspond to a directory in the "po" source folder.
 export LINGUAS=en
 ./autogen.sh [--without-gimp ALL_LINGUAS="en"]
 make
 make pdf-local
```

