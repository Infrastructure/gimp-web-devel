---
title: "Building GIMP for Linux"
Author: "GIMP team"
Date: 2021-08-18
weight: 2
---

The main GIMP building instructions are at [Building GIMP](../).
This page is for Linux-specific additions. It will help installing
the dependencies for some distros, ideally with all build options enabled.

## Supported Distributions

GIMP can be built, in principle, on pretty much any GNU/Linux distribution
out there. For the **stable branch** in particular, we take great care at
not bumping the minimum requirements too often. We also try not to bump
build tools (e.g. `meson`) without a good reason. But we allow a bit more
flexibility for optional dependencies.

For the **development branch**, requirements might be higher than the
stable branch, though we also try to avoid bumping. First, we try to
never bump a dependency if the minimum version is not already at least in
[Debian testing](https://www.debian.org/distrib/packages) since it is
"*reasonably recent yet not too recent*". Also, we will try to find ways
to use new features while still keeping an old requirement (for instance
using macros to test the dependency version).

As a general advice, software contributors should anyway use distributions
providing reasonably recent packages (e.g. Debian testing, Fedora, Arch)
if you don't want to have to compile yourself too many dependencies,
which sadly is the case of distributions with longer support (e.g. Ubuntu LTS).
Ideally, in GIMP case, you should only have to build `babl`, `GEGL` and
`GIMP`. All the rest could come from the package manager.

### Debian testing

Since our <abbr title="Continuous Integration">CI</abbr> build for the
**development branch** uses Debian testing, and that we try to keep this build
as full-featured and up-to-date as possible (because it is here to warn us of
any new issue), if you use Debian testing, it might be a very good idea to look
at the `.gitlab-ci.yml` file in the GIMP source you cloned or downloaded.
In particular, look at the `deps-debian-x64` job for most dependencies
installed from the package manager.

The dependencies we build ourselves are in the [main page](/core/setup/build/#preparing-for-building).

### Debian stable

Similarly, our <abbr title="Continuous Integration">CI</abbr> build for the
**stable branch** uses Debian stable. So, look at the stable `.gitlab-ci.yml`.


## Other distributions

Regarding the distributions below, we reinforce the [advice regarding
babl and GEGL](/core/setup/build/#babl-and-gegl). Also, some things
should be considered:

### Debian flavors and derivatives using `apt`

If your distribution is Debian-based (Ubuntu, Linux Mint…), a good start
would be to install `clang` or `build-essential`. There exists also a
special command which would install all the packages necessary to build
the GIMP version supported by the distro (normally closer to stable):

```sh
sudo apt build-dep gimp
```

You may get an error about incomplete `sources.list`. If this happens,
this forum thread gives a few hints on how to properly set your system
with <abbr title="Command Line Interface">CLI</abbr> and
<abbr title="Graphical User Interface">GUI</abbr> alternative methods:
https://askubuntu.com/a/857433

### Fedora and other distributions using `dnf`

Fedora has a similar command as Debian which will install all packages needed
to build the supported GIMP version (which can, but not always, be close to
`master`, due this distro being "dev oriented" like Debian, as said above):

```sh
sudo dnf builddep gimp
```

*Note*: `builddep` is a plug-in to `dnf` so it might require getting installed
first.


## Building with Flatpak for all distributions

Most contributors simply build GIMP the "old-school" way above, nevertheless it's
possible to use Flatpak as a development environment, which is specially useful
for contributors who not use our supported distros. Here is how this can be done:

**Running, from GIMP source, `build/linux/flatpak/1_build-deps-flatpak.sh` then
`build/linux/flatpak/2_build-gimp-flatpak.sh` might be all there is to do**.
To better understand the compilation flow, you may read below.

### Install the dependencies

Install `flatpak-builder` package, available for most distributions.

Install the runtime and corresponding SDK if you haven't already:
  ```sh
  flatpak remote-add --if-not-exists --user --from gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo
  flatpak install --user gnome-nightly org.gnome.Platform/$(uname -m)/master org.gnome.Sdk/$(uname -m)/master -y
  ```

### Setting up the environment

You just need to set `GIMP_PREFIX`. Due to sandboxing, the other variables
outlined in the main page are of no use (neither at build time nor at runtime).

### Building the software

From GIMP source dir, let's build all the deps not present in GNOME runtime:
  ```sh
  flatpak-builder --force-clean --ccache --state-dir=../.flatpak-builder --keep-build-dirs --stop-at=gimp \
                  "$GIMP_PREFIX" build/linux/flatpak/org.gimp.GIMP-nightly.json 2>&1 | tee flatpak-builder.log
  ```

* Note that this will take a lot of time. So, we configured it to use `ccache` for
  future speed, and to keep built files (found in `../.flatpak-builder/build/`).
  For that same reason, the command log will be stored in `flatpak-builder.log`.

Now build and install GIMP with the [standard meson-ninja commands](/core/setup/build/#meson-build)
Just make sure of adding, before each of them, a wrapper this way:
  ```sh
  flatpak-builder --run --ccache "$GIMP_PREFIX" build/linux/flatpak/org.gimp.GIMP-nightly.json command_here
  ```

As explained above, we set only one var so additional meson options are needed:
`-Dprefix=/app/ -Dlibdir=/app/lib/`.

### Running your flatpak

Finally, you can just run your flatpak directly with:
  ```sh
  flatpak-builder --run "$GIMP_PREFIX" build/linux/flatpak/org.gimp.GIMP-nightly.json gimp
  ```

If you need to export the built files to a repo or even create a .flatpak
to test, you can make things a bit easier by running the scripts this way:

  ```sh
  # Build then exports the exact same way as our CI
  build/linux/flatpak/1_build-deps-flatpak.sh --ci
  build/linux/flatpak/2_build-gimp-flatpak.sh --ci
  # Creates .flatpak file to be installed
  build/linux/flatpak/3_dist-gimp-flatpak.sh
  ```
