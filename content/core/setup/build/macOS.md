---
title: "Building GIMP for macOS"
Author: "GIMP team"
Date: 2021-08-30
weight: 4
---

The main GIMP building instructions are at [Building GIMP](../). This page is for macOS-specific changes.

## Supported macOS API

The supported versions of macOS are noted in the
`devel-docs/os-support.txt` file of the GIMP source you cloned or downloaded.

GIMP project is favoring backward compatibility as a general rule and we really
don't like deprecating hardware when it is just a few years old. Nevertheless we
may have to bump our macOS requirement when it becomes too hard to maintain.
This is especially true as this OS has extremely low contribution rate while
core system changes happen at nearly each macOS release.
Sometimes, lowering compatibility with old versions is the only way to reduce
hardship for the few contributor(s) we have.

Therefore if you find the minimum supported macOS version too high, the
best solution would be to contribute.


## Building GIMP natively under macOS using MacPorts

The files in the [`gimp-macos-build`](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/) repo are used to build GIMP both locally and on Circle CI. Take a look in this folder.

To build locally, you need to clone the repo linked above to a folder called `project`
in your home folder. Checkout `master` for GIMP `3.0` (`2.10` is not longer being supported though it is in the `gimp-2-10` branch).


### Setting up the environment
At a minimum, you will need to install:

1. Xcode Command Line Tools:
  ```sh
  xcode-select --install
  ```
2. Also `gtk-mac-bundler`:
  ```sh
  if [ ! -d ~/Source/gtk-mac-bundler ]; then
    mkdir -p ~/Source
    cd ~/Source
    git clone https://gitlab.gnome.org/lukaso/gtk-mac-bundler
    cd gtk-mac-bundler
    make install
  fi
  ```
3. Checkout `gimp-macos-build`:
  ```sh
  cd ~
  git clone https://gitlab.gnome.org/Infrastructure/gimp-macos-build project
  cd project
  ```
4. Then, install MacPorts with:
  ```sh
  scripts/macports0_install.sh --homedirgimp3
  ```

* It is possible you will get conflicts with Homebrew, though the maintainers
  have not had this happen. If problems arise, disabling Homebrew somehow or
  taking care that login shell isn't a Homebrew shell may solve these.

### Building the software
You can't follow the instruction on the main page. Be careful of the following changes:

Dependencies are builded with the following command:
```sh
scripts/macports1_install_packages.sh
```

And GIMP:
```sh
scripts/macports2_install_gimp.sh
```

After running this, the GIMP executable will be at `~/macports-gimp${VGIMP}-${arch}/bin/` and can be run from the command line with `~/macports-gimp${VGIMP}-${arch}/bin/gimp`.

### Packaging
However, to be a full MacOS app, GIMP need to be packaged to be properly executed, so:
```sh
scripts/macports3_build_app.sh
```

The script will create a staged version of the app in: `~/macports-gimp${VGIMP}-osx-app-${arch}`,
which can be run with:
```sh
~/macports-gimp${VGIMP}-osx-app-${arch}/GIMP.app/Contents/MacOS/gimp
```

or by navigating to `~/macports-gimp${VGIMP}-osx-app-${arch}/GIMP` in finder and starting the app.

### Notarization and Distribution
This script will create a DMG file which is the "installer", if you wish:
```sh
macports4_build_dmg.sh
```

It can be found in `/tmp/artifacts/`

* `macports4_build_dmg.sh` can fail if you don't click Allow/OK in pop
up to give permissions to access Finder in order to layout the DMG.

About code signing and security entitlements, these links have useful content.
When these weren't set correctly, the application, and specifically Python
plug-ins fail to load and basically hung with no error messages.
- [https://developer.apple.com/documentation/bundleresources/entitlements/com_apple_security_cs_allow-unsigned-executable-memory](https://developer.apple.com/documentation/bundleresources/entitlements/com_apple_security_cs_allow-unsigned-executable-memory)
- [https://developer.apple.com/documentation/apple-silicon/porting-just-in-time-compilers-to-apple-silicon](https://developer.apple.com/documentation/apple-silicon/porting-just-in-time-compilers-to-apple-silicon)
- [https://developer.apple.com/forums/thread/132908](https://developer.apple.com/forums/thread/132908)
- [https://developer.apple.com/forums/thread/130560](https://developer.apple.com/forums/thread/130560)

---

Once you've used the scripts above, to build on a much faster local build loop use the following script:

```sh
scripts/quick_rebuild_gimp3.sh --fast
```

Any changes you make in the files under the `work` directory will be
wiped out the next time you ask MacPorts to build GIMP. So, if you want
to make changes to GIMP itself, once you have tested, copy them to a repo
manually (or temporarily connect this repo to your remote).

Finally, if you need to start over from scratch, run:
```sh
scripts/macports_uninstall.sh
```
