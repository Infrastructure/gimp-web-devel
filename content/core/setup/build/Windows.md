---
title: "Building GIMP for Windows"
Author: "GIMP team"
Date: 2021-08-30
weight: 3
---

The main GIMP building instructions are at [Building GIMP](../). This page is for Windows-specific additions.

## Supported Windows API

The supported versions of Windows are noted in the
`devel-docs/os-support.txt` file of the GIMP source you cloned or downloaded.

GIMP project is favoring backward compatibility as a general rule and we really
don't like deprecating hardware (unfortunately often associated to OS version,
sometimes for no good reasons, sometimes on purpose by vendors) when it is just
a few years old. Nevertheless we may have to bump our Windows requirement when
it becomes too hard to maintain both old and new <abbr title="Application
Programming Interface">API</abbr>s.

## Building GIMP natively under Windows using MSYS2

MSYS2 is a project that provides basically everything you would need
(package manager, compiler…) to build Windows software.

**Running, from GIMP source, `build\windows\1_build-deps-msys2.ps1` then
`build\windows\2_build-gimp-msys2.ps1` might be all there is to do**.
To better understand the compilation flow, you may read below.

### Install the dependencies for GIMP

1. First, if you don't have MSYS2 installed, simply run in PowerShell:

```pwsh
winget install MSYS2.MSYS2
```

2. To be able to easily use MSYS2 packages, you need to add it to PATH:

```pwsh
$MSYS_ROOT = $(Get-ChildItem HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall -Recurse | ForEach-Object { Get-ItemProperty $_.PSPath -ErrorAction SilentlyContinue } | Where-Object { $_.PSObject.Properties.Value -like "*The MSYS2 Developers*" } | ForEach-Object { return "$($_.InstallLocation)" }) -replace '\\','/'

$MSYSTEM_PREFIX = if ((Get-WmiObject Win32_ComputerSystem).SystemType -like 'ARM64*') { 'clangarm64' } else { 'clang64' }

$env:Path = "$MSYS_ROOT/$MSYSTEM_PREFIX/bin;$MSYS_ROOT/usr/bin;" + $env:Path
```

For 2.10 series, however, just open `MINGW64` shortcut on Start Menu.

3. Update the system:

```sh
pacman -Syyuu
```

If you are working in 2.10 series and the shell tells you to close the terminal,
close it and run again. That is a normal procedure for that terminal.

4. Dependencies are installed with the following command:

#### Stable branch (GIMP 2.10) dependencies

```sh
pacman --noconfirm -S --needed \
    git \
    base-devel \
    ${MINGW_PACKAGE_PREFIX}-toolchain \
    ${MINGW_PACKAGE_PREFIX}-asciidoc \
    ${MINGW_PACKAGE_PREFIX}-drmingw \
    ${MINGW_PACKAGE_PREFIX}-gexiv2 \
    ${MINGW_PACKAGE_PREFIX}-gettext-tools \
    ${MINGW_PACKAGE_PREFIX}-ghostscript \
    ${MINGW_PACKAGE_PREFIX}-glib-networking \
    ${MINGW_PACKAGE_PREFIX}-graphviz \
    ${MINGW_PACKAGE_PREFIX}-gtk2 \
    ${MINGW_PACKAGE_PREFIX}-gobject-introspection \
    ${MINGW_PACKAGE_PREFIX}-iso-codes \
    ${MINGW_PACKAGE_PREFIX}-json-c \
    ${MINGW_PACKAGE_PREFIX}-json-glib \
    ${MINGW_PACKAGE_PREFIX}-lcms2 \
    ${MINGW_PACKAGE_PREFIX}-lensfun \
    ${MINGW_PACKAGE_PREFIX}-libheif \
    ${MINGW_PACKAGE_PREFIX}-libraw \
    ${MINGW_PACKAGE_PREFIX}-libspiro \
    ${MINGW_PACKAGE_PREFIX}-libwebp \
    ${MINGW_PACKAGE_PREFIX}-libwmf \
    ${MINGW_PACKAGE_PREFIX}-meson \
    ${MINGW_PACKAGE_PREFIX}-mypaint-brushes \
    ${MINGW_PACKAGE_PREFIX}-openexr \
    ${MINGW_PACKAGE_PREFIX}-poppler \
    ${MINGW_PACKAGE_PREFIX}-python2-pygtk \
    ${MINGW_PACKAGE_PREFIX}-SDL2 \
    ${MINGW_PACKAGE_PREFIX}-suitesparse \
    ${MINGW_PACKAGE_PREFIX}-xpm-nox
```

Note that $MINGW_PACKAGE_PREFIX is defined internally by MINGW64 shell.
You don't need to manually change it.

#### Unstable branch (GIMP 2.99) dependencies

Set $MINGW_PACKAGE_PREFIX to `mingw-w64-clang-aarch64` (for ARM) or
`mingw-w64-clang-x86_64` (for x86) triplet. Then, from GIMP source, run:

```pwsh
pacman --noconfirm -S --needed (Get-Content build/windows/all-deps-uni.txt).Replace('${MINGW_PACKAGE_PREFIX}',$MINGW_PACKAGE_PREFIX).Replace(' \','')
```

This step will download a ton of packages, and may take a while.

### Prepare for building

On Windows shell, environment variables are defined this way:

```pwsh
#https://gitlab.gnome.org/GNOME/gimp/-/issues/12284
$GIMP_PREFIX = "${HOME}\_install".Replace('\', '/')

# Used to detect the build dependencies
$env:PKG_CONFIG_PATH = "$GIMP_PREFIX/lib/pkgconfig;$MSYS_ROOT/$MSYSTEM_PREFIX/lib/pkgconfig;$MSYS_ROOT/$MSYSTEM_PREFIX/share/pkgconfig"
# Used to find the glib-introspection dependencies
$env:XDG_DATA_DIRS = "$GIMP_PREFIX/share;$MSYS_ROOT/$MSYSTEM_PREFIX/share"

# Used to find programs/tools during build and at runtime
$env:Path = "$GIMP_PREFIX/bin;" + $env:Path
# Used to find introspection files
$env:GI_TYPELIB_PATH = "$GIMP_PREFIX/lib/girepository-1.0;$MSYS_ROOT/$MSYSTEM_PREFIX/lib/girepository-1.0"
```

### Building the software

You can now just follow the instruction on the main page [Building GIMP](/core/setup/build/#gimp).
Just be careful that the following option should be added at meson setup: `-Drelocatable-bundle=no`

Once installed, you can always run `gimp*` using a [.ps1 profile](/core/setup/build/#running-your-new-gimp).

## Building GIMP using Microsoft tools

Microsoft Visual Studio comes with its own C compiler know as MSVC.
Most of GIMP development is done with `Clang` or `GCC` compilers
(provided by MSYS2).

🚧 [GIMP do not build on MSVC](https://gitlab.gnome.org/GNOME/gimp/-/issues/11473).
If you wish to contribute fixes to make GIMP build with Microsoft VS, then
maintain the build so that it continues working, you are welcome to contribute.

## Packaging third-party GIMP plug-ins for Windows

Users on the Windows and macOS platforms expect software to be
packaged in a self-installing file. Even though GIMP plug-ins are
relatively easy to install, you might want to package them using an
installer such as [Inno Setup](http://www.jrsoftware.org/isinfo.php)
for the traditional version, or [Windows SDK](/resource/windows-plugin-packaging) for the Store version.
