---
title: "Warp tool: UI design info"
Author: "GIMP team"
Date: 2014-09-01
lastmod: 2021-05-14
reviewStatus: "draft from before finale specs"
---

**NOTE: this is a document salvaged from our old wiki for a tool which
is now available in stable versions. It should eventually be transformed
into a proper specification with final UX choices.**

## Idea and background

A plugin named [iWarp](http://docs.gimp.org/2.6/en/plug-in-iwarp.html) already exists in Gimp, but suffer from different troubles (small preview, uneasy manipulation). This year’s Google Summer of Code have a project to implement this behavior as a real live on-canvas tool in Gimp. This project is taken by Michael Muré (batolettre@gmail.com, irc: Bat`O on #gimp@irc.gnome.org).

Basically, this would allow users to deform an image by a series of cursor action on it. This can be seen as a sort of sculpt. See for example the case where user can move pixel with a stroke of mouse. Others behavior can also be implemented, see below.

## A quick technical sneak peek

This tool will be a complete rewrite, no parts will be taken from the plugin. The tool will works with Gegl. The plugin can be seen as a proof of concept.

Technically, this tool will work in two steps:

* a series of tools, one by behavior, act on a image-sized buffer that hold relative coordinate for each pixel. That mean that each pixel of the final image will have on this buffer a coordinate from where the final color come from.
* the final image is computed from the original image and this buffer.

The approach with a coordinate buffer instead of acting directly on the image allows to:

* Render the tool effect in a constant time, no matter the number of stroke.
* No information lost. See for example the case of a pinch done multiple times in the same area so the input pixel would collapse. If you un-pinch the area, the map-absolute operator will fetch the right pixels, with no data loss.

## Potential behavior

This technical approach gives a good liberty when implementing behavior. Following is a list a potential behavior, but other can be found and implemented.

* move: the stroke moves pixels under the cursor
* swirl (left and right) - rotary transform
* erase transform: cancel the transformation locally (ie set the relative coordinate to zero)
* pinch/concentrate
* expand
* smooth: tend to flatten the transformation

All operations are applied with a feathered "brush", so effect is largest at center and smallest at edge.

## Tool options

This tool would have as options at least:

* size of the effect
* shape of the effect (round or square ?) -i dont think offering other shapes than round provides much... [[User:AlexiaDeath|AlexiaDeath]] 20:41, 2 June 2011 (UTC)
* Hardness of the effect (see hardness in round brushes)

This part, as the others, need to be thought in details, an probably expanded.

## Undo

Undo should be achievable with this tool. What is exactly doable (one step undo ? more ?) is currently unclear and need to be discussed.

Possible way to achieve that:

* keeping copies of the coordinate buffer
  * it will quickly eat a lot of memory
* applying the stroke in reverse,
  * it lead to approximation problem
  * not all behavior are easy to undo (see for instance the cancel or smooth behavior)
* recording all the stroke applied, and recomputing the coordinate buffer when undoing
  * can take a lot of time if there is many strokes
* mixed solution: keeping "keyframe" coordinate buffer, every n stroke + every strokes, and recompute from this keyframe when undoing
* keeping modified tiles for every stroke done, and recompose a buffer when undoing
* stack one gegl op for each stroke in a gegl graph to compute the final coordinate buffer, and unstack op for undo

## Quality

Since editing the coordinate buffer and rendering the transformation are two separated process, we can render the image with different quality without affecting the deformation.

## Pressure sensitivity

Options like the size of the effect or the sharpening of the shape could be pressure sensitive.
