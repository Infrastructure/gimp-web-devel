---
title: "User Interaction specifications"
author: "The Gimp Development Team"
date: 2022-08-07
weight: 9
---

Here is a list of specifications for interaction with the graphical
interface, containing interaction logic, reasoning and rationale.
