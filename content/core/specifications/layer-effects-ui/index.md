---
title: "Layer effects UI"
Author: "Aryeom"
Date: 2023-07-12
toc: true
---

This document is the specifications for layer effects UI.

## Basic Scenario

### 1. Apply an effect
1) You have an image imported as a layer:
[![Image in a layer](Basic/apply_an_effect/1.jpg)](Basic/apply_an_effect/1.jpg)

2) Open gaussian blur (`Filters>Blur>gaussian blur`):
[![Image in a layer](Basic/apply_an_effect/2.jpg)](Basic/apply_an_effect/2.jpg)

3) result:
[![Image in a layer](Basic/apply_an_effect/3.jpg)](Basic/apply_an_effect/3.jpg)

4) Notice on layer panel:

1. A small `F`-ish icon between the lock icon space and thumbnail of layer.
2. Created a "Gaussian blur" effect over the source layer.
3. The effect also has an eye icon, lock, right click menu like normal layer.
4. When the layer effect has been created, the layer is selected (see below about editing masks).

-> Hide (or show) the layer effect list by clicking the `F`-ish icon
[![image in a layer](Basic/apply_an_effect/3close.jpg)](Basic/apply_an_effect/3close.jpg)

-> Re-open effect dialog: double click on the effect

### 2. Apply two effects
1) You have an image imported as a layer:
[![Image in a layer](Basic/apply_another_effect/1.jpg)](Basic/apply_another_effect/1.jpg)

2) Apply gaussian blur effect (`Filters>Blur>gaussian blur`):
[![Image in a layer](Basic/apply_another_effect/2_add_blur.jpg)](Basic/apply_another_effect/2_add_blur.jpg)

3) Change levels value (`Colors>Levels...`):
[![Image in a layer](Basic/apply_another_effect/3_add_levels.jpg)](Basic/apply_another_effect/3_add_levels.jpg)

4) Disable Levels:
[![Image in a layer](Basic/apply_another_effect/4closelevels.jpg)](Basic/apply_another_effect/4closelevels.jpg)

5) Disable Gaussian blur:
[![Image in a layer](Basic/apply_another_effect/5closeblur.jpg)](Basic/apply_another_effect/5closeblur.jpg)

6) Disable all effects
[![Image in a layer](Basic/apply_another_effect/6closealleyes.jpg)](Basic/apply_another_effect/6closealleyes.jpg)

## Advanced usage

### 3. Apply effect on selected area(mask)
1) Open an image and select with select tool:
[![Image in a layer](advanced/apply_on_selection/1.jpg)](advanced/apply_on_selection/1.jpg)

2) Apply gaussian blur (`Filters>Blur>gaussian blur`):
[![Image in a layer](advanced/apply_on_selection/2.jpg)](advanced/apply_on_selection/2.jpg)

3) result:
[![Image in a layer](advanced/apply_on_selection/3.jpg)](advanced/apply_on_selection/3.jpg)

Note: the selection is not removed.

### 4. Change effect's values

1) Revalue : double click on the layer effect
[![Image in a layer](advanced/change_values/3-4.jpg)](advanced/change_values/3-4.jpg)
[![Image in a layer](advanced/change_values/4.jpg)](advanced/change_values/4.jpg)

2) result:
[![Image in a layer](advanced/change_values/5.jpg)](advanced/change_values/5.jpg)

Notice on layer panel:
When it change the value, it selected but when the work revaluing finish, the selection(gray color) is on layer image. 

### 5. Merge down
1)Here is two layer effects applied on a layer image:
[![Image in a layer](advanced/merge/1.jpg)](advanced/merge/1.jpg)

2A)On the Gaussian blur, do 'Merge Down':
[![Image in a layer](advanced/merge/2A.jpg)](advanced/merge/2A.jpg)
Result:
[![Image in a layer](advanced/merge/2A_result.jpg)](advanced/merge/2A_result.jpg)

2B)On the Levels, do 'Merge Down':
[![Image in a layer](advanced/merge/2B.jpg)](advanced/merge/2B.jpg)
Result:
[![Image in a layer](advanced/merge/2B_result.jpg
)](advanced/merge/2B_result.jpg)

Note: should it be impossible to merge down layer effects on layer groups or should it flatten the layer group?

### 6. Edit effect's mask
1)Select a part of image:
[![Image in a layer](advanced/edit_mask/1.jpg
)](advanced/edit_mask/1.jpg)
2)Apply a filter (`Filters>Artistic>Photocopy`) and remove the selection:
[![Image in a layer](advanced/edit_mask/2.jpg
)](advanced/edit_mask/2.jpg)
3)Draw on mask layer with a paint tool (ex. the paintbrush tool):
[![Image in a layer](advanced/edit_mask/3.jpg
)](advanced/edit_mask/3.jpg)
Result:
[![Image in a layer](advanced/edit_mask/3_result.jpg
)](advanced/edit_mask/3_result.jpg)

Painting in the layer effect's mask is similar to painting in layer masks. The color you paint with is used as grayscale. Painting in white means 1.0 (the effect is fully applied). Black is 0.0 (effect is not applied). Intermediates are partially applied.

You can also apply effects on layer effects masks, except that the effect will be merged immediately. This is why when a new effect is created, the layer is selected (and not the layer effect): if you run an effect while the layer is selected, the new effect will be added on top, but if you run an effect while a layer effect is selected, the new effect is applied on the current layer effect mask.

Note: when a layer effect has no mask, we had discussions: should painting (while the layer effect is selected) automatically create a layer effect mask (full white equivalent)? Or should you manually create a layer effect mask first to avoid mistakes? To be tested further with the actual implementation..

### 7. Locks

* Pixel lock: cannot edit layer effect mask.
* Visible lock: cannot switch the eye icon.
* Move lock: cannot move the layer effect mask.

### 8. Organization

* You can reorder layer effects by drag'n drop. Dragging to another layer (in the same image or another) move the effect to this layer and keep its configuration.
* Copy-pasting a layer effect creates a new one with the same start configuration. You can paste it on the same layer or on another (in the same image or another).
* If you click on the layer effect mask, should the white border appear, then you can copy-paste only the mask?

## To be continued

This specification will probably be modified with further testing.