---
title: "Core Development"
date: 2022-07-24T10:46:15-0500
author: "pat the wise and wonderful"
menu: main
weight: 1
---

This _section_ contains information about GIMP internals. It is
targetted at people wishing to contribute to core development,
additionally to the [contents in "Resource Development"](/resource/).
