+++
title = "GIMP and Standards"
date = "2022-07-20"
abbrev = "Standards"
description = "Standards GIMP conforms to"
weight = 10
show_subs = false
+++

GIMP is supposed to integrate well into your workflow. Thus is
needs to interoperate with other applications and your desktop.
Standards are there to make this happen, so we try to follow
established (and sometimes even proposed) standards. Below you
will find a list with links to specifications that a GIMP
developer may find useful.

## GIMP Formats

GIMP project created its own formats for various purposes. The most well known
is the `XCF` format, which is our own project format.

* [XCF](xcf/): save format of a work-in-progress image project in GIMP.
* [GIMP Brush (GBR)](gbr/): format to store pixmap brushes.
* [GIMP Brush Pipe (GIH)](gih/): format to store a series of pixmap brushes.
* [GIMP Generated Brush (VBR)](vbr/): format of "generated" brushes.
* [GIMP Brush Pixmap (GPB)](gpb/): *OBSOLETE* format to store pixel brushes.
* [GIMP Gradients (GGR)](ggr/)
* [GIMP Pattern (PAT)](pat/)
* [GIMP Palette (GPL)](gpl/): palette format.

## Image File Formats

These are image formats supported in GIMP or not. Note that GIMP has a
policy to support every image format, however good or bad it is, at
least as an import format. For instance, being able to restore old
images in outdated and long-forgotten formats is definitely a real
use case.

[APNG](https://wiki.mozilla.org/APNG_Specification)
: Extension of the PNG format, adding support for animated images. It is
backward compatible with PNG-supporting software where it would simply
show the first frame instead of an animation. While adoption was
stalling for years, it gained more support across browsers recently.
It is not supported in GIMP yet. The biggest reason is that it relied on
a patched `libpng`, using the same namespace as legacy `libpng` and not
distributed commonly by distributions. This is apparently how it is
shipped, bundedl, in Firefox historically. Nevertheless it seems that
some browsers, like
[Chromium](https://chromium.googlesource.com/chromium/src/+/7d2b8c45afc9c0230410011293cc2e1dbb8943a7),
have managed to support it using only legacy `libpng`. It would need to
be studied.

[AVIF (AV1 Image File Format)](https://aomediacodec.github.io/av1-avif/)
: The royalty free counterpart of `HEIC`, using <abbr title="AOMedia
Video 1">[AV1](https://aomediacodec.github.io/av1-spec/av1-spec.pdf)</abbr>
image encoding and the same <abbr title="High Efficiency Image File
Format">HEIF</abbr> container format.
It supports up to 12-bit per channel, animation, HDR images and more.
GIMP uses [libheif](https://github.com/strukturag/libheif) for support,
though there are talks that moving to
[libavif](https://github.com/AOMediaCodec/libavif) could be better to
support some features, such as animated images.


[BMP](https://en.wikipedia.org/wiki/BMP_file_format)
: Legacy image format with no or low compression and very simple to
implement. There doesn't seem to be centralized format specifications
though we find various pages on the web describing its structure.
From a quick skimming, the Wikipedia page itself might be all there is
to know to encode and decode a `BMP` file.


[DDS (DirectDraw Surface)](https://docs.microsoft.com/en-us/windows/win32/direct3ddds/dx-graphics-dds)
: Microsoft format for storing data compressed with the previously
proprietary S3 Texture Compression (S3TC) algorithm, which can be
decompressed in hardware by GPUs.
It seems to be quite used in the video game industry in particular for
assets such as textures and other images mapped on 3D objects.


[GIF (Graphics Interchange Format)](http://www.w3.org/Graphics/GIF/spec-gif89a.txt)
: Describes the GIF file format, used in the GIF plug-in. GIF
is bad for various reasons, so modern programs should not use it for
anything else than supporting people's files and use cases (i.e. import
and export, as we do in GIMP). Yet it kept quite a popularity for very
long as the one image format to do small cycle animations on the web.
These last years, it slowly loses its throne to new formats such as
`WebP`, though it is still quite omnipresent.


[HEIC](https://www.itu.int/rec/T-REC-H.265)
: Modern image format, supporting up to 12-bit per channel, animation,
HDR images and more, using <abbr title="High Efficiency Video
Coding">HEVC</abbr> image encoding and the <abbr title="High Efficiency
Image File Format">HEIF</abbr> container format.
Unfortunately `HEVC` has [patent
issues](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding#Patent_holders),
unlike its `AVIF` counterpart, using `AV1`, which was created
specifically to be royalty-free by the Alliance for Open Media
(AOMedia).
This is why many Free Software distributors do not wish to package
`HEIC` support, hence why our `file-heif` plug-in runs runtime tests to
determine support for `AVIF` or `HEIC`, making it in particular able to
support only `AVIF`.
GIMP uses [libheif](https://github.com/strukturag/libheif) for support.
[HEVC specifications](https://www.itu.int/rec/T-REC-H.265) are freely
available.


[HEIF](https://www.iso.org/standard/66067.html)
: The image and animation container used for `AVIF` and `HEIC`.
As ISO standard, the specifications are not freely available. GIMP
supports this container by supporting `AVIF` and `HEIC` image formats.


[JPEG (Joint Photographic Experts Group)](http://www.w3.org/Graphics/JPEG/)
: Describes the JPEG JFIF file format, used in the JPEG plug-in. JPEG is
lossy regarding pixel data (though there is a lossless JPEG standard,
currently [unsupported by
libjpeg-turbo](https://github.com/libjpeg-turbo/libjpeg-turbo/issues/402),
the library we rely on), and only up to 8-bit per channel. Once again,
not entirely true: 12-bit support is added as an extension of the JPEG
specification and `libjpeg-turbo` has support, but it needs to be set at
compile time and only one variant — 8 or 12-bit — at the same time.
[Plans exist to provide both 8 and 12-bit support to a
program](https://github.com/libjpeg-turbo/libjpeg-turbo/issues/199#issuecomment-493764983)
but there is no timeline when it will happen.
Anyway similarly to GIF for small animated images, this format (in the
8-bit variant) is still the most popular format for photography on the
web, despite its quality issues.


[JPEG 2000](https://jpeg.org/jpeg2000/)
: New-ish format, still by the Joint Photographic Experts Group, which
supports lossy and lossless compression, high bit depth (up to 38-bits
per channel!). It compresses a bit better than legacy JPEG but this is
not really its focus.
This format clearly didn't gain a lot of popularity, at least towards
the general public. Its biggest usage though is for the cinema market as
it is the image format used for image tracks in the <abbr title="Digital
Cinema Package">DCP</abbr> container format, which is the de-facto
standard in the cinema industry these days as it replaced films reels in
theaters using digital movie projectors.
As [ISO standards](https://jpeg.org/jpeg2000/workplan.html), the
specifications are not freely available. For support, GIMP uses
[OpenJPEG](http://www.openjpeg.org/), one of the reference
implementations.


[JPEG XL](https://jpeg.org/jpegxl/index.html)
: New format, still by the Joint Photographic Experts Group, which
supports lossy and lossless compression, high bit depth, and supposed to
compress a lot better than legacy JPEG. As [ISO
standards](https://www.loc.gov/preservation/digital/formats/fdd/fdd000536.shtml#specs),
the specifications are not freely available. The reference
implementation though, [libjxl](https://github.com/libjxl/libjxl), is
what we use in GIMP.


[JNG (JPEG Network Graphics) Format](http://www.libpng.org/pub/mng/spec/jng.html)
: GIMP doesn't use this format yet but it would be nice to
extend the MNG plug-in to use it and to add a dedicated JNG
plug-in.


[MNG (Multiple-image Network Graphics) Format](http://www.libpng.org/pub/mng/spec/)
: Describes the MNG file format, used in the MNG plug-in.


[OpenRaster](https://www.openraster.org/)
: Meant to be an open exchange format for raster editing software. It is
still a bit bare and evolution has stalled these last years. GIMP
contributors originally contributed as part of its creation process.


[PDF (Portable Document Format)](https://www.pdfa.org/resource/pdf-specification-index/)
: File format for document layout. It contains vector as well as raster
images, text and more.
GIMP uses `libpoppler` for import and `libcairo-pdf` for export.
It doesn't support any of the `PDF/X` variants yet, but this will need
to happen for good printing support. One of the Free Software with good
`PDF/X` support is Scribus, making it a good complementary software to
GIMP for doing printing jobs.


[Portable Network Graphics (PNG)](http://www.w3.org/TR/PNG/)
: Describes the PNG file format, used in the PNG plug-in. GIMP
also reads patterns in the PNG file format and it stores
thumbnails as PNG images. It supports up to 16-bit per component and
proper alpha channel.
It is widely used on the web, especially for flat colored images (logos
or other design) where it can even sometimes end up in smaller files (or
close size, yet better quality) than `JPEG`, and even for photography or
complex images when quality matters more than size (thanks to its
lossless property regarding pixel data).


[PSD (Photoshop Document)](https://www.adobe.com/devnet-apps/photoshop/fileformatashtml/)
: Core file format for Photoshop project files. A public specification
is available though it is not always very complete, missing features or
details.

[Scalable Vector Graphics (SVG) 1.1](http://www.w3.org/TR/SVG/)
: Describes the SVG file format, used in the SVG plug-in. GIMP
uses SVG for import and export of paths and it also loads
gradients from SVG files.


[TIFF 6.0](https://developer.adobe.com/content/dam/udp/en/open/standards/tiff/TIFF6.pdf)
: Describes the TIFF file format, used in the TIFF plug-in. See also
the 
[Unofficial TIFF Home Page](http://www.awaresystems.be/imaging/tiff.html).


[Digital Negative (DNG)](http://www.adobe.com/products/dng/pdfs/dng_spec.pdf)
: Specifies DNG, a format, proposed by Adobe, aiming to become
a standard for storing raw data from digital cameras.
As for most other "RAW image formats", GIMP doesn't support them
natively anymore and instead promotes passing through a RAW image
developer, such as `darktable` or `RawTherapee`. GIMP has an
infrastructure system redirecting automatically RAW images to one of
these software if available, then get the result back for further
processing. Other RAW developer software may be added to GIMP by
creating proper plug-ins with a `GimpLoadProcedure` flagged with the
`gimp_load_procedure_set_handles_raw()` function.


[WebP](https://developers.google.com/speed/webp/docs/riff_container)
: Modern image format, supporting lossy and lossless compression,
animation and transparency. It is often described as a replacement for
GIF, JPEG and PNG, but not that much further as it doesn't support [high
bit depth or many color
models](https://developers.google.com/speed/webp/faq#what_color_spaces_does_the_webp_format_support).
This makes it good for web usage, but less for advanced image usages
where other recent formats make take the higher road.
GIMP supports this format through the
[libwebp](https://developers.google.com/speed/webp) reference library.

## Generic File Formats

[Extensible Markup Language (XML)](http://www.w3.org/XML/)
: Describes the XML markup language, used to store the menu layout, the
startup tips, help indices and other things.

## Metadata File Formats

[Exif (Exchangeable image file format)](https://www.cipa.jp/std/documents/e/DC-X008-Translation-2019-E.pdf)
: Widely used metadata format.

[Extensible Metadata Platform (XMP)](http://www.adobe.com/products/xmp/main.html)
: Describes XMP, a labeling technology that allows to embed
data about a file, known as metadata, into the file itself.

## Palette File Formats

The Swatchbooker project has a [very nice listing with technical information of
existing color palette formats](http://www.selapa.net/swatches/colors/fileformats.php).

Below are more details on formats currently supported (at least in parts) by
GIMP:

[GIMP's Palette Format (GPL)](/core/standards/gpl/)
: GIMP's own palette format, used to store palettes created in GIMP.

[Adobe Color Table (ACT)](https://www.adobe.com/devnet-apps/photoshop/fileformatashtml/#50577411_pgfId-1070626)
: Section "Color Table" in "Adobe Photoshop File Formats Specification"
(November 2019). Only uint8 RGB colors are supported. No specific color spaces
can be specified.

[Adobe Color Swatches (ACO)](https://www.adobe.com/devnet-apps/photoshop/fileformatashtml/#50577411_pgfId-1055819)
: Section "Color Swatches" in "Adobe Photoshop File Formats Specification"
(November 2019). RGB, HSB, CMYK, Lab and Grayscale colors are supported, with
uint16 components. No specific color spaces can be specified.<br/>
Version 2 of the format allows to set names to colors (they are currently
ignored by GIMP).

[Adobe Color Books (ACB)](https://www.adobe.com/devnet-apps/photoshop/fileformatashtml/#50577411_pgfId-1066780)
: Section "Color Books" in "Adobe Photoshop File Formats Specification"
(November 2019). RGB, CMYK and CIE Lab colors are supported, with
uint8 components. No specific color spaces can be specified. Color entries have
names.

Adobe Swatch Exchange (ASE)
: Specifications were not found online but there are various [blog
posts](https://devblog.cyotek.com/post/reading-adobe-swatch-exchange-ase-files-using-csharp)
by third-parties.
AFAWK, it supports RGB, CMYK, Grayscale and CIE Lab in floating point precision.
The palette has a name, colors have names too and each color in the palette has
its own color model (possibly mixing RGB, CMYK, Grayscale and Lab entries).<br/>
No specific color spaces can be specified.<br/>
Finally color entries have a type between "Process", "Global" and "Spot" which
seems to imply it can be used for spot colors, though there is not much
information on how and if it is for specific catalogs or vendor-agnostic. It seems
it would require additional information for this field to be actually useful.

[RIFF Palette File Format](https://www.aelius.com/njh/wavemetatools/doc/riffmci.pdf)
: Section "Palette File Format" (3-18) of the "Multimedia Programming Interface
and Data Specifications 1.0".<br/>
GIMP currently supports the "Simple PAL Format" (RGB only) but not the "Extended
PAL Format" which can additionally contain YUV and XYZ palettes.<br/>
Only uint8 RGB colors are supported. No specific color spaces can be specified.<br/>
See also [Microsoft docs on PALETTEENTRY
structure](https://learn.microsoft.com/en-us/previous-versions/dd162769(v=vs.85)).

JASC Palettes (Paint Shop Pro)
: A plain text file with header "JASC-PAL" line followed by "0100" line,
followed by number of colors (in plain text, not binary), followed by as many
lines as the number of colors. Each of these lines is a triplet of integers (in
plain text) separated by space. Line separation is Windows style `CR-LF`.<br/>
Only uint8 RGB colors are supported. No specific color spaces can be specified.

[SwatchBooker format (SBZ)](http://www.selapa.net/swatchbooker/)
: Though it is sometimes considered deprecated because the corresponding
software's development is abandoned these days, it is the most complete of all
currently supported formats.<br/>
Palettes can have a name and a description (with possible localization). Color
entries support (possibly mixed) sRGB, RGB, HSV, HSL, CMY, CMYK and spot colors
(possibly with multiple model representation for a given spot color), though the
catalog does not seem to be associated to spot color identifiers.<br/>
And finally, when relevant, a space can be attributed to color values through
embedded ICC profiles.<br/>
Sample files can be found from our [merge
request](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/843) as well as
from the [FreieFarbe project](https://freiefarbe.de/thema-farbe/software/).

## Color Management

[sRGB Color Space](http://www.w3.org/Graphics/Color/sRGB)
: Describes sRGB, a color space proposed as a standard default
color space for the Internet and other interested vendors.


[ICC Specification](http://www.color.org/icc_specs2.html)
: Specifies the profile format defined by the International
Color Consortium (ICC). The intent of this format is to
provide a cross-platform device profile format that can be
used to translate color data between device colorspaces.


[ICC Profiles In X Specification](http://www.burtonini.com/computing/x-icc-profiles-spec-latest.html)
: This is a specification for associating ICC color profiles
with X screens. GIMP 2.4 implements this proposed standard. 

## Color Compositing

Our compositing and blending algorithms are not necessarily exactly
the below W3C specifications, yet they contain base concepts and
definitions of the process behind compositing images into one combined
result:

* [Compositing and Blending Level 1](https://www.w3.org/TR/compositing-1/)
* [SVG Compositing Specification](https://www.w3.org/TR/SVGCompositing/)

## Desktop Standards

[AppStream](https://www.freedesktop.org/software/appstream/docs/)
: Metadata to describe a software. It is used massively on Linux by
software centers (such as GNOME Software or KDE Discover), which is
where they get a software name, description, screenshots, release
information and more. Our AppStream file is visible in
`desktop/org.gimp.GIMP.appdata.xml.in.in`.
We also generates the release note tab of the "Welcome Dialog" from the
release data in our AppStream file.
Finally we also use this format as metadata for the new extension format
of GIMP.


[Desktop Entry Specification](http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html)
: This document describes desktop entries: files describing
information about an application such as the name, icon, and
description. GIMP installs such a `.desktop` file.


[Desktop Message Bus](http://dbus.freedesktop.org/doc/dbus-specification.html)
: D-Bus is a message bus for the desktop. If available, GIMP
uses it to detect if another GIMP instance is already
running. In the future, GIMP might make even more use of
D-Bus.


[File URI Specification](http://freedesktop.org/wiki/Specifications/file-uri-spec)
: Specifies how URIs for normal UNIX filenames (file: URIs)
are interpreted and created. This functionality is provided by GLib,


[GNOME Human Interface Guidelines](https://developer.gnome.org/hig)
: We don't follow this spec to the word but we try to adopt as
much of these guidelines as makes sense.


[Recent File Storage Specification](http://standards.freedesktop.org/recent-file-spec/recent-file-spec-latest.html)
: Provides a standard mechanism for storing a list of recently
used files. Supported since GIMP version 2.1.6.


[Shared MIME Database](http://standards.freedesktop.org/shared-mime-info-spec/shared-mime-info-spec-latest.html)
: The shared MIME database contains common MIME types, descriptions,
and rules for determining the types of files. GIMP file plug-ins
should use the MIME types and descriptions defined here.


[ Startup Notification](http://standards.freedesktop.org/startup-notification-spec/startup-notification-latest.txt)
: Specifies a mechanism allowing a desktop environment to
track application startup to provide user feedback. GTK
provides support for this protocol.


[Thumbnail Managing Standard](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html)
: Deals with the permanent storage of previews for file
content. In particular, it tries to define a general and
widely accepted standard for this task. GIMP 2.0 implements
this standard and dropped support for the old-fashioned
<filename>.xvpics</filename>.


## Standards specific to the X window system

[Clipboards](http://standards.freedesktop.org/clipboards-spec/clipboards-latest.txt)
: Not a formal specification, but explains the consensus of the
Qt and GTK developers on how the X clipboard works.


[Clipboard Manager](http://www.freedesktop.org/wiki/ClipboardManager)
: The Clipboard Manager specification describes how
applications can actively store the contents of the
clipboard when the application is quit. This requires that a
compliant clipboard manager is running.


[Drag-and-Drop Protocol for the X Window System](http://freedesktop.org/Standards/XDND)
: XDND defines a standard for drag and drop on X11. It is implemented
by GTK.


[Direct Save Protocol for the X Window System](https://freedesktop.org/wiki/Specifications/XDS/)
: XDS defines an extension to XDND that allow users to save a file by
simply dragging it to a file manager window. GIMP 2.4 supports
this protocol.


[Extended Window Manager Hints](http://standards.freedesktop.org/wm-spec/wm-spec-latest.html)
: The Window Manager Specification is meant to unify the GNOME and KDE
window manager hint conventions.


[Inter-Client Communication Conventions Manual (ICCCM)](http://tronche.com/gui/x/icccm/)
: This spec defines the interaction between X11 clients. In
particular it talks about selections, cut buffers, window
and session management, manipulation of shared resources
and device color characterization.


[XSETTINGS](https://specifications.freedesktop.org/xsettings-spec/xsettings-latest.html)
: The XSETTINGS protocol provides a mechanism for applications
written with different toolkits to share simple
configuration settings such as double-click-times and
background colors. GTK hides this from us.

## Programming Standards

[GIMP Coding Style guidelines](../coding_style)
: Our project coding style guidelines. It explains how the GIMP source
code should be formatted.

[GNU coding standards](http://www.gnu.org/prep/standards/)
: A guide to writing portable, robust and reliable
programs. Also defines the
[GNU coding style](http://www.gnu.org/prep/standards/standards.html#Formatting).
Our own Coding Style originally derivates from the GNU Coding style.

[ISO/IEC 9899](https://www.iso.org/standard/74528.html)
: ISO 9899 is the international standard for the C programming language.
