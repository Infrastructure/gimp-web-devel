---
title: "GIMP Pixmap Brush File Format"
Author: "GIMP team"
Date: 2003-07-09
toc: true
---

**********************************************
**THIS FORMAT IS OBSOLETE AND SHOULD GO AWAY**
**********************************************

The current format for gpb files, the pixmap
brush format is very simple. What it essentially
boils down to is a greyscale gbr (gimp brush) and
a rgb pat (gimp pattern) concatenated into the same file.
