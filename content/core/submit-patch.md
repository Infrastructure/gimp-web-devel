---
title: "Submit your first patch"
description: "Bugs, bugs, bugs"
date: "2018-08-12"
lastmod: "2022-09-13"
abbrev: "Gitlab"
author: Jehan
weight: 2
---

The GIMP project uses Gitlab's [issue
tracker](https://gitlab.gnome.org/GNOME/gimp/-/issues)
to coordinate bug reports and contributed fixes. Gitlab is also used for
enhancement requests and the preferred way to submit patches for GIMP is
to make a [merge
request](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests).

On [www.gimp.org](https://www.gimp.org) you can find a document describing
[how to report GIMP Bugs](https://www.gimp.org/bugs/report.html).
If this is what you were interested in, stop there. The below
instructions will go further and explain you what to do to contribute
your own code changes.

The instructions in this page also apply for GEGL, babl and other
[repositories hosted by the GIMP project](/core/setup/git/).

## Set up your developer environment

The first step is to get the latest source code and build GIMP. For
this, you will be interested by the [Setting up your developer
environment](/core/setup/) section.

GIMP relies on various technologies, and if our tutorials are not
enough, you should try to document yourself on the proper upstream
documentation. Such technologies include:

* [git](https://git-scm.com/doc)
* [meson](https://mesonbuild.com/Quick-guide.html) (`master` branch
  only)
* Standard [autotools
  procedure](https://gitlab.gnome.org/GNOME/gimp/-/blob/b25e219c46572c35cfd5e41448b9807f077fe999/INSTALL.in#L210-214)
  (`gimp-2-10` branch only)

## Finding what to fix or implement

This may seem obvious but is apparently a big problem for many
first-time contributors: *what to work on?*

The most common advice is: if you use GIMP, did you never encounter a
bug or annoyance you wanted to see fixed? This is most often the best
tip, because then you have personal reasons to see your contribution
through and feel extra rewarded at the end (and each time you use GIMP
and the feature you fixed!). Most of us started this way, often with
very simple patches on some little issue we had.

Of course, a second good tip is to not necessarily see too big for your
first patch, or at least to contribute according to your experience. A
total beginner in development trying to implement a very important core
feature might just exhaust oneself.

If you have absolutely no such problem of your own, below is a list of
links to get you started:

* [List of Open Reports](https://gitlab.gnome.org/GNOME/gimp/-/issues)
* [List of Open Reports tagged for "Newcomers"](https://gitlab.gnome.org/GNOME/gimp/-/issues/?state=opened&label_name%5B%5D=4.%20Newcomers)
* [List of Open Bugs (excluding enhancement requests)](https://gitlab.gnome.org/GNOME/gimp/-/issues/?label_name%5B%5D=1.%20Bug)

## Checkout the source

To checkout the source of GIMP open the command line and type

```sh
git clone --recurse-submodules https://gitlab.gnome.org/GNOME/gimp
```

*Note: of course, replace the GIT URL and branch names if you want to
contribute to another [GIMP-hosted project](/core/setup/git/), such as
GEGL or babl. From now on, it will be assumed you understood you have to
tweak these instructions depending on the actual target project. This is
the last time we repeat ourselves about this.*

Depending on whether you want to contribute for the stable version only
or the development version, you will have to checkout a specific branch.
GIMP branch naming scheme is that our stable branch is in the form
`gimp-<major>-<minor>`. Development version is in the historically named
`master` branch.

So say you want to fix a bug which happens only in the GIMP 2.10 stable
branch, then checkout the branch this way:

```sh
git checkout gimp-2-10
```

On the other hand, to work on the main development branch:

```sh
git checkout master
```

For simplicity sake, in the remnant of this tutorial, we will use the
environment variable `$BASE_BRANCH` to mean either `origin/gimp-2-10` or
`origin/master` depending on the branch you chose here. Each time you
see this variable, remember it means you need to use your own chosen
base branch.

🔊 For new features or bugs which affect all branches, we request that
you first contribute them on the `master` branch. Once it is reviewed
and merged, you are more than welcome to backport the fix or the feature
to the `gimp-2-10` branch.
The reason is pretty simple: if you fix or improve something in a stable
version, then disappear, it creates a regression bug on the development
version (an issue which used to be fixed and not anymore). And that
makes more work for maintainers. So always work on `master` first for
anything touching both codebases.

Finally don't forget you should always work on latest code in your
branch, hence pull:

```sh
git pull origin
```

## Modifying the source

It's time to write code. By the way, do not forget to read our
[Coding Style](/core/coding_style)
document before writing any code. Having to spend a lot of time on style
in the review can feel a waste of time for both you and the reviewer.
Ideally you read and follow our guidelines from the start.

We advise to create a local branch for your modifications, as it will
make it easier for you to track your work and won't pollute your main
branch (which will be annoying in particular when you want to work on
several topics at once).

So first choose a branch name. It can be anything, but a naming scheme
we like in GIMP is naming it like
`wip/<your-username>/<meaningful-name>`. E.g. imagine you work on fixing
a segfault when doing *abc*, then I (Jehan) could name my branch
`wip/Jehan/fixing-abc-segfault`.

And finally I would create the branch, based on the `$BASE_BRANCH`
codebase (remember: replace `$BASE_BRANCH` with `origin/master` or
`origin/gimp-2-10` if you work for GIMP 2.10 only, as explained
[previously](#checkout-the-source)):

```sh
git checkout -b wip/Jehan/fixing-abc-segfault $BASE_BRANCH
```

This branch will now contain your source. Do changes while on this
branch, commit often (this will NOT affect other people's repository).

ℹ️  At any time, if you forget what is the name of your branch, type `git
branch` to see which branch you are working on).

To see the status of your branch (which files were changed/added/removed) run:

```sh
git status
```

In order to have a graphical user interface to browse the current state
of the branch, and to see the exact changes you made to specific files,
you can use graphical `git` frontends, such as `gitk`.

In order to commit your changes (i.e. save the current state of the
source) do:

```sh
git commit -a -m "Message"
```

*(replace "Message" with your own message, describing the current commit)*

Note that if you have a long commit message (longer than 80 characters),
then you should brake it into a summary line (of up to 80 characters)
and description lines (directly after the summary) which describe the
commit with more details (again, each of the lines should not be more
than 80 characters long).

## Finalizing your changes

When finished (and committed code one final time), since some time may
have passed and you must always submit patches based on the newest code,
run:

```sh
git rebase $BASE_BRANCH
```

Fix any merge errors and test your code again (after finishing this,
execute `git rebase $BASE_BRANCH` again if it took you long to fix the
merge errors).

## Contributing upstream
### Using Gitlab merge request workflow

Nowadays most people just create merge requests.
The process is as follow:

1. Go on our main repository page: https://gitlab.gnome.org/GNOME/gimp
2. If you are not logged-in, click the "*Sign in / Register*" button on
   the top-right. Create an account if necessary. You may either create
   an account from scratch with a login and password, or sign in with an
   existing account from another supported platform (Gitlab.com, OpenID,
   Github and Google accounts at time of writing).
3. Once logged, you will see a new "*Fork*" button (still top-right, but
   below the "*Sign in / Register*" button, right next to the "*Star*"
   one) with a number next to it. Click this button, then follow the
   instructions.
4. Once your fork is created, click the blue "*Clone*" button to choose
   one of the clone URLs (SSH or HTTPS). Copy the chosen URL. Let's say
   the copied URL is now `$MY_GIMP_CLONE`.
5. Add a remote in your `git` repository with the following command:

   ```sh
   git remote add mine $MY_GIMP_CLONE
   ```

   Note that `mine` is just a random name you set for this remote. This is
   a naming scheme I personally use to differenciate with the `origin`
   remote, which is the name I usually keep for the upstream repository.

   Now you are free to name it however you want. Some people even do the
   opposite as they fork first, then clone their own repository (which is
   called `origin` by default) and name the upstream repository something
   else (e.g. `upstream`). It's all up to your workflow choices.

6. Now push your branch to your repository this way:

   ```sh
   git push -u mine wip/Jehan/fixing-abc-segfault
   ```

   *Note: remember that `wip/Jehan/fixing-abc-segfault` is how we named
   our hypothetical branch. Yours would be called something else.*

7. Once you did all this, go again to [GIMP repository web
   page](https://gitlab.gnome.org/GNOME/gimp). Chances are that you will
   see a green banner at the top saying something like this:

   ```html
    ✅ You pushed to wip/Jehan/fixing-abc-segfault at Jehan / gimp 10 minutes ago
   ```

   Click the "Create merge request" button in the banner.

   If you don't see this green banner (possibly too much time has passed
   since you pushed), just go to the [merge
   requests](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests)
   page, click "*New merge request*", select the source repository
   (yours) and branch (`wip/Jehan/fixing-abc-segfault`), as well as the
   target repository (`GNOME/gimp`) and branch (whatever was
   `$BASE_BRANCH`), then click "*Compare branches and continue*".
8. Fill the various fields appropriately according to the [recommendations](https://gitlab.gnome.org/GNOME/gimp/-/blob/master/.gitlab/merge_request_templates/default.md?ref_type=heads).
9. Click "*Create merge request*".
10. Done! You made your first contribution, congratulation!

See also: [Gitlab documentation on creating merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

### Upload a patch in a bug report

As an alternative method, we still accept simple patch files, as was
done since the dawn of times (or nearly! 🤭).

The command to create a patch in `git` is extra easy:

```sh
git format-patch $BASE_BRANCH
```

This will give you patch files in the form of git commits with names
like `0001-plug-ins-Use-string-literal-as-format-in-metadata-p.patch`.

For each one of your commits which is not in `$BASE_BRANCH`, a patch
file will be created in the directory (the patch files will be numbered
by the order of the commits). If you want to avoid polluting your
current working directory with many patch files, you can use the `-o
<output-folder>` option (replacing `<output-folder>` with the folder
where you want the patch files to be created of course).

Alternatively if your fix is made in several commits, you may prefer to
have a single file. It is possible by redirecting the output to `stdout`
then to a file with this command:

```sh
git format-patch --stdout $BASE_BRANCH > fixing-abc-segfault.patch
```

Now, all you have to do is to upload the patch file(s) to the bug report
discussing the bug you were fixing or feature you were implementing.

## Some general advices on git usage

Some very useful info about git usage can be found at the [Coding Style](/core/coding_style/#multiple-or-single-commits).
