---
title: "Contributing to the main website: www.gimp.org"
Author: "GIMP team"
Date: 2016-10-25
toc: true
weight: 100
---

GIMP community does not only need developers. We welcome all sorts of
contributions: design, testing, bug triaging, translators… and of course
website improvements!

Even the website you are reading right now can be [contributed
to](/about/), for instance to propose new developer tutorials or
improving existing ones.

This page is about the [main website](https://www.gimp.org/), often
abbreviated <abbr title="Www.Gimp.Org">WGO</abbr>, which is the central
point to get information about GIMP.

## Possible improvements

Some of the many things which were cited across the years are:

* Porting the website to Hugo (work started by Patrick David). We
  already lowered the barrier-to-entry when [moving to Pelican in
  2015](https://www.gimp.org/news/2015/11/22/20-years-of-gimp-release-of-gimp-2816/#new-website)
  and believe the website is due for a new facelift. It could help
  entice users to generate new content.
* More tutorials on WGO for specific useful workflows.
* Add some community news: events organized by community members, better
  advertizing the events that the core team itself organizes…
* Add nice "behind the scene news": interviews, meeting users…
* More contents updates in general: more book listings, nice images,
  tutorials, news…

## Base website principles
### Static content

As all GIMP websites, WGO is static-only, or to be more accurate
server-side static. Indeed we don't use any server-side dynamic language
such as PHP or Python; our web server only serves pre-built HTML, CSS
and Javascript files. This has the following advantages:

* The website is much faster as our web server hosts and directly serves
  existing files. No on-the-fly compilation (or cache system), no
  database, no interpreter running.
* The website is much safer. We can't have any SQL injection (or any
  kind of code injection), private data leaking (nothing is private on
  our website).
* Our website is saner and doesn't require GDPR handling or cookie
  warnings: we don't gather anyone's private data, nor do we want
  to. There is simply nothing to warn about with a static website.

The only dynamic part of the website can be for Javascript, which we
keep to a minimum. In particular, **a very important rule is that the
website should perfectly work without Javascript!**
For instance, we use JavaScript to redirect to a "thank you" page with
some info and links after the "Download" button was clicked. With JS
disabled, the download button would still work (only the redirection
would not happen, that's all).

### Simple content format and separated from style

The content for the website is decoupled from the presentation layer.
One method of doing this is to house all of the content in an easy-to-use plain text format such as Markdown (or some combination of Markdown + html).

A benefit of separating the content from the presentation layer is portability of the content.
It becomes easier later on to re-adapt the content layer to a new presentation layer as needed.
This also helps to lower the barrier to entry, as any given piece of content is a self-contained folder of plain-text markdown (or html), as well as the associated assets needed.

This was already the case on WGO before 2015, then it got even further removing the presentation information as well in the Pelican port. The upcoming Hugo port will simplify even further.

Consider, for example, this historical snippet from the tutorial "GIMP Quickies":

```html
<!--#include virtual="/includes/wgo-xhtml-init.xhtml" -->
<title>GIMP - GIMP Quickies</title>
<link href="styles.css" rel="stylesheet" type="text/css"/>
<!--#include virtual="/includes/wgo-look-feel.xhtml" -->
<!--#include virtual="/includes/wgo-page-init.xhtml" -->

<h1>GIMP Quickies</h1>

<h2>Intention</h2>

So you installed GIMP on your computer, congratulations!
GIMP is a very powerful image manipulation software, but don’t let that intimidate you.
Even if you don’t have time to learn advanced computer graphics, GIMP can still be a very useful and handy tool for quick image modifications.

It is my hope that these few examples will help to solve those small, quick modifications that you may need to apply to an image.
Hopefully this will lead to learning even more powerful image editing capabilities that GIMP is capable of as well.

For quick access, these are the four main points I’ll cover in this quick tutorial:

* <a href="#scale">Changing the Size (Dimensions) of an Image (Scale)</a>
* <a href="#compress">Changing the Size (Filesize) of a JPEG</a>
* <a href="#crop">Crop an Image</a>
* <a href="#transform">Rotate or Flip an Image</a>
```

There are still some Server Side Include statements, as well as having to build out a valid HTML document while writing.
This may cause extra burden on someone willing to contribute new content, but not comfortable with writing directly to HTML.

This same content, represented as Markdown, would look like this:

```markdown
---
title: "Gimp Quickies"
author: "Pat David"
...
collection: tutorial
---

# GIMP Quickies

## Intention

So you installed GIMP on your computer, congratulations!
GIMP is a very powerful image manipulation software, but don’t let that intimidate you.
Even if you don’t have time to learn advanced computer graphics, GIMP can still be a very useful and handy tool for quick image modifications.

It is my hope that these few examples will help to solve those small, quick modifications that you may need to apply to an image.
Hopefully this will lead to learning even more powerful image editing capabilities that GIMP is capable of as well.

For quick access, these are the four main points I’ll cover in this quick tutorial:

* [the Size (Dimensions) of an Image (Scale)](Changing)(#scale)
* [the Size (Filesize) of a JPEG](Changing)(#compress)
* [an Image](Crop)(#crop)
* [or Flip an Image](Rotate)(#transform)
```

### Important website features

Here are a list of things that are the most important to users visiting
the page (as per original discussion between Schumaml and Patdavid
during the Pelican redesign):

* The project name. Main branding: GIMP - The GNU Image Manipulation Program
* How do I get it (Download)?
* What is it (The Free &amp; Open Source Image Editor)?
* How do I use it (tutorials/docs)?
* News
* Get Involved
* Donate

Heare are the ideas Patdavid proposed moving forward with:

* The main page should serve these distinct purposes (*roughly in order of importance*):
  * Clearly state the name of the project
  * Clearly state the description of the project
  * Clearly provide a means for getting the software
  * Display recent news items/articles/blog posts
  * Describe the program in high-level

  Basically, anyone landing on the main page should have no doubt what they are looking at, what the program is for, and how to get it.
* Try to keep the existing URL structure intact as much as possible.
  * The only way it might not be the same going forward could be due to removal of some pages that could be considered extraneous.
  * Even then, the pages should probably still exist to not break anything (just soft-remove them by no longer linking to them from anywhere).
* Consolidate page information onto a single page as appropriate.
  For example *Screenshots* and *Features* might be better off having their information be presented/duplicated onto the main page.

These concepts are still true to this day.

## Current website organization

Looking at WGO right now shows a very simple breakdown of content type into two main types:

* News Entry
* Static Pages

These are the primary page types we can find.

### News Entry/Article Post

News entries are relatively simple, as they've primarily been the
snippets of updates shown in reverse chronology order on the [landing
page of WGO](http://www.gimp.org/).

For years, there have been talks to sub-categorize these a bit, in
particular with:

* Core team events: we already announce events such as Libre Graphics
  Meeting or other [team meetups](/conferences/) but not always as well
  as we could. A "Future event" section could also be interesting as a
  calendar where people can check upcoming events in their area.
* Community events: we could/should encourage the broader community to
  post about their own events involving GIMP.
* Community-building: apart from release news and events, it would be
  awesome to get community-building articles, such as interviews (e.g.
  the interview of [mitch](https://www.gimp.org/news/2017/03/01/an-interview-with-michael-natterer-gimp-maintainer/)
  and of [schumaml](https://www.gimp.org/news/2017/05/15/an-interview-with-michael-schumacher-gimp-administrator/)
  written a few years back by Jehan. Other than contributors interviews,
  we could have GIMP users interviews (the ones doing really great stuff
  with GIMP or working professionally in particular).

### Static Pages

The rest of the pages can be considered simple static pages. These include:

* Features
* Release Notes
* Downloads
* Documentation
  * User Manual (*see below*)
  * Tutorials
  * Books
  * FAQ
* Get Involved (*see below*)
* Donations

There are a few other pages that follow the same conventions as a simple static page.

About the **User Manual**, we should simplify redundant data which is
handled already by the dedicated [GIMP
Documentation](https://docs.gimp.org/) project and redirect people
there.

About the **Get Involved** page, we should also simplify redundant data
and redirect interested people on the current [Developer
website](https://developer.gimp.org/).

It would also be in our best interests to make sure that the URL's
remain the same as they've always been, or to carry the correct 301
redirects as needed to avoid losing any indexing-sauce that has been
built up. So any change we make to a page, which involves removing or
changing an existing URL should be accompanied by proper redirections.

😎 [**Cool URIs don't change**](https://www.w3.org/Provider/Style/URI)!

## Contributing to the website
### Building the website to test locally

Our current website uses Pelican for the time being. The
[README.md](https://gitlab.gnome.org/Infrastructure/gimp-web/-/blob/testing/README.md)
file in the repository is pretty clear on the process to quickly build
it for local view, which makes it extra easy to contribute.

It should be even easier when we move to Hugo, but we are not there yet!

### Content writing

Most content is available in the `content/` directory. You want to
propose tutorials, add books to our listing, propose interesting news,
it's (nearly) all there, written in [Markdown](http://daringfireball.net/projects/markdown/)
format. Just have a look and propose [merge
requests](https://gitlab.gnome.org/Infrastructure/gimp-web/-/merge_requests).

### Redesign

There are a few possible benefits from conducting a redesign at regular
interval:

* When it helps to better organize the content
* Modern Look
* A chance to consider usability and ease of access for users (and to improve it)
* Lowering the barrier to entry for new contributors/contributions.
* Responsive Design

### Technical improvements

From an architecture standpoint, the general idea is to separate content from presentation.
This is already how the previous pre-2015 website worked, how the Pelican website works and how the Hugo version will too.

Here are some technical concepts:

* Possibly having role-based permissions against different areas of a git repository (admin, presentation, content, tutorials, news, etc...) ([git sub-modules](http://www.git-scm.com/book/en/v2/Git-Tools-Submodules) were mentioned as a way to manage this).
* The proposed idea is to separate WGO content into more user-friendly discrete pieces, such as individual [Markdown](http://daringfireball.net/projects/markdown/) text files.
* These text files (+assets) are passed through a processor to be turned into static HTML assets.
* These static HTML assets are served to users.

Overall, the approach from Cristobal (who originated the discussions on
the website redesign in a [2015
email](https://mail.gnome.org/archives/gimp-web-list/2015-January/msg00000.html))
considers the problem one of creating
**Application**/**Content**/**Presentation** layers.

#### Application - Role based access (git submodules)

There was some original proposition to utilize [Git submodules](http://www.git-scm.com/book/en/v2/Git-Tools-Submodules) as a means for segregating content/admin/presentation from each other within a repository.

It appears that this may be something that can be separately addressed later in the process of implementing a new site.
Moreover we probably don't have the volume yet for this to be
interesting, but it could become in the future, if we ever wanted to
give maintainership to tutorials to someone, to news to someone else,
and so on.

#### Presentation

With the content abstracted further to plain text files, the presentation data for WGO can be managed separately from the content.
This allows not only parallel workflows but the possibility of drastic modification to the presentation layer without any undue burden on the content creators.

The presentation layer is also something that is usually defined in terms of the static site generation (but can be modified/customized as needed).

For pixls.us, I had used a bit of Adam Kaplan's [Grid](http://adamkaplan.me/grid/) as a basis for extending the site.
This has the benefit of being a responsive design with various possible screen sizes accounted for in @media CSS.
This is not a requirement, of course, but if doing a redesign we might as well consider responsive and mobile-first options.

### From Pelican to Hugo

Some work is already in progress to move our static web engine from
Pelican to Hugo.

Anyone is welcome to contribute to this effort.
