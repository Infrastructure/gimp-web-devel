---
title: "Giving or obtaining developer access in Gitlab"
date: 2023-12-30
author: Jehan
weight: 1000
show_subs: false
---

Maintainers
: *Send this link to anyone whom you consider suitable for obtaining
developer access to the GIMP project.*

Contributors
: *Please read carefully this page about what it means to have developer
access.*

## The responsibility which comes with push access

The rules of having repo access are basically:

0. Keep a **linear history** in the repo (no merge commits) and nice
   **readable commit messages**.<br/>
   Merge commits can make reading the `git log` a pain. Merge workflows
   are better for huge projects with a lot of contributors where the
   main branches are only for merging other public branches. We may get
   there eventually (yet not with the current team organization).

1. We **care a lot about the [coding style](/core/coding_style/)**.

2. **Don't break things**: don't push on the main branches (i.e. `master`
   and `gimp-2-10` in particular) known broken code, or worse, code
   which doesn't even compile on one platform.

3. If you want to propose ground-breaking changes, obviously discuss
   this with us first and propose merge requests for discussions and
   reviews.

4. For big fixes or changes, also propose merge requests/patches for
   review, especially when you are not sure yourself, or if you want a
   review, or the change has chances to be controversial.

5. If you see obvious typos, bugs or absolutely non-controversial
   changes, and you are sure enough that your code is bug-free, you
   might push directly (even on code you don't maintain).

6. Some recent contributors just do merge requests all the time, even
   for small fixes and/or when they push it themselves soon after the CI
   validated it. It's not a bad idea because it allows to test on the CI
   before pushing. It's up to you. Older contributors with a lot of
   experience are usually more daredevils (we come from a time back when
   we had no CI anyway) but it's probably not a bad idea for newer
   contributors not to do this and open merge requests for everything.

7. For code you are maintaining, you may feel free to push directly even
   complex code (though of course, even here, if something is probably
   controversial, we'd recommend discussing with others first to make
   sure everyone is onboard! Moreover even on your own piece of code,
   you are welcome to ask for opinions!). 

As for how to define "*code you are maintaining*": if it's big chunk of
code you implemented yourself originally (some big new feature, etc.)
with others' agreement, you are still probably the one who knows it
better. Or when you repeatedly fixed some code nobody has touched for
years, you'd be the one who knows it better too. If you are unsure, ask
others. In any case, you are also always allowed to make MRs just because
you want to discuss code changes, as a base for discussion (as said
before).

More generally: if you are unsure about anything, don't hesitate to ask
others.

We obviously have other small rules here and there, not always perfectly
written down (more like "*passed down verbally from generations of
developers to the next*", which is often [easier on
IRC](#irc-is-an-important-tool)), which you'll probably learn too as you
go.

## The main rule: be careful, caring, nice and open-minded (a.k.a. the main reason why someone would lose git access)

About quality of code: "bug-free" is obviously a stretch. Nobody is ever
sure to do bug-free code. It's mostly about how much you trust your own
changes. And making errors is fine as a general rule. We all do, that's
how we learn. Nobody will chastise you for this. Just when you realize
an issue or if someone tells you, please fix it. 😉

So the pure "skills" are not what we care the most. We won't remove git
access or be angry for a technical mistake (well hypothetically if you
were to do a **lot** of them all the time, we could *temporarily* remove
access until one learns better, I guess; though this case never
happened).

We care the most about the human beings and to keep a nice atmosphere
within the core team. If ever we were to ban someone from the team,
nowadays it would be for toxic behavior rather than technical reasons.

Best developers and maintainers are the ones who are also able to listen
to others, discuss their and others' ideas and possibly review their own
ideas if they are told it's a mistake (which it might be, or not! Maybe
your idea or implementation is the right one, but at least you can hear
being told it's not, then argue politely and maybe it will end up you
are right or you might be persuaded you weren't; basically the
definition of being "open-minded").

We are a community software, we want to be able to discuss nicely. We
don't want mean contributors (you have the right to strong technical
opinions as long as you understand discussions, other people's right to
their own strong — even though possibly wrong according to you 😛 —
technical opinions and you understand compromises too!) or contributors
who push random stuff when others told them it's wrong and should not be
pushed without getting to an agreement.

We don't believe in the "*we have the right to be assholes as long as we
are good or because it's a technical discussion*" logic. GIMP team is a
place where people can speak their mind *while* being nice, and it's a
place where everyone should feel welcome.

## IRC is an important tool

IRC is nice for team work if you can use it at least from time to time.

By the past, we had some difficulty to communicate with some
contributors whom we could never really easily discuss with, so it's
often more comfortable if contributors are on IRC (at least when you
push big code or propose merge requests). You don't have to be there all
the time (and don't have to speak most of the time, if you don't want
to). But being there regularly makes it easier to communicate.

It's not necessarily a blocker point. We have some active contributors
who are never on IRC. Yet they answer quickly on Gitlab and are very
nice to work with, even though only through Gitlab. So it still works
fine. Let's just say that when there are human-relationship issues, the
fact of not even being able to discuss more easily may make it harder
but if one is able to make it work with Gitlab only, it's fine too.

## How to

Please follow [the procedure to request developer accounts](https://handbook.gnome.org/infrastructure/developer-access.html).

In the report where you ask for access, please name `@Jehan` (or other
maintainers who proposed you the developer access) and say that I asked
you to get access. I will get a notification and will add a comment
vouching for you.

⚠️ **Big Warning** ☢️ : you will be able to push to all software in the
`GNOME/` namespace. Obviously it doesn't mean you should just push
commits everywhere. Well if you try, you would probably soon end up
banned by GNOME admins anyway, so don't play jokes! 😛 Even GIMP
maintainers don't push anything to GTK, GLib or other repos directly.
We still upload patches and merge requests like everyone else. Only push
directly where project maintainers told you it was fine to.

As a conclusion, be aware it's a big responsibility to get git access
which also requires to understand the basics of git to not make a
mistake.

**Welcome to the few with git access!**

--------------------------

# Bonus sections

## When do we propose developer access? (a.k.a.: GIMP is organized as a community)

As per the [be
caring](#the-main-rule-be-careful-caring-nice-and-open-minded-aka-the-main-reason-why-someone-would-lose-git-access)
rule, we propose access to people with whom we had good feedbacks and
who interacted nicely with existing contributors. If one started
contributing by insulting others or with ad-hominem remarks, chances
are that they won't get developer access anytime soon.

We also try and determine people who seem to have good team attitude
towards deciding things as a group. Also we appreciate people who seem
reliable and autonomous (the more trusted contributors with access there
are, the more time saved for everyone).

What you will soon discover is that GIMP is more than a Free Software,
it is a [Community, Free
Software](https://www.gimp.org/news/2021/12/31/gimp-2021-annual-report/#building-a-lovely-team).
While we do have maintainers, our role is to organize and protect the
project, but most work is done quite horizontally.

So when we give you keys to the core community, the biggest
responsibility is not the code. It's actually not to mess up the human
side of the project.

## Please don't ask for developer rights yourself

*Note: this section is for people stumbling onto this page. If you were
directed here by a maintainer, you don't have to bother reading this.*

Usually you don't need to ask for developer rights. A maintainer will
tell you if they feel you are apt to get them.

Note that in some cases, if you don't get such rights even after
contributing for what seems for a long time, it does not necessarily
mean you were deemed unworthy. There may be a few reasons. For
instance, we have some contributors who have contributed patches for
years, but only once every few months. In such a case, we don't feel the
need to give them `git` rights (even more as these rights expire after a
few months unused so it'd be more cumbersome than anything else).
Going through merge requests is simpler for such long-term small-load
contributors.

Also some contributors have contributed more specifically to one of our
websites or to a packaging repository, or some other side repository. In
such a case, giving access to the main repository isn't needed.

In other cases, we have sometimes willing contributors who take longer
to use `git` right (often messing their merge request branches for
instance) and this can be a problem. We don't want such errors happening
on the real repository, this could be a huge issue. We expect people to
take utter care of our source repositories and it's a big
responsibility. So you need acceptable git understanding/knowledge to
get access.

Of course in some cases, some people have a harder time with
collaboration and try to change everything to their liking. While we
understand the reason (it would help you a lot, sure!) and welcome the
efforts of the many contributions, we expect core developers to
understand what community efforts mean. In particular being able to
accept refusal and discussions, also to not wipe out other people's
code, replacing it with your own, without clear consensus. For such
contributors, staying on a merge request basis with review every time
still is the best.

The last reason for refusal is non-team player, in particular when you
get to be mean, or worse, insulting to others and thinking only your
code and ideas are the best. We cannot condone this. While we will still
accept good patches, this will stay on a merge-request basis, and if
things get too far of course, even this may be cut short.
