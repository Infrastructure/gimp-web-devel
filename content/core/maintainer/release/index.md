---
title: "How to do a GIMP release"
description: "A check-list for doing a GIMP release"
date: 2022-10-18
author: Jehan
---

This is the process for maintainers doing a GIMP release.

1. Create a [bug report in
   Gitlab](https://gitlab.gnome.org/GNOME/gimp/-/issues/new?issue[title]=Release%20GIMP%20X.Y.Z)
   titled "*Release GIMP X.Y.Z*" where `X.Y.Z` is to be replaced with the
   pending release version.
   <!-- https://gitlab.gnome.org/GNOME/gimp/-/issues/new?issue[title]=Release%20GIMP%20X.Y.Z&issue[description]=%3C!-- Please fill me following indications at https://developer.gimp.org --%3E -->
2. Prepend the description by notifying all people of interest for a release,
   copy-pasted from below:

   ```markdown
   *Note: CC list of people to notify when preparing a release. If you wish to
   help for further release, e.g. with testing, news writing or other
   contributions, please come forward in comments to be added in the next CC
   list.*

   * Release manager in charge of this version: <!-- Add your Gitlab handle -->
   * GIMP maintainers: @Jehan @mitch
   * GEGL maintainers: @ok
   * GIMP docs maintainers: @Wormnest @ciampix
   * Core Developers: @Jehan @mitch @ok @Wormnest @cmyk.student @dnovomesky @lb90 @bootchk @nielsdg @programmer_ceds @schumaml
   <!-- Add any relevant developer which made specific patches in the current release cycle. -->
   * Designers: @aryeom @rangelovd
   * Packagers:
       - Windows installer: @jernejs @brunolopesdsilv
       - Microsoft Store: @jernejs @brunolopesdsilv
       - macOS DMGs: @lukaso @brunolopesdsilv
       - Flatpak: @Jehan @hub @HarryMichal @dnovomesky @brunolopesdsilv
   * User testing:
       - Windows: @sevenixvii (also managing testers on Discord) @ShiroYuki_Mot @dnovomesky @Uzugijin @hmartinez82 (Aarch64, MSIX, Windows S mode…)
       - macOS: @Niels @bootchk
       - Flatpak: @dnovomesky @ajonsson
   * News writers: @cmyk.student @Jehan
   * Proofreaders: @drc @barefootliam @patdavid
   * Social networks: @patdavid @barefootliam @schumaml @Jehan
   * Various other core team: @akkana @nmat @ellestone
   ```

3. Fill the description with the contents below:
   {{< details "**Copy this contents as report description**" "content/core/maintainer/release/report.md" >}}

4. This is a check-list for doing a GIMP release, in markdown syntax. You should
   check each step in Gitlab as you, packagers or testers make them happen.

5. Once it's all checked, close the report. Release is done. Enjoy!
