Release check-list:

- [ ] Prepare a `<release>` tag with `<description>`, [as documented](http://developer.gimp.org/core/maintainer/appstream/),
      within `desktop/org.gimp.GIMP.appdata.xml.in.in` for the upcoming version.
- [ ] [Announce a string freeze on GNOME discourse with the `gimp` and `i18n`
      tags](https://discourse.gnome.org/new-topic?title=gimp-x-y%20branch%20string%20freeze%20until%20YYYY-MM-DD&category=Applications&tags=gimp,i18n)

     It serves as an announcement to both developers and translators.

     Mention:
     * the planned release version;
     * the frozen branch name;
     * the expected release date (end of freeze) (plan for a couple of weeks at
       least. No translatable strings must be touched during this time);
     * this bug report URL.
- [ ] Add the next version milestone on [GIMP's Gitlab](https://gitlab.gnome.org/GNOME/gimp/-/milestones) per
      [instructions](https://developer.gimp.org/core/maintainer/gitlab-milestones/).
      Start moving features which won't make it in time to the release to this next milestone.

     *Note: we don't close immediately the milestone of the just-released
     version as we may still assign some reports to it, but older
     milestones might be closed (up to what feels the most needed for
     organization of reports).*
- [ ] Entering source freeze a few days before release, except for obvious bug
      fixes. Ask macOS and Flatpak packagers to make test packages based on
      current `HEAD` of the release branch.
- [ ] Make sure that changes which deserve some tour get the appropriate "demo"
      attribute in `desktop/org.gimp.GIMP.appdata.xml.in` [as
      documented](https://developer.gimp.org/core/maintainer/appstream/).
- [ ] Verify with babl and GEGL maintainer(s) if new versions are required for
      GIMP release.
- [ ] Verify dependencies if needed and sync with packagers to make sure the
      latest babl and GEGL released versions in particular are used. The latest
      babl and GEGL tag can be determined this way:

     ```sh
      dep=babl
      repo="https://gitlab.gnome.org/GNOME/${dep}.git"
      tag=$(git ls-remote --tags --exit-code --refs "$repo" | grep -oi "${dep}_[0-9]*_[0-9]*_[0-9]*" | sort --version-sort | tail -1)
      echo Please use babl tag: $tag

      dep=gegl
      repo="https://gitlab.gnome.org/GNOME/${dep}.git"
      tag=$(git ls-remote --tags --exit-code --refs "$repo" | grep -oi "${dep}_[0-9]*_[0-9]*_[0-9]*" | sort --version-sort | tail -1)
      echo Please use GEGL tag: $tag
     ```
- [ ] [Create a pipeline](https://gitlab.gnome.org/GNOME/gimp/-/pipelines/new?var[CI_COMMIT_TAG]=1) to test packages:
      choose the **branch** we plan to release and add the variable
      `CI_COMMIT_TAG` to any value (it will simulate a build with a tag,
      which is characteristical of a release, creating most packages).
      
      *Note: the test build will likely take up to 1 hour, especially
      because of the Windows installer step. And of course, if you ever
      discover any build or run into issues, the time will be multiplied.
      It is advised to take this in consideration. This test step should
      be run well in advance, even the day before.*
- [ ] Packagers should post links to test packages in comments:

     ⚠️  We remind that these are unstable builds and even for packages
     which usually come with auto-updates (such as Windows MSIX or Linux
     Flatpak), it isn't the case here. Therefore after thorough testing
     and commenting, we highly recommend not to forget to uninstall the
     test package. ⚠️
     - [ ] Windows installer:
     - [ ] Windows .msixbundle (install `pseudo-gimp.pfx` as a CA trusted certificate; password: "eek"; uninstall the cert with Windows Certificate Manager after testing):
     - [ ] macOS DMG for Intel:
     - [ ] macOS DMG for Silicon:
     - [ ] Linux Flatpak (run with `flatpak run org.gimp.GIMP//test` after install):
     - [ ] Linux AppImage for x86_64:
     - [ ] Linux AppImage for aarch64:
- [ ] Testers: please test the above packages and comment. ⬆️
- [ ] Until the date specified in the announcement, get bug fixes applied which
      don't modify strings. Re-do the above 2 steps as many times as needed.
- [ ] Check that you have working SSH access to `master.gimp.org` and that you
      are a member of the `gimpftpadmin` group. If not, ask Michael Natterer,
      Michael Schumacher or Jehan for assistance.
- [ ] Check that `master.gimp.org` has enough space to upload the release and
      to place it into the download area. If not, make place or ask Michael
      Natterer, Michael Schumacher or Jehan to do that.
- [ ] Check that you have admin access to [`gimp` module on Gitlab](https://gitlab.gnome.org/GNOME/gimp/)
      and commit access to the [`gimp-web` module](https://gitlab.gnome.org/Infrastructure/gimp-web),
      or that someone can do the changes for you.
- [ ] Check if the following files need to be updated and commit changes:
     - [ ] `NEWS`
     - [ ] `authors.xml` (and the generated `AUTHORS`)
     - [ ] `README`
     - [ ] `INSTALL.in`
- [ ] Did you update the splash image yet? Every point release (even micro one)
      must have a new splash. Development releases don't require a splash screen
      update. See the [requirements](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/README.md?ref_type=heads#requirements)
      for the new image.
- [ ] Verify that our `meson.build` depends on stable `babl` and `GEGL`
      versions, then sync up with their maintainers for new releases if needed.
- [ ] If ever the actual release date evolved and is different from the
      planned date, update the `"date"` in the `<release>` tag of the appdata in
      `desktop/org.gimp.GIMP.appdata.xml.in`
- [ ] Check again our Continuous Integration builds with CI_COMMIT_TAG: all jobs must be
      successful because we should not release with code known not to build in some conditions.
     - [ ] The following jobs should be triggered by CI_COMMIT_TAG:
          | Stage: dependencies                          | Stage: gimp                                  | Stage: distribution                        |
          | -------------------------------------------- | -------------------------------------------- | ------------------------------------------ |
          | deps-debian-x64                              | gimp-debian-x64                              | sources-debian<br>dev-docs                 |
          | deps-debian-a64<br>deps-debian-x64           | gimp-debian-a64<br>gimp-debian-x64           | dist-appimage-weekly                       |
          | deps-win-a64<br>deps-win-x64<br>deps-win-x86 | gimp-win-a64<br>gimp-win-x64<br>gimp-win-x86 | dist-installer-weekly<br>dist-store-weekly |
     - [ ] Check in particular:
          * the job `dist-appimage-weekly` should contain 2 .AppImage files, one for each arch, with
            1 .zsync file and 2 checksum files per .AppImage;
          * the job `dist-installer-weekly` should contain a .exe installer and
            2 checksum files;
          * the job `dist-store-weekly` should contain a .msixbundle and
            2 checksum files;
          * the job `sources-debian` should contain a tar.xz tarball and
            2 checksum files;
          * the job `dev-docs` should contain 3 tar.xz tarballs (one for GIMP,
            one for GEGL and one for babl docs) and
            2 checksum files per tarball.

     If these steps work fine, we are ready to tag and publish.

- [ ] Bump the version number in `meson.build` to the release version.
      Releases always have an **even micro version**.
- [ ] Bump or reset `gimp_interface_age` in `meson.build` according to
      [libtool instructions](https://developer.gimp.org/core/maintainer/versioning/#libtool-version).
- [ ] Commit the version bumps only and push. Since there was no code change
      since the last CI check, the CI should build fine once again. Make sure of
      it.
- [ ] Tag the release and push the tag:

     ```sh
       git tag -s GIMP_2_x_y
       git push origin GIMP_2_x_y
     ```

     All release tags are signed in order for the authenticity and
     origin of the release to be publicly verified.

- [ ] Gitlab will run a new CI pipeline specifically for the tag.
      Once it is done, you will find the artifacts listed on the
      detailed CI_COMMIT_TAG check above.
- [ ] Ask macOS and Flatpak packagers to make release candidate packages
- [ ] Packagers should post links to release candidate packages in comments:
     - [ ] Windows installer:
     - [~] Windows .msixupload (is sent directly to MS Store)
     - [ ] macOS DMG for Intel:
     - [ ] macOS DMG for Silicon:
     - [ ] Linux Flatpak (run with `flatpak run org.gimp.GIMP//test` after install):
     - [ ] Linux AppImage for x86_64:
     - [ ] Linux AppImage for aarch64:
- [ ] Testers: please test the above packages and comment. ⬆️
- [ ] Windows packager: sign the Windows installer and send the link as comment.
- [ ] Merge the flatpak release merge request;
- [ ] Publish dist tarballs and binaries:
     - [ ] Make sure that the script `tools/downloads/upload-release.py` in the
           `gimp-web` repository has not been tampered with by running a `git
           log`. This file should only ever be modified by maintainers with
           access to `master.gimp.org` only.
     - [ ] Make a dry run of `tools/downloads/upload-release.py` and look at the
           output carefully, making sure nothing will break. The option `-h`
           will tell you about options.
     - [ ] Now run `tools/downloads/upload-release.py` passing in the released
           version, then follow the instructions.
     - [ ] Once the script ends, double check that everything looks fine on the
           download server: https://download.gimp.org/
     - [ ] The script will also tell you how to modify
           `content/gimp_versions.json` (under "STABLE" for stable releases or
           "DEVELOPMENT" for dev releases). You may tweak details if needed
           (such as the main release date which may have been a bit earlier,
           etc. Use the tagging date).
- [ ] Bump the version number in `meson.build`.
      Except on the release commit, git code always have an **odd micro version**.
      For RC builds, add `+git` at the end of the previous RC release
      version.
- [ ] Bump `gimp_interface_age` in `meson.build`.
- [ ] Commit the version bumps only and push.
- [ ] Checkout the `testing` branch of the 'gimp-web' module:
     - [ ] Create a news items for the release in `content/news/` and commit it.
     - [ ] Run `ninja authors.md` in GIMP repository. This will generate
           the file `authors.md`. Move it to `./content/about/authors.md` on
           the 'gimp-web' module and commit it.
     - [ ] Commit and push the changes to the `testing` branch, the web
           server should then update itself soon.
     - [ ] Go to https://testing.gimp.org to verify the changes.
     - [ ] Proofreaders should validate the news.
     - [ ] Do not merge to `master` branch yet. We usually wait for a few
           packages to be available (often at least the Flatpak and
           Windows installer) and published to the download server when
           relevant.
- [ ] Update the developer website to make sure the API docs is fine.
    - [ ] Verify the [testing developer website](https://testing.developer.gimp.org/).
    - [ ] In particular, you might want to check that [babl, GEGL and GIMP API references](https://testing.developer.gimp.org/resource/api/) were updated.
    - [ ] If needed, make the necessary fixes on [`gimp-web-devel` repository](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/),
          branch `testing`. You may force an update of the testing website by
          creating a [pipeline](https://gitlab.gnome.org/Infrastructure/gimp-web/-/pipelines/new?ref=oscp&var[TRIGGER_JOB]=aarch64-testing_developer)
          for branch `oscp` with variable `TRIGGER_JOB = aarch64-testing_developer`.
    - [ ] If the `testing` branch is fine, merge it into the `master` branch and
          push.
    - [ ] In order not to wait for the daily developer website update, force an
          update by creating a [pipeline](https://gitlab.gnome.org/Infrastructure/gimp-web/-/pipelines/new?ref=oscp&var[TRIGGER_JOB]=aarch64-developer)
          for branch `oscp` with variable `TRIGGER_JOB = aarch64-developer`
- [ ] Add a [new Gitlab release](https://gitlab.gnome.org/GNOME/gimp/-/releases/new),
      linking the recently pushed git tag (`GIMP_X_Y_Z`), the related milestone,
      the news on `gimp.org` website and each package (source tarball,
      installers, docs tarball…).
- [ ] Announce the release on gimp.org and send release announcements:
     - [ ] Check out the gimp-web `master` branch and `merge` the
           `origin/testing` branch.
     - [ ] Push the changes, the web server should update itself soon.
     - [ ] Verify the changes on https://www.gimp.org
     - [ ] Announce on:
          - [ ] Mastodon
          - [ ] Twitter
          - [ ] Facebook
          - [ ] BlueSky
          - [ ] The discourse thread created in the beginning.
