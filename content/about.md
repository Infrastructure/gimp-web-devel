+++
title = "About this Site"
description = "About this Site."
date = "2022-08-07"
author = "The GIMP Development Team"
menu = "footer"
+++

This is the website for development of or with [GIMP](http://www.gimp.org/).

Under the hood it uses the [Hugo framework](https://gohugo.io/) and
`markdown` syntax.
If you want to help us writing contents for `developer.gimp.org` or
improving the website, contribute to the `git` repository
[`gimp-web-devel`](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/).

Any **recent contents since 2023-05-10** is licensed under [Creative Commons
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
](https://creativecommons.org/licenses/by-sa/4.0/), unless otherwise stated.

[![Creative Commons Attribution-ShareAlike 4.0 International
License](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)

Older contents might come from older sources (e.g. former GIMP wikis or
developer website) and may not have explicit licenses. You should not assume any
license for these. The [`git`
history](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/) is the only
absolute reference on whether a file contents is reusable or not.
