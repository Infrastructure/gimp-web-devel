---
title: "Resource Development"
date: 2022-07-24T10:46:15-0500
author: "pat the wise and wonderful"
menu: main
weight: 2
---

This _section_ contains information to create resources for GIMP or use
it as a development platform.

For instance you can write plug-ins to customize your workflow, run GIMP
non-interactively from scripts to process images, and so on.
