+++
title = "How to write a plug-in"
description = "Writing GIMP plug-ins"
date = "2022-07-20"
weight = 2
show_subs = false
+++

GIMP is powerful, but what makes it even more powerful is its
extensibility. In this series of articles, I will focus on programmatic
extensibility through code (plug-ins, scripts…).

You may also be interested into the [How to write a
filter](../writing-a-filter/) tutorial which is yet another way to
extend GIMP programmatically.

## Tutorial for GIMP 3.0 Plug-ins

These tutorials will take you through the general plug-in architecture
only. There is much more to the abilities of our libraries which you
should discover by reading more advanced documentation or the [API
reference](https://developer.gimp.org/api/3.0/libgimp/index.html).

0. [C plug-ins - basic](tutorial-c-basic)
1. [C plug-in GUI](tutorial-c-gui)
2. [C - complex GUI](tutorial-c-complex-gui)
3. [Calling a plug-in from another (PDB)](tutorial-pdb)
4. [Python plug-ins](tutorial-python)
5. [Script-Fu plug-ins](tutorial-script-fu)
6. [Calling GEGL Operations from plug-ins](tutorial-gegl-ops)
<!-- TODO: 7. Extensions -->

We do suggest for you to read through the whole parts of the tutorial,
in the given order, even if you are only interested by specific part.
For instance if you want to write a Python or Script-Fu plug-in, the C
tutorial stays of interest. You may then skip the "**[Code]**" sections
and read at least the "**[Theory]**" parts.

<!-- ## Historical documentation

For historical purpose, you may also be interested into the below older
tutorials. Note that their contents is quite outdated, yet are kept as
archive of the evolution of GIMP plug-in infrastucture.

* [GIMP 2.0 API reference](/api/2.0)
* Talk about [GIMP plug-in programming](http://www.home.unix-ag.org/simon/gimp/guadec2002/gimp-plugin/html)
  by Simon Budig at GUADEC in Sevilla

A tutorial for the GIMP 2.x series:

* [How to write a GIMP plug-in, part I](1/)
* [How to write a GIMP plug-in, part II](2/)
* [How to write a GIMP plug-in, part III](3/)

Even older documentation:

* [Writing a GIMP Plug-In](http://gimp-plug-ins.sourceforge.net/doc/Writing/html/plug-in.html) by Kevin Turner for GIMP 1.x series (2000).
* [GIMP Python module Documentation](https://www.gimp.org/docs/python/index.html) by James Henstridge (1999)
* [GIMP Script-Fu changes in GIMP 2.4 (migration guide)](https://www.gimp.org/docs/script-fu-update.html)
-->
