+++
title = "How to create a C or C++ plugin that will work on MacOS and workarounds"
date = "2022-10-02"
abbrev = "macos-plugin-publish"
description = "GIMP Reference Manuals"
+++

When plugins are written in C, C++, or compiled into binaries, special actions need to be taken in order for them to work on MacOS. Specifically, they need to be codesigned and notarized. This is due to security restrictions imposed by Apple in more recent versions of the OS.

If a plug-in isn't signed and notarized, recent versions of MacOS will quarantine the plugin and ask the user to delete it.

### GIMP code signing

Here is the code that does code signing for GIMP on the Mac in case this is helpful. This is what the authors of plugins would need to do. Look at

- [Importing signing certificate](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/-/blob/a24d63a5a1db4994b3710ca6c336cac61b2bb25e/.circleci/config.yml#L185-195)
- [Signing libraries and application](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/-/blob/a24d63a5a1db4994b3710ca6c336cac61b2bb25e/package/build.sh#L133-151)
- [Hardening entitlements](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/-/blob/master/package/gimp-hardening.entitlements)
- [Notarization](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/-/blob/master/package/notarize.sh)

An Apple Developer account and the right credentials are also needed.

### Workaround for unsigned plugins (if you have admin rights) on Catalina and later Macs

1. Download a plugin.
1. Open the archive and copy the contents to `~/Library/Application\ Support/GIMP/2.10/plug-ins/`
1. Do the following to remove Apple security constraints (this is at your own risk). It will ask for your administrator password:

```shell
cd ~/Library/Application\ Support/GIMP/2.10/plug-ins/ # assuming it was installed here
sudo xattr -rd com.apple.quarantine *
```

**Note** The final `*` might have to be more specific for this to work.
