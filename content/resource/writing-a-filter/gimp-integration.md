+++
title = "Integrate with GIMP"
description = "How to get a Seamless Integration of your Filter in GIMP"
date = "2025-02-15"
author = "Jehan"
+++

As you saw already, GEGL operations actually integrates already pretty
well with GIMP by default:

* As long as you set a "title", your action will be searchable in the
  ["Search Actions"](https://docs.gimp.org/3.0/en/gimp-help-search-and-run.html)
  dialog;
* Your filter will also be made available in the list of filters
  available in the `Tools > GEGL Operations…` dialog.

Furthermore you noticed how GIMP generates a very nice dialog to tweak
the parameters you made, and you get all the nice features of core
filters.

In the future, we may show how to get an even more complex GUI using the
meta keys.

## Adding a Menu Item

One of the next step though, especially for discoverability, would be to
have your own item in menus! Well this is possible.

The call to `gegl_operation_class_set_keys()` which you call for every
operation has the great advantage that you can create any key you want.
It is a generic key-value system.

GIMP in particular will recognize a few custom keys:

* `"gimp:menu-path"`: the value has the same syntax as the
  [`gimp_procedure_add_menu_path()`](../tutorial-c-basic/#code-adding-plug-in-metadata)
  function used on plug-ins. And as you may have guessed, if your
  plug-in has this key, GIMP will add a plug-in item at the suggested
  path.
* `"gimp-menu-label"`: again this is equivalent to `gimp_procedure_set_menu_label()`
  for plug-ins. This will override the operation's title, if set, for
  the menu item's label. Note that the operation's title will still be
  used as dialog title.

Here is an example of using these keys:

```C
  gegl_operation_class_set_keys (operation_class,
                                 "title",           "Hello World Point Filter",
                                 "name",            "zemarmot:hello-world-point-filter",
                                 "categories",      "Artistic",
                                 "description",     "Hello World as a Point filter for official GIMP tutorial",
                                 "gimp:menu-path",  "<Image>/Filters/Hello Worlds/",
                                 "gimp:menu-label", "Point Filter",
                                 NULL);
```

With these set, recompiling then restarting GIMP, you will find a new
submenu "Hello Worlds" under the "Filters" menu. And as expected, it
will contain a menu item "Point Filter" (you could consider that since
the submenu label is "Hello Worlds", repeating "Hello World" in each
menu item feels redundant for *menu title*, yet you'd still want for the
*full title* to contain the "Hello World" mention) which will run our
point filter operation.  You could do the same for the meta operation we
created, so that your `Filters > Hello Worlds` submenu would now be
populated by our 2 demo filters.

In other words, there is no need anymore to wrap code on GIMP side to
have them visible in menus! Any third-party developer can now specify a
menu path and label for their filter to appear as!

* Back to ["How to write a filter" tutorial index](../)
* Previous tutorial: [Hello World as a Point Filter Operation](../point-filter-operation)
* Next tutorial: [Helper Tools for Developers](../helper-tools)
