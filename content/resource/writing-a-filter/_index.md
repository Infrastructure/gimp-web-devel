+++
title = "How to write a filter"
abbrev = "Writing GEGL plug-ins"
description = "Write a GEGL operation (a.k.a. GEGL plug-ins)"
date = "2022-07-20"
weight = 2
show_subs = false
+++

GEGL filters are also called “GEGL operations” or “GEGL plug-ins”. In
GIMP, we often just call them "filters" or "effects". You may see any of
these naming interchangeably, they usually refer to the same thing.

Third-party filters will appear automatically in the list of filters
available from the “GEGL Operation” dialog, or in the [Search
Actions](https://docs.gimp.org/3.0/en/gimp-help-search-and-run.html)
dialog. It is also possible to make them available in menus.

What makes filters very powerful is that they offer automatic [on-canvas
previews](https://docs.gimp.org/3.0/en/gimp-filters-common.html), with
split view, and they are not specific to a version of GIMP (your GEGL
operation will be usable in GIMP 2.10, 3.0, 3.2 or even any other
software using GEGL if they have a generic operation interface).

You may consider a filter as a black box to which you give an input
image (a [`GeglBuffer`](https://developer.gimp.org/api/gegl/class.Buffer.html)),
or sometimes even none or several input images, various arguments, and
which returns an output image.

## Tutorial for Filter's Development

This tutorial will explain how to create your own operation (for you, it
won't be a black box anymore!).

Note that GEGL operations must be compiled into a dynamic library which
GEGL will load. This means that filters are usually implemented in C (or
some other compiled language which can use the GEGL API).

<!-- *Note: some part of the tutorial heavily borrows from the 2022/2023
tutorial by Liam Quin about GEGL plug-ins: [Using GEGL
Plug-Ins](https://barefootliam.blogspot.com/2022/10/gegl-plug-ins-for-gimp-part-one-using.html),
[GEGL
Graph](https://barefootliam.blogspot.com/2022/12/gegl-plug-ins-for-gimp-part-two-gegl.html)
and [Writing C
Plug-Ins](http://barefootliam.blogspot.com/2023/01/gegl-plug-ins-for-gimp-part-three.html).* -->

* [Architecture and How to Install Filters](architecture-install)
* [Demo: your first Hello World as a Meta Operation](meta-operation)
* [Demo: your first Hello World as a Point Filter Operation](point-filter-operation)
* [Integrate with GIMP](gimp-integration)
* [Helper Tools for Developers](helper-tools)
