+++
title = "GIMP 2.10 Plug-In Template"
abbrev = "Plug-In Template"
description = "Don't start from scratch"
date = "2022-07-20"
weight = 5
+++

The GIMP Plug-In Template is an empty GIMP Plug-In that does
nothing. It also has a GUI that allows to control how it achieves
this task. But best of all, it comes with a complete setup for
[autoconf](https://www.gnu.org/software/autoconf/),
[automake](https://www.gnu.org/software/automake/),
internationalisation and all these things you never wanted to know
about.

Thanks to the GIMP Plug-In Template you don't need to worry about
all these things. Just download the tarball, add some magic image
manipulation routines and voilà, your own GIMP plug-in.

Grab the latest release from
[download.gimp.org/pub/gimp/plugin-template/](https://download.gimp.org/mirror/pub/gimp/plugin-template/gimp-plugin-template-2.2.0.tar.gz).

*Note: this template might be a bit outdated yet should still mostly
work for plug-ins for GIMP 2.10.*
