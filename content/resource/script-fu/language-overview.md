+++
title = "Script-Fu Language Overview"
description = "The ScriptFu language, it's ancestry and features"
date = "2024-10-24"
weight = 3
author = "Lloyd Konneker"
+++

ScriptFu is a system and a language.
This is an overview of the language.

### Ancestry

The language descends from TinyScheme from Scheme from Lisp.

### Standards Conformance

There are many dialects of Scheme.
There are standards specifying language features.
ScriptFu more or less conforms to the R5RS standard,
with the notable exception that it lacks hygenic macros.

### Extra features

The ScriptFu language has extra features not in any
other dialect of Scheme.
The main feature of ScriptFu language is that it defines the functions
of the GIMP PDB.
That is, you can call GIMP from a script.
Use the *PDB Browser* of the GIMP app to see the GIMP procedures you can call.

There are a few other extra features:

  * Unicode strings using UTF-8 encoding
  * the byte type

### Summary

Another way of saying this is:

ScriptFu is an interpreter that embeds the TinyScheme interpreter.

The TinyScheme interprets a subset of Scheme R5RS language.

ScriptFu adds extra language features on top of TinyScheme.