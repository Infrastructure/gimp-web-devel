+++
title = "Overview of changes in ScriptFu version 3"
description = "A changelog or summary of changes"
date = "2024-10-24"
weight = 3
author = "Lloyd Konneker"
+++

### About

This is a brief list of changes, in order of importance.
See other pages for more information.

This is the NEWS for ScriptFu version 3.

The first few changes mean some existing scripts need to be ported.

### GIMP PDB Programmers Data Base

It changed for major release GIMP 3.0, breaking API.  This affects some existing scripts.

### New registration functions and GUI

You can register a script as a filter or an ordinary procedure.

Such scripts use a new dialog with improved settings.

The old registration function script-fu-register is deprecated.

### New filter scripts are multi-layer aware

All new filter scripts should be multi-layer capable and take a vector of drawables
(should use script-fu-register-filter.)

You can still declare a filter plugin taking one drawable,
but it will give a warning to the terminal where GIMP was started.

### Optional dialect for return values from the PDB

A script can optionally use a dialect that means shorter and more natural scripts.

### SF-VALUE is obsolete

Use SF-ADJUSTMENT instead.

### ScriptFu separate interpreter

You can install scripts like other plugins in the /plug-ins directory.
They will run in their own process.
Such scripts won't take down other scripts or menu items, if they crash hard.

### ScriptFu Console

The history of commands persists across sessions.
The next time you start the GIMP app,
you can up-arrow to get commands from the previous session.

### Byte type

The byte type was added.  It lets you read and write bytes from files.

### Scheme string-port

Fixed and slightly changed.

### Scheme display

The function now writes to stdout.
You can use it to debug scripts.

### Scheme quit

The function now immediately returns to GIMP with an error code
as the status of the call to the plugin script.

### Interpreter error messages

The interpreter gives improved error messages for mismatched parens
and other errors.

### Forgiving calls to the PDB

The interpreter will call PDB procedures
even when too few or too many arguments are given,
with a warning instead of an error.
Sometimes this will succeed.

### Logging

The interpreter does more logging to the terminal where GIMP was started.
This can aid in debugging scripts.

### Init scripts

The init scripts such as the conventional init.scm
are now in the /scripts/script-fu-init directory.

You can put your own such scripts in a similar, user directory
and a script can load them at run time.

### Compatibility scripts

Scripts can optionally load compatibility scripts for some obsolete
Scheme functions and PDB procedures
from GIMP 2.

### Test framework and scripts

New test scripts show how ScriptFu should behave.

When you find a bug, you should submit a test script to show it.

### Refresh Scripts

This menu item was removed.

To install or remove a script, use the GIMP Extension Manager.

### Unicode

Minor fixes.


