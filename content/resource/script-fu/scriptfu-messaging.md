
# Script messaging


## About

This tells how script plugins can "print" messages to a user.
It is a reference manual for several functions
in the ScriptFu language.

Some of the functions are like in other Scheme language implementations,
but have additional effects since ScriptFu is the "system"
that wraps or catches their effects.

The audience is script plugin authors using the ScriptFu language, or other languages.

## Introduction

A script can show messages.
Also known as "printing" messages.

Messages from scripts can go to:

  1. a *GIMP Error Dialog*
  2. the *GIMP Error Console*, a dockable in the GIMP app
  3. any terminal from which GIMP was started.
      A user does not always start GIMP in a terminal.
  4. the *ScriptFu Console*, a script IDE

Messages can be for debugging, or about errors.

A script plugin should not require a user to monitor
either the *GIMP Error Console* or a terminal.

Most scheme functions we discuss are called for their side effects,
and not for the value they return.

The functions when called in the *ScriptFu Console* (a REPL)
have different side effects.
This document says "show" and then explains
what is printed to what destination in various cases.

## Messaging functions

### PDB procedure *gimp-message*

```
(gimp-message <string>) => ()
```
Shows a message to the user of the script.

The function returns NIL to the script.
A script calls it for its side effects.

The only argument is a string.

This shows as a "Warning".


This is a procedure in the PDB (the GIMP Procedural Database.)
It is not a function in most Schemes.
Since it is in the PDB, plugins in other languages can call it.

Destination:

1. When the GIMP app has a GUI and the user has opened the *GIMP Error Console* : shows in the *GIMP Error Console*.
GIMP brings the *GIMP Error Console* to the top
and flashes it so the user should not miss the message. GIMP labels the message with "Warning".

2. When the GIMP app has a GUI and the user has *NOT* opened the *GIMP Error Console*: shows in the GIMP status bar.  This may flash by quickly, and *the user can miss seeing it.*

3. When the GIMP app has no GUI i.e. in batch mode: shows as text in the terminal where GIMP batch mode was started.

*A script calling *gimp-message* never shows a dialog.
A user does NOT need to acknowledge a *gimp-message*.*

*Because the user may miss seeing it, a script should
not call *gimp-message* for a condition the user should know about.*

### The *display* function

```
(display <any> ... ) => ()
```
Shows the representation of its arguments.
This is a side effect, and the function returns nil.

Variadic. Takes one or more arguments.

Shows the usual Scheme representation of its arguments.
That is, shows lists in parenthesis, functions by "#\<CLOSURE>", etc.

Shows immediately, i.e. IO is flushed, for all but the *ScriptFu Console.*

Destination:

1.  When GIMP was started from a terminal: shows to the terminal i.e. stdout.

2.  When GIMP was NOT started from a terminal: shows nowhere.

3.  When called from the *ScriptFu Console*: shows in the history pane, but only after the entire script is evaluated.  That is, IO is not flushed after every call to *display.*

Formerly, in ScriptFu v2, behavior was different, i.e. calling *display* did not show anything unless the script later had an exception,
and then GIMP which showed the representation of any as a prefix of an error message.

### The *print* function

Same as *display*, but appends a line separator character.

Not part of R5RS. Sometime known as *displayln*.

Equivalent to:
```
(display foo)
(newline)
```

### The *error* function

```
(error <reason string> ...) => nothing, does not yield or continue
```

Stops script execution and returns a string representation of its arguments to GIMP as an error message.

This function is not in R5RS but is like in other Schemes.
It behaves in ScriptFu like it does in SRFI 23.

When the first argument is a string,
ScriptFu does not show its representation surrounded by quote characters.

When the first argument is not a string, ScriptFu substitutes
"--" for the missing reason string,
followed by the representation of the first argument.

Variadic other arguments can be any Scheme object.

Does not return so yields nothing.

Destination of the error message:

1. When the Gimp app has a GUI, and the user has not opened the *GIMP Error Console* : shown in a separate *GIMP Error Dialog* requiring user to acknowledge by choosing the *OK* button.

2. When the Gimp app has a GUI, and the user has opened the *GIMP Error Console*: shown in the *GIMP Error Console*, but *a user is not required to acknowledge.*

3. When the Gimp app is in batch mode (started from a terminal and without a GUI): prints to said terminal i.e. stdout.

4. When called in the *ScriptFu Console*: shows "Error: Reason <...>" and nothing else.  Shows nothing to any terminal where Gimp was started.  Shows nothing to any open *GIMP Error Console*.


Examples of script plugins calling *error*:

Suppose the script plugin clothify.scm, which registers the menu item "Clothify", calls *error*.

When the GIMP app has a GUI and the *Gimp Error Console* is NOT open,
opens a dialog that has an exclamation point and an "Error" label, also showing:

```
Execution error for 'Clothify':
Reason.
```
When the GIMP app has a GUI and the *Gimp Error Console* is open, shows the same thing but in the *Gimp Error Console*.

Examples of calls to *error* in the *ScriptFu Console* :
```
> (error 1 "Reason")
Error:  --  1 "Reason"
```

```
> (error "Reason" 2)
Error: Reason 2
```
### The *quit* function

```
(quit <int>) => nothing
```
Stops script execution.
When a non-zero integer is passed, returns an error to GIMP
and writes to the current output port.

The function does not return, so yields nothing.

Any strings shown or returned to GIMP are side effects.
*Since you should not use it in scripts (see Best Practices)
we don't describe the effects in detail.*

In the *ScriptFu Console*, does not close the window.
When the integer is non-zero:
```
> (quit -1)
script quit with code: -1
Unknown error
```

The integer arg to *quit* is optional.
Only a non-zero integer returns an error to GIMP or the calling plugin.

```
(quit 0)
(quit)
```
These:

1. Terminate a script normally.
2. Return no error to GIMP or a calling plugin.
3. Writes nothing to stdout
4. Does nothing in the *ScriptFu Console*

Usually not necessary, just fall off the end of the script.
You can use them for early but normal returns from a script.

### The *any->string* function

```
(any->string <object>) => <string>
```
Returns a string representation of any Scheme object.

This function is in ScriptFu since Gimp v3.  This function is not in R5RS. It is similar to the *repr* special method of Python.

The representation is as you would see in most REPL's like the *ScriptFu Console*.  Some representations are as *sharp constants*,
having prefix the "#" character:

```
>(any->string gimp-message)
#<CLOSURE>
```
That is, the object gimp-message is a function.

## Best practice for declaring errors

The preferred way for a script to declare an error condition is to call the Scheme *error* function.

Example, when a script itself discovers a condition that prevents
continuing:

```
(error "Reason")
```
GIMP shows a message like:
```
Execution error for 'Clothify...':
Reason
```

Discouraged:

1. Calling *gimp-message* alone.
2. Calling *quit* alone.

Calling *gimp-message* alone does not imply an error.
A script calling *gimp-message* does not terminate the script
and return an error to GIMP or a calling plugin.

Note that the message may appear in any open *GIMP Error Console* as a "Warning".

Calling *quit* alone can return an error to GIMP,
but the error message is just a number that is useless to a user.

Errors discovered by the interpreter,
such as syntax or semantic errors,
are exceptions
that are returned to GIMP
via the \*error hook\*.
The user will experience these errors the same as if the script
called *error.*

## Best practice for harnessing a script with debug messages

Harnessing a program with debug statements means inserting "print" statements that aid in debugging.

Harness a script using the *display* function.  Start GIMP in a terminal. This only works since ScriptFu v3.  This has the advantage that you can leave the harness statements in place
and most users will never see the messages since they go to a terminal that a casual user does not open.

You can, but should not, harness the script with the *gimp-message* function, and open the *Gimp Error Console*.
This has the disadvantage that,
if you leave the harness in the script,
users will see the messages
flash by in the Gimp status bar or see the messages in the *Gimp Error Console*, when the user has it open.
This has the advantage, for temporary debugging,
that the messages are not interspersed with debugging messages
from GIMP or interpreters.

## Progress messages

You don't need to call *gimp-message* or *display* for progress messages.
ScriptFu displays progress in the GIMP status bar
whenever a script calls a PDB procedure.

## The \*error-hook\*

ScriptFu has a standard definition of the \*error-hook\* symbol.
As defined in the usual init.scm script,
named script-fu.init.  The standard definition is equivalent to just Scheme throw.

Advance users can redefine the \*error-hook\*.
When they do, the behavior of the *error* function as described above can be changed by the new \*error-hook\*.
A call to *error* generates an exception which the \*error-hook\* catches.
The behavior described above is downstream of the \*error-hook\*
and a redefined \*error-hook\* can handle the exception differently.



<!---
Side effects:

1. Writes the representation of all arguments
to the current output port, prefixed with "Error:"
The current output port is usually stdout/stderr.
2. Shows a message in GIMP
3. Returns an error to GIMP as the result of the script as a plugin.


In a script, when *the integer is non-zero* the GIMP app shows:

```
Execution error for 'Clothify...':
Script quit with no message.
```

A side effect writes this string to the current output port
(usually stdout/stderr):

```
script quit with code: <int>
```
--->

