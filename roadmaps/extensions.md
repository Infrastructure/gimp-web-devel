| Icon themes in extensions | Yes | |
| Themes in extensions | Yes | |
| Brushes in extensions | Yes | |
| Plug-ins in extensions | nearly done | plug-ins cannot be disabled or enabled live (without GIMP restart) yet |
| GEGL operations in extensions | No | |
| Installing extensions from `.gex` file | Yes | |
| Extension website | No | A [repository](https://gitlab.gnome.org/Infrastructure/gimp-extensions-web/) has been created yet is still quite bare. |
| Searching, installing from remote repository | WIP | |
| In-GIMP GUI for extension display | WIP | The dialog exists, but it's an early very-ugly implementation |
| Bi-direction core ⇆ plug-ins communication | WIP | [!120](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/120) Allow core to send signals for plug-ins to react |
