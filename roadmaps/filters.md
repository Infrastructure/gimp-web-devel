| Better GUI for layer effects | *Specified* | See [UX specs](https://developer.gimp.org/core/specifications/layer-effects-ui/). We might have to reimplement the item dockable widgets. |
| Layer effects and adjustement layers import from PSD | No | |
| Layer effects export to PSD | No | |
| Graph view | Discussed | As an alternative to the layer view, a graph-based view is regularly discussed. |
