| Action storage infrastructure | No | Class (`GimpMacro`?) to handle a "recording" which can be replayed |
| Store GEGL ops, plug-in runs, GIMP actions and tool use | WIP | These must be recordable in a `GimpMacro` |
| On-disk storage | No | A `GimpMacro` must be serializable and deserializable across sessions |
| Script-export | No | A `GimpMacro` should also be exportable in "code form" which can be reused as a plug-in |
| Macro tweak | No | A `GimpMacro` should be viewable and tweakable: e.g. you could trigger a recorded drawing at a higher resolution! |
| Script recording and playback | No |  [#8](https://gitlab.gnome.org/GNOME/gimp/issues/8) |
