| Improved toolbox | No | Simpler customization, draggable to other area, detachable, ability to show more than display size with `…` icon, etc. |
| Toolbar | No | Customizable to set quick buttons to any possible action |
| Better window management |  No |  Rather than a SWM vs MWM, both able to do some things the other can't, we should have a single window mode which can do everything |
| Dockable interaction review | No | Review and change how we can create, lock, unlock and reorganize dockables |
